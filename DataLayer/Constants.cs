﻿using System;
using System.Web;
using DataLayer.Models;

namespace DataLayer
{
    /// <summary>
    /// Класс, который определяет константы в программе
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Номер текущей сборки приложения
        /// </summary>
        public const string BuildVersion = "20190929_"+SystemVersion+"_"+SystemVersionName;

        /// <summary>
        /// Версия текущей сборки приложения (постминорное число - кол-во коммитов, минорное - слияний в мастер текущей мажорной версии)
        /// </summary>
        public const string SystemVersion = "0.0.15.214";

        /// <summary>
        /// Название текущей версии приложения
        /// </summary>
        public const string SystemVersionName = "Beta-RC1";

        public class HttpStatusCodes
        {
            /// <summary>
            /// HTTP статус-код успешного результата
            /// </summary>
            public const int STATUS_CODE_OK=200;

            /// <summary>
            /// HTTP статус-код неизвестной ошибки
            /// </summary>
            public const int STATUS_CODE_FAIL = 418;
        }

        /// <summary>
        /// Имена классов в подключаемых модулях
        /// </summary>
        public static class ModuleClassesNames
        {
            /// <summary>
            /// Имя класса в подключаемых модулях для конфигурации модуля
            /// </summary>
            public const string ModuleConfiguration = "Configuration";
        }

        /// <summary>
        /// Формат даты, который поддерживает Javascript для приведения в тип Date
        /// </summary>
        public const string JavascriptDateFormat = "yyyy-MM-dd";

        /// <summary>
        /// Формат даты, с помощью которого отправляется дата на сервер с использованием MomentJS
        /// </summary>
        public const string MomentServerDateFormat = "YYYY-MM-DD";

        /// <summary>
        /// Формат даты, который будет выводиться на страницу через MomentJS
        /// </summary>
        public const string MomentDateFormat = "DD.MM.YYYY";

        /// <summary>
        /// Формат даты, который будет выводиться на страницу через Razor
        /// </summary>
        public const string DisplayDateFormat = "dd.MM.yyyy";

        /// <summary>
        /// Формат даты и времени, который будет выводиться на страницу через Razor
        /// </summary>
        public const string DisplayDateTimeFormat = "dd.MM.yyyy hh:mm";

        /// <summary>
        /// Формат даты, который будет в компоненте выбора даты
        /// </summary>
        public const string DatePickerDateFormat = "dd.mm.yy";

        /// <summary>
        /// Максимальная длина описаний в программе
        /// </summary>
        public const int DescriptionMaxLength = 100;

        /// <summary>
        /// Максимальная длина названий в программе
        /// </summary>
        public const int NameMaxLength = 45;

        /// <summary>
        /// Максимальное число месяцев, для которых надо рассчитать бюджет, доступные для запросу пользователю
        /// </summary>
        public const int MaxMonthsBudgetsAvailableForRequest = 24;

        /// <summary>
        /// Минимальная сумма денег
        /// </summary>
        public const double MinMoneyValue = 0.01;

        /// <summary>
        /// Максимальная сумма денег
        /// </summary>
        public const double MaxMoneyValue = 1000000.00;

        /// <summary>
        /// Культура, которая используется для компонента выбора даты
        /// </summary>
        public const string DateTimePickerCulture = "en-US";

        /// <summary>
        /// Регулярное выражение, которое проверяет, какие имена свойств не разрешается логгировать
        /// </summary>
        public const string PropertyNameUnallowedLoggingRegex = ".*Id$";

        /// <summary>
        /// Регулярное выражение, которое проверяет, является ли символ допустимым для поискового запроса
        /// </summary>
        public const string SearchUnAllowedSymbolsRegex = "[^a-zA-Z0-9А-Яа-я\\-\\s]";

        /// <summary>
        /// Регулярное выражение, которое проверяет валидацию имени пользователя и имени группы пользователей
        /// </summary>
        /// <remarks>
        /// username is 4-12 characters long, no _ or . at the beginning, no __ or _. or ._ or .. inside, no _ or . at the end
        /// </remarks>
        public const string UserNameRegex = "^[a-zA-Z0-9А-Яа-я]([._](?![._])|[a-zA-Z0-9А-Яа-я]){2,10}[a-zA-Z0-9А-Яа-я]$";

        /// <summary>
        /// Регулярное выражение, которое проверяет, является ли поле конфигурации модуля полем для пароля
        /// </summary>
        public const string ModulePasswordFieldRegex = "\\[\\*\\]$";

        /// <summary>
        /// Регулярное выражение, которое проверяет, является ли поле конфигурации скрытым
        /// </summary>
        public const string ModuleHiddenFieldRegex = "\\[_\\]$";

        /// <summary>
        /// Строка для парольных полей ввода дополнительной конфигурации модуля по умолчанию.
        /// </summary>
        public const string DefaultModulePasswordString = "YouOnlyLiveTwice";

        /// <summary>
        /// Количество знаков после запятой в денежных значениях
        /// </summary>
        public const int MoneyDecimalPlaces = 2;

        /// <summary>
        /// Минимальное количество символов в пароле
        /// </summary>
        public const int MinPasswordLength = 6;

        /// <summary>
        /// Кол-во элементов на одной загружаемой странице
        /// </summary>
        public const int PageItemsCount = 7;

        /// <summary>
        /// Базовый параметр количества загружаемых элементов на странице поиска
        /// </summary>
        public const byte DefaultSearchTakeValue = 10;

        /// <summary>
        /// Базовый параметр количества загружаемых элементов в выпадающем меню поиска
        /// </summary>
        public const byte DropdownSearchTakeValue = 5;

        /// <summary>
        /// Максимальное количество хранимых типов ресурсов
        /// </summary>
        public const byte MaxResourceTypesCount = 10;

        /// <summary>
        /// URL для мнимых ссылок на страницах
        /// </summary>
        public const string FakeLinkUrl = "javascript: void(0)";

        /// <summary>
        /// Среднее количество дней в одном месяце
        /// </summary>
        public const double AverageDaysInMonth = 30.4;

        /// <summary>
        /// Значение точности для полнотекстового поиска в строковом представлении
        /// </summary>
        public const string FuzzySearchAccuracy = "0.5";

        /// <summary>
        /// Значение точности для non-fuzzy полнотекстового поиска в строковом представлении
        /// </summary>
        public const string NonFuzzySearchAccuracy = "0.9999999";

        /// <summary>
        /// Общий HTML идентификатор для модальной панели
        /// </summary>
        public const string ModalPanelId = "ModalPanel";

        /// <summary>
        /// Имя пользователя, которым подписывается приложение для выполнения задач
        /// </summary>
        public const string SystemUsername = "System";

        /// <summary>
        /// Имя куки, кототорое хранит в себе идентификатор текущего используемого ресурспака
        /// </summary>
        public const string CurrentResourcePackCookieName = "ResourcePack";

        /// <summary>
        /// Доступные для выбора элементы количества загружаемых элементов на странице поиска
        /// </summary>
        public static readonly byte[] AvaliableSearchTakeValues = { 5, 10, 25, 50 };

        /// <summary>
        /// Выбранные по умолчанию объекты полнотекстового поиска
        /// </summary>
        public static readonly Type[] DefaultSearchTypes = {
            typeof(Resource),
            typeof(MandatoryCashOff),
            typeof(Credit),
            typeof(Income),
            typeof(Spending)
        };

        /// <summary>
        /// Название стандартных узлов XML после сериализации для удаления их после конвертирования в JSON
        /// </summary>
        public static readonly string[] DefaultXmlNodeNames = {
            "@xmlns:xsi",
            "@xmlns:xsd"
        };

        /// <summary>
        /// Путь к папке хранения индекса для полнотекстового поиска
        /// </summary>
        public static readonly string SearchIndexDirectory = HttpContext.Current.Server.MapPath("~") + "\\SearchIndex";

        /// <summary>
        /// Минимальная дата, которую можно внести в программе
        /// </summary>
        public static readonly DateTime MinDate = new DateTime(2017, 06, 01);

        /// <summary>
        /// Максимальная дата, которую можно внести в программе
        /// </summary>
        public static readonly DateTime MaxDate = new DateTime(2099, 12, 31);

        /// <summary>
        /// Путь к папке подключаемых модулей
        /// </summary>
        public static readonly string ModulesDirectoryPath = string.Format("{0}Modules\\", HttpRuntime.AppDomainAppPath);
    }
}