﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lucene.Linq.Storage.EntityFramework;
using DataLayer.Interfaces;
using DataLayer.Infrastructure;
using StructureMap;
using DataLayer.Models;
using Lucene.Linq.Mapping;
using System.Reflection;
using Lucene.Linq;
using Lucene.Linq.Search;
using System.Collections;

namespace DataLayer
{
    /// <summary>
    /// Провайдер для полнотекстового поиска Lucene
    /// </summary>
    public class SearchProvider : ISearchProvider
    {
        /// <summary>
        /// Менеджер коллекции индексов поиска
        /// </summary>
        private SearchIndexSet searchIndexManager;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public SearchProvider()
        {
            searchIndexManager = ObjectFactory.GetInstance<SearchIndexSet>();
        }

        /// <summary>
        /// Ищет результаты поиска согласно конфигурации
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <param name="searchType">Тип индекса для поиска</param>
        /// <returns>Возвращает коллекцию результатов поиска</returns>
        public IEnumerable Search(SearchConfiguration configuration, Type searchType)
        {
            var searchQuery = SearchHelper.GenerateQuery(configuration);
            return GetIndexByTypeAndSearch(searchType, searchQuery, configuration.Page, configuration.Take);
        }

        /// <summary>
        /// Ищет результаты поиска исключительно Fuzzy запросом согласно конфигурации
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <param name="searchType">Тип индекса для поиска</param>
        /// <returns>Возвращает коллекцию результатов поиска</returns>
        public IEnumerable SearchFuzzy(SearchConfiguration configuration, Type searchType)
        {
            var searchQuery = SearchHelper.GenerateFuzzyQuery(configuration);
            return GetIndexByTypeAndSearch(searchType, searchQuery, configuration.Page, configuration.Take);
        }

        /// <summary>
        /// Добавляет объект в индекс полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность добавляемого объекта</param>
        public void AddToIndex(BaseModel entity)
        {
            searchIndexManager.GetIndex(entity.GetType()).Get(entity.GetType()).Add(entity);
        }

        /// <summary>
        /// Обновляет объект в индексе полнотекстового поиска
        /// </summary>
        /// <param name="oldentity">Старая сущность обновляемого объекта</param>
        /// <param name="newEntity">Новая сущность обновляемого объекта</param>
        public void UpdateInIndex(BaseModel oldentity, BaseModel newEntity)
        {
            RemoveFromIndex(oldentity);
            AddToIndex(newEntity);
        }

        /// <summary>
        /// Удаляет объект из индекса полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность удаляемого объекта</param>
        public void RemoveFromIndex(BaseModel entity)
        {
            searchIndexManager.GetIndex(entity.GetType()).Get(entity.GetType()).Delete(entity.Id.ToString());
        }

        /// <summary>
        /// Получает все типы моделей, которые могут использоваться в поиске
        /// </summary>
        /// <returns>Возвращает коллекцию типов для поиска</returns>
        public static IEnumerable<Type> GetAllSearchableModelTypes()
        {
            foreach (var t in Assembly.GetExecutingAssembly().GetTypes())
                if (t.GetCustomAttributes(typeof(DocumentAttribute), false).Any() && t.IsSubclassOf(typeof(BaseModel)))
                    yield return t;
        }

        /// <summary>
        /// Получает требуемый поисковой индекс согласно типу и производит поиск
        /// </summary>
        /// <param name="t">Тип индекса для поиска</param>
        /// <param name="query">Поисковой запрос</param>
        /// <param name="page">Страница результатов поиска</param>
        /// <param name="take">Количество элементов для взятия</param>
        /// <returns>Возвращает коллекцию результатов поиска</returns>
        private IEnumerable GetIndexByTypeAndSearch(Type t, string query, int page, byte take)
        {
            var searchIndex = searchIndexManager.GetIndex(t);
            MethodInfo method = typeof(SearchProvider).GetMethod("GetSearchPage", BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo generic = method.MakeGenericMethod(t);
            var result = generic.Invoke(this, new object[] { searchIndex, query, page, take });
            return (IEnumerable)result;
        }

        /// <summary>
        /// Получает определенную страницу результатов из индекса поиска
        /// </summary>
        /// <typeparam name="T">Тип индекса для поиска</typeparam>
        /// <param name="indexSet">Контейнер индексов поиска</param>
        /// <param name="query">Поисковой запрос</param>
        /// <param name="page">Страница результатов поиска</param>
        /// <param name="take">Количество элементов для взятия</param>
        /// <returns>Возвращает коллекцию результатов поиска</returns>
        private List<T> GetSearchPage<T>(EfDatabaseIndexSet<DatabaseContext> indexSet, string query, int page, byte take)
            where T: BaseModel, IIndexable
        {
            return indexSet.Get<T>().Where(x => x.Search(query)).Skip(page * take).Take(take).ToList();
        }
    }
}