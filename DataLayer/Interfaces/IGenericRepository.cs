﻿using System;
using System.Collections.Generic;
using DataLayer.Models;
using System.Linq.Expressions;

namespace DataLayer.Interfaces
{
    /// <summary>
    /// Базовый репозиторий таблицы базы данных
    /// </summary>
    /// <typeparam name="TEntity">Базовая модель</typeparam>
    public interface IGenericRepository<TEntity> where TEntity : BaseModel
    {
        /// <summary>
        /// Добавить объект в таблицу
        /// </summary>
        /// <param name="item">Добавляемый объект</param>
        void Create(TEntity item);

        /// Добавить или обновить объекты в таблице
        /// </summary>
        /// <param name="item">Коллекция добавляемых объектов</param>
        void CreateOrUpdate(TEntity[] items);

        /// <summary>
        /// Получить объект по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Возвращает объект</returns>
        TEntity FindById(long id);

        /// <summary>
        /// Получить объект по идентификатору с использованием трекинга
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Возвращает объект</returns>
        TEntity FindByIdWithTracking(long id);

        /// <summary>
        /// Получить объект по идентификатору,а также необходимые зависимые от него объекты
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает объект</returns>
        TEntity FindByIdWithInclude(long id, params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// Получить все объекты из таблицы
        /// </summary>
        /// <returns>Возвращает список всех объектов</returns>
        IEnumerable<TEntity> Get();

        /// <summary>
        /// Пересоздает контекст БД
        /// </summary>
        void RecreateContext();

        /// <summary>
        /// Получить "страницу" объектов
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="predicate">Предикат</param>
        /// <returns>Возвращает "страницу" объектов</returns>
        IEnumerable<TEntity> Page(int page, Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получить "страницу" объектов с зависимыми полями
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="predicate">Предикат</param>
        /// <param name="includeProperties">Зависимые объекты для получения</param>
        /// <returns>Возвращает "страницу" объектов с зависимыми полями</returns>
        IEnumerable<TEntity> PageWithInclude(
            int page,
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// Получить все объекты из таблицы, удовлетворяющие предикату
        /// </summary>
        /// <param name="predicate">Предикат</param>
        /// <returns>Возвращает список объектов</returns>
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получить количество элементов в таблице
        /// </summary>
        /// <returns>Возвращает количество элементов в таблице</returns>
        long Count();

        /// <summary>
        /// Получить количество элементов, согласно предикату, в таблице
        /// </summary>
        /// <returns>Возвращает количество элементов, согласно предикату, в таблице</returns>
        long Count(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получить значение, которое показывает, есть ли в таблице элементы по заданному условию
        /// </summary>
        /// <returns>Возвращает значение, которое показывает, есть ли в таблице элементы по заданному условию</returns>
        bool Any(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Удалить объект из таблицы базы данных
        /// </summary>
        /// <param name="item">Удаляемый объект</param>
        void Remove(TEntity item);

        /// <summary>
        /// Удалить объект из таблицы базы данных по предикату
        /// </summary>
        /// <param name="predicate">Предикат</param>
        void Remove(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Обновить объект в таблице базы данных
        /// </summary>
        /// <param name="item">Обновляемый объект</param>
        void Update(TEntity item);

        /// <summary>
        /// Получить все объекты из таблицы, а также необходимые зависимые от них объекты
        /// </summary>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает список объектов</returns>
        IEnumerable<TEntity> GetWithInclude(params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// Получить все объекты из таблицы, а также необходимые зависимые от них объекты, удовлетворяющие предикату
        /// </summary>
        /// <param name="predicate">Предикат</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает список объектов</returns>
        IEnumerable<TEntity> GetWithInclude(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// Получить все объекты из таблицы, удовлетворяющие предикату с занесением объектов в контекст
        /// </summary>
        /// <param name="predicate">Предикат</param>
        /// <returns>Возвращает список объектов</returns>
        IEnumerable<TEntity> GetWithTracking(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получить последний объект по указанному параметру порядка с зависимыми объектами
        /// </summary>
        /// <param name="orderProperty">Поле, по которому следует сортировать</param>
        /// <param name="predicate">Условный предикат</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает объект</returns>
        TEntity LastOrDefaultWithInclude<TOrderKey>(
            Expression<Func<TEntity, TOrderKey>> orderProperty,
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// Получить список сгруппированных последних объектов по указанному параметру порядка с зависимыми объектами
        /// </summary>
        /// <param name="orderProperty">Поле, по которому следует сортировать</param>
        /// <param name="predicate">Условный предикат</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <param name="groupPredicate">Предикат группировки</param>
        /// <returns>Возвращает список объектов</returns>
        List<TEntity> GroupedLastOrDefaultWithInclude<TGroupKey, TOrderKey>(
            Expression<Func<TEntity, TOrderKey>> orderProperty,
            Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TGroupKey>> groupPredicate,
            params Expression<Func<TEntity, object>>[] includeProperties);
    }
}