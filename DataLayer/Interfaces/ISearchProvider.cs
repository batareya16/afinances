﻿using System;
using DataLayer.Models;
using System.Collections;

namespace DataLayer.Interfaces
{
    /// <summary>
    /// Провайдер для полнотекстового поиска Lucene
    /// </summary>
    public interface ISearchProvider
    {
        /// <summary>
        /// Ищет результаты поиска согласно конфигурации
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <param name="searchType">Тип индекса для поиска</param>
        /// <returns>Возвращает коллекцию результатов поиска</returns>
        IEnumerable Search(SearchConfiguration configuration, Type searchType);

        /// <summary>
        /// Ищет результаты поиска исключительно Fuzzy запросом согласно конфигурации
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <param name="searchType">Тип индекса для поиска</param>
        /// <returns>Возвращает коллекцию результатов поиска</returns>
        IEnumerable SearchFuzzy(SearchConfiguration configuration, Type searchType);

        /// <summary>
        /// Добавляет объект в индекс полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность добавляемого объекта</param>
        void AddToIndex(BaseModel entity);

        /// <summary>
        /// Удаляет объект из индекса полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность удаляемого объекта</param>
        void RemoveFromIndex(BaseModel entity);

        /// <summary>
        /// Обновляет объект в индексе полнотекстового поиска
        /// </summary>
        /// <param name="oldentity">Старая сущность обновляемого объекта</param>
        /// <param name="newEntity">Новая сущность обновляемого объекта</param>
        void UpdateInIndex(BaseModel oldentity, BaseModel newEntity);
    }
}