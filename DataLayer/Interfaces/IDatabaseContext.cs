﻿namespace DataLayer.Interfaces
{
    /// <summary>
    /// Интерфейс для контекста базы данных (нужен для внедрения зависимостей)
    /// </summary>
    public interface IDatabaseContext
    {
    }
}