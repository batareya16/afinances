﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;
using System.Data;
using DataLayer.Interfaces;
using DataLayer.Models;
using StructureMap;

namespace DataLayer
{
    /// <summary>
    /// Базовый репозиторий таблицы базы данных
    /// </summary>
    /// <typeparam name="TEntity">Базовая модель</typeparam>
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseModel
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private DatabaseContext _context;

        /// <summary>
        /// Таблица базы данных
        /// </summary>
        private DbSet<TEntity> _dbSet;
 
        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public GenericRepository()
        {
            _context = ObjectFactory.GetInstance<IDatabaseContext>() as DatabaseContext;
            _dbSet = _context.Set<TEntity>();
        }

        /// <summary>
        /// Пересоздает контекст БД
        /// </summary>
        public void RecreateContext()
        {
            _context = new DatabaseContext();
        }

        /// <summary>
        /// Получить все объекты из таблицы
        /// </summary>
        /// <returns>Возвращает список всех объектов</returns>
        public IEnumerable<TEntity> Get()
        {
            return _dbSet.AsNoTracking().ToList();
        }

        /// <summary>
        /// Получить "страницу" объектов
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="predicate">Предикат</param>
        /// <returns>Возвращает "страницу" объектов</returns>
        public IEnumerable<TEntity> Page(int page, Expression<Func<TEntity, bool>> predicate)
        {
            var skip = page * Constants.PageItemsCount;
            var take = Constants.PageItemsCount;
            return _dbSet.AsNoTracking().Where(predicate).OrderByDescending(x => x.Id).Skip(skip).Take(take).ToList();
        }

        /// <summary>
        /// Получить "страницу" объектов с зависимыми полями
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="predicate">Предикат</param>
        /// <param name="includeProperties">Зависимые объекты для получения</param>
        /// <returns>Возвращает "страницу" объектов с зависимыми полями</returns>
        public IEnumerable<TEntity> PageWithInclude(
            int page,
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var skip = page * Constants.PageItemsCount;
            var take = Constants.PageItemsCount;
            var query = Include(includeProperties);
            return query.Where(predicate).OrderByDescending(x => x.Id).Skip(skip).Take(take).ToList();
        }

        /// <summary>
        /// Получить все объекты из таблицы, удовлетворяющие предикату
        /// </summary>
        /// <param name="predicate">Предикат</param>
        /// <returns>Возвращает список объектов</returns>
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate).ToList();
        }

        /// <summary>
        /// Получить все объекты из таблицы, удовлетворяющие предикату с занесением объектов в контекст
        /// </summary>
        /// <param name="predicate">Предикат</param>
        /// <returns>Возвращает список объектов</returns>
        public IEnumerable<TEntity> GetWithTracking(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        /// <summary>
        /// Получить объект по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Возвращает объект</returns>
        public TEntity FindById(long id)
        {
            return _dbSet.AsNoTracking().Single(x => x.Id == id);
        }

        /// <summary>
        /// Получить объект по идентификатору с использованием трекинга
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Возвращает объект</returns>
        public TEntity FindByIdWithTracking(long id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// Получить объект по идентификатору,а также необходимые зависимые от него объекты
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает объект</returns>
        public TEntity FindByIdWithInclude(long id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return query.Single(x => x.Id == id);
        }

        /// <summary>
        /// Получить количество элементов, согласно предикату, в таблице
        /// </summary>
        /// <returns>Возвращает количество элементов, согласно предикату, в таблице</returns>
        public long Count(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).Count();
        }

        /// <summary>
        /// Получить количество элементов в таблице
        /// </summary>
        /// <returns>Возвращает количество элементов в таблице</returns>
        public long Count()
        {
            return _dbSet.Count();
        }

        /// <summary>
        /// Получить значение, которое показывает, есть ли в таблице элементы по заданному условию
        /// </summary>
        /// <returns>Возвращает значение, которое показывает, есть ли в таблице элементы по заданному условию</returns>
        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Any(predicate);
        }

        /// <summary>
        /// Добавить объект в таблицу
        /// </summary>
        /// <param name="item">Добавляемый объект</param>
        public void Create(TEntity item)
        {
            _dbSet.Add(item);
            _context.SaveChanges();
        }

        /// <summary>
        /// Добавить или обновить объекты в таблице
        /// </summary>
        /// <param name="item">Коллекция добавляемых объектов</param>
        public void CreateOrUpdate(TEntity[] items)
        {
            _dbSet.AddOrUpdate(items);
            _context.SaveChanges();
        }

        /// <summary>
        /// Обновить объект в таблице базы данных
        /// </summary>
        /// <param name="item">Обновляемый объект</param>
        public void Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        /// <summary>
        /// Удалить объект из таблицы базы данных
        /// </summary>
        /// <param name="item">Удаляемый объект</param>
        public void Remove(TEntity item)
        {
            _dbSet.Remove(item);
            _context.SaveChanges();
        }

        /// <summary>
        /// Удалить объект из таблицы базы данных по предикату
        /// </summary>
        /// <param name="predicate">Предикат</param>
        public void Remove(Expression<Func<TEntity, bool>> predicate)
        {
            foreach (var item in _dbSet.Where(predicate))
                _dbSet.Remove(item);
            _context.SaveChanges();
        }

        /// <summary>
        /// Получить все объекты из таблицы, а также необходимые зависимые от них объекты
        /// </summary>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает список объектов</returns>
        public IEnumerable<TEntity> GetWithInclude(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return Include(includeProperties).ToList();
        }

        /// <summary>
        /// Получить все объекты из таблицы, а также необходимые зависимые от них объекты, удовлетворяющие предикату
        /// </summary>
        /// <param name="predicate">Предикат</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает список объектов</returns>
        public IEnumerable<TEntity> GetWithInclude(
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return query.Where(predicate).ToList();
        }

        /// <summary>
        /// Получить последний объект по указанному параметру порядка с зависимыми объектами
        /// </summary>
        /// <param name="orderProperty">Поле, по которому следует сортировать</param>
        /// <param name="predicate">Условный предикат</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <returns>Возвращает объект</returns>
        public TEntity LastOrDefaultWithInclude<TOrderKey>(
            Expression<Func<TEntity, TOrderKey>> orderProperty,
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return Include(includeProperties)
                .OrderByDescending(orderProperty)
                .Where(predicate)
                .FirstOrDefault();
        }

        /// <summary>
        /// Получить список сгруппированных последних объектов по указанному параметру порядка с зависимыми объектами
        /// </summary>
        /// <param name="orderProperty">Поле, по которому следует сортировать</param>
        /// <param name="predicate">Условный предикат</param>
        /// <param name="includeProperties">Список необходимых зависимых связанных объектов</param>
        /// <param name="groupPredicate">Предикат группировки</param>
        /// <returns>Возвращает список объектов</returns>
        public List<TEntity> GroupedLastOrDefaultWithInclude<TGroupKey, TOrderKey>(
            Expression<Func<TEntity, TOrderKey>> orderProperty,
            Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TGroupKey>> groupPredicate,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return Include(includeProperties)
                .OrderByDescending(orderProperty)
                .Where(predicate)
                .GroupBy(groupPredicate)
                .Select(x => x.LastOrDefault())
                .ToList();
        }

        private IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }
    }
}