﻿using System.ComponentModel.DataAnnotations;
using CommonResource = DataLayer.Resources.Models.Common;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;

namespace DataLayer.Models
{
    /// <summary>
    /// Базовая модель сущностей БД
    /// </summary>
    public abstract class BaseModel
    {
        /// <summary>
        /// Идентификатор сущности базы данных
        /// </summary>
        [Key]
        [Display(ResourceType = typeof(CommonResource), Name = "Id")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes)]
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор сущности для полнотекстового поиска Lucene
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsKey = true, Analyzer = typeof(KeywordAnalyzer))]
        [NotMapped]
        public string LuceneSearchId { get { return Id.ToString(); } set { } }
    }
}