﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;
using Lucene.Linq.Search;
using System.Collections.Generic;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность типа ресурсов
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "ResourceTypeName")]
    [Document]
    public class ResourceType : BaseModel, IIndexable
    {
        /// <summary>
        /// Название типа ресурсов
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Resource), Name = "Name")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор группы пользователей, к которой относится тип ресурса
        /// </summary>
        /// <remarks>
        /// Без внешнего ключа, чтобы не создавать цикличную зависимость в БД
        /// </remarks>
        public long UserGroupId { get; set; }
    }
}