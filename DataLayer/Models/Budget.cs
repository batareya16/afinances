﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность бюджета
    /// </summary>
    public class Budget : BaseModel
    {
        /// <summary>
        /// Баланс бюджета
        /// </summary>
        public double Balance { get; set; }

        /// <summary>
        /// Идентификатор ресурса
        /// </summary>
        [ForeignKey("Resource")]
        public long ResourceId { get; set; }

        /// <summary>
        /// Дата измерения бюджета
        /// </summary>
        public DateTime BudgetDateTime { get; set; }

        /// <summary>
        /// Связанная сущность ресурса
        /// </summary>
        public Resource Resource { get; set; }
    }
}