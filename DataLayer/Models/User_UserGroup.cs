﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность связки пользователя и группы пользователей
    /// </summary>
    public class User_UserGroup : BaseModel
    {
        /// <summary>
        /// Имя привязанного пользователя
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Идентификатор группы пользователей
        /// </summary>
        [ForeignKey("UserGroup")]
        public long UserGroupId { get; set; }

        /// <summary>
        /// Идентификатор группы пользователей
        /// </summary>
        public UserGroup UserGroup { get; set; }
    }
}