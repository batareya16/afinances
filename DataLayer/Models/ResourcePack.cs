﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;
using Lucene.Linq.Search;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность ресурспака
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "ResourcePackName")]
    [Document]
    public class ResourcePack : BaseModel, IIndexable
    {
        /// <summary>
        /// Название ресурспака
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Resource), Name = "Name")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Name { get; set; }

        /// <summary>
        /// Лимит средств для ресурспака
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Resource), Name = "Limit")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public double Limit { get; set; }

        /// <summary>
        /// Идентификатор группы пользователей, к которой относится ресурспак
        /// </summary>
        [ForeignKey("UserGroup")]
        public long UserGroupId { get; set; }

        /// <summary>
        /// Группа пользователей, к которой относится ресурспак
        /// </summary>
        public UserGroup UserGroup { get; set; }
        
        /// <summary>
        /// Список связанных Ресурсов
        /// </summary>
        public virtual IEnumerable<Resource> Resources { get; set; }

        /// <summary>
        /// Список связанных Кредитов
        /// </summary>
        public virtual IEnumerable<Credit> Credits { get; set; }
    }
}