﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность результатов полнотекстового поиска
    /// </summary>
    public class SearchResult
    {
        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public SearchResult()
        {
            Results = new Dictionary<Type, IEnumerable>();
        }

        /// <summary>
        /// Коллекция элементов результата
        /// </summary>
        public Dictionary<Type, IEnumerable> Results { get; set; }
        
        /// <summary>
        /// Общее количество элементов результата
        /// </summary>
        public long Count { get; set; }

        /// <summary>
        /// Текущая страница поиска
        /// </summary>
        public long Page { get; set; }
    }
}