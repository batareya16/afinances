﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources;
using DataLayer.Infrastructure;
using DataLayer.Resources.Models;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;
using Lucene.Linq.Search;
using System.Globalization;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность расхода
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "SpendingName")]
    [Document]
    public class Spending : BaseModel, IIndexable
    {
        /// <summary>
        /// Дата расхода
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        public DateTime DateAndTime { get; set; }

        /// <summary>
        /// Строка даты расхода. Нужна для полнотекстового поиска
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsSortable = true, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        [NotMapped]
        public string DateTimeString {
            get { return DateAndTime.ToString(Constants.DisplayDateFormat); }
            set {
                try
                {
                    DateAndTime = DateTime.ParseExact(value, Constants.DisplayDateFormat, CultureInfo.InvariantCulture);
                }
                catch
                {
                    DateAndTime = new DateTime();
                }
            } 
        }
        
        /// <summary>
        /// Место расхода бюджета
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Place")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Place {get; set; }

        /// <summary>
        /// Описание расхода
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Description")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Description { get; set; }

        /// <summary>
        /// Сумма расхода
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Price")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public double Price { get; set; }

        [NotMapped]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "RoundedPrice")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public long RoundedPrice { get { return (long)Math.Round(Price); } set { } } 

        /// <summary>
        /// Идентификатор связанного объекта Ресурса
        /// </summary>
        [ForeignKey("Resource")]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Resource")]
        public long ResourceId { get; set; }

        /// <summary>
        /// Связанный объект Ресурса
        /// </summary>
        public Resource Resource { get; set; }
    }
}