﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using Lucene.Linq.Mapping;
using Lucene.Linq.Search;
using Lucene.Net.Analysis;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность ресурса
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "ResourceName")]
    [Document]
    public class Resource : BaseModel, IIndexable
    {
        /// <summary>
        /// Идентификатор связанного объекта типа ресурса
        /// </summary>
        [ForeignKey("ResourceType")]
        [Display(ResourceType = typeof(Resources.Models.Resource), Name = "ResourceType")]
        public long ResourceTypeId { get; set; }

        /// <summary>
        /// Название ресурса
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Resource), Name = "Name")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор связанного объекта Ресурспака
        /// </summary>
        [ForeignKey("ResourcePack")]
        [Display(ResourceType = typeof(Resources.Models.Resource), Name = "ResourcePack")]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Связанный объект типа ресурса
        /// </summary>
        public ResourceType ResourceType { get; set; }

        /// <summary>
        /// Связанный объект Ресурспака
        /// </summary>
        public ResourcePack ResourcePack { get; set; }
    }
}