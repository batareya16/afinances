﻿using Lucene.Linq.Search;
using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using DataLayer.Resources.Models;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность пользователя. Нужна исключительно для полнотекстового поиска
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "UserName")]
    [Document]
    public class User : BaseModel, IIndexable
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Display(ResourceType = typeof(Account), Name = "UserName")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public string UserName { get; set; }
    }
}