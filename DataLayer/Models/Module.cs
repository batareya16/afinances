﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность модуля
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "ModuleName")]
    public class Module : BaseModel
    {
        /// <summary>
        /// Идентификатор связанного объекта Репозитория
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Module), Name = "Repository")]
        [ForeignKey("Repository")]
        public long? RepositoryId { get; set; }

        /// <summary>
        /// Имя файла модуля
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Module), Name = "Name")]
        [MaxLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// Отображаемое имя модуля
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Module), Name = "DisplayName")]
        [MaxLength(40)]
        public string DisplayName { get; set; }

        /// <summary>
        /// Данные картинки модуля в виде BASE64 строки
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Module), Name = "ImageBase64")]
        [MaxLength]
        public string ImageBase64 { get; set; }

        /// <summary>
        /// Связанный объект репозитория
        /// </summary>
        public ModuleRepository Repository { get; set; }
    }
}