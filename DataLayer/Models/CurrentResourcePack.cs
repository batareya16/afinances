﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность, которая определяет связку пользователя и текущий его используемый ресурспак в БД
    /// </summary>
    [Table("CurrentUserResourcePacks")]
    public class CurrentResourcePack : BaseModel
    {
        /// <summary>
        /// Имя пользователя, к которому привязан текущий ресурспак
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Идентификатор привязанного текущего используемого ресурспака
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.CurrentResourcePack), Name = "ResourcePack")]
        [ForeignKey("ResourcePack")]
        public long? CurrentResourcePackId { get; set; }

        /// <summary>
        /// Идентификатор привязанной текущей группы пользователей
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.CurrentResourcePack), Name = "UserGroup")]
        [ForeignKey("UserGroup")]
        public long? CurrentUserGroupId { get; set; }

        /// <summary>
        /// Объект привязанного текущего используемого ресурспака
        /// </summary>
        public ResourcePack ResourcePack { get; set; }

        /// <summary>
        /// Объект привязанного текущей используемой группы пользователей
        /// </summary>
        public UserGroup UserGroup { get; set; }
    }
}