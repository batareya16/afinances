﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using DataLayer.Resources.Models;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;
using System.Globalization;
using Lucene.Linq.Search;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность прибыли
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "IncomeName")]
    [Document]
    public class Income : BaseModel, IIndexable
    {
        /// <summary>
        /// Дата прибыли
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        public DateTime DateAndTime { get; set; }

        /// <summary>
        /// Строка даты прибыли. Нужна для полнотекстового поиска
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsSortable = true, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        [NotMapped]
        public string DateTimeString
        {
            get { return DateAndTime.ToString(Constants.DisplayDateFormat); }
            set
            {
                try
                {
                    DateAndTime = DateTime.ParseExact(value, Constants.DisplayDateFormat, CultureInfo.InvariantCulture);
                }
                catch
                {
                    DateAndTime = new DateTime();
                }
            }
        }

        /// <summary>
        /// Описание прибыли
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Description")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Description { get; set; }

        /// <summary>
        /// Сумма прибыли
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Summ")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public double Summ { get; set; }

        [NotMapped]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "RoundedSumm")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public long RoundedSumm { get { return (long)Math.Round(Summ); } set { } } 

        /// <summary>
        /// Идентификатор связанного Ресурса
        /// </summary>
        [ForeignKey("Resource")]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Resource")]
        public long ResourceId { get; set; }


        /// <summary>
        /// Связанный объект Ресурса
        /// </summary>
        public Resource Resource { get; set; }
    }
}