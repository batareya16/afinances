﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность модуля, привязанного к ресурсу
    /// </summary>
    public class ModuleResource : BaseModel
    {
        /// <summary>
        /// Идентификатор модуля
        /// </summary>
        [ForeignKey("Module")]
        [Display(ResourceType = typeof(ModuleResource), Name = "Module")]
        public long ModuleId { get; set; }

        /// <summary>
        /// Идентификатор ресурса
        /// </summary>
        [ForeignKey("Resource")]
        [Display(ResourceType = typeof(ModuleResource), Name = "Resource")]
        public long ResourceId { get; set; }

        /// <summary>
        /// Флаг, который показывает, запущен модуль или нет
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "IsActive")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Флаг, который показывает, отложенная работа модуля или запланированная
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "IsDelayedJob")]
        public bool IsDelayedJob { get; set; }

        /// <summary>
        /// Часы для планировщика задач модулей
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "Hours")]
        public byte Hours { get; set; }

        /// <summary>
        /// Минуты для планировщика задач модулей
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "Minutes")]
        public byte Minutes { get; set; }

        /// <summary>
        /// JSON-строка дополнительной конфигурации для модуля
        /// </summary>
        [MaxLength]
        public string AdditionalConfiguration { get; set; }

        /// <summary>
        /// Связанная сущность модуля
        /// </summary>
        public Module Module { get; set; }

        /// <summary>
        /// Связанная сущность ресурса
        /// </summary>
        public Resource Resource { get; set; }
    }
}