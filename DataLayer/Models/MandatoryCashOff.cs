﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;
using Lucene.Linq.Search;
using System.Globalization;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность обязательной выплаты
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "MandatoryCashOffName")]
    [Document]
    public class MandatoryCashOff : BaseModel, IIndexable
    {
        /// <summary>
        /// Место выплаты
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Place")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Place { get; set; }

        /// <summary>
        /// Описание выплаты
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Description")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Description { get; set; }

        /// <summary>
        /// Сумма выплаты
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Summ")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public double Summ { get; set; }

        [NotMapped]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public long RoundedSum { get { return (long)Math.Round(Summ); } set { } } 

        /// <summary>
        /// Идентификатор связанного объекта Ресурспака
        /// </summary>
        [ForeignKey("ResourcePack")]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "ResourcePack")]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Дата обязательной выплаты
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Строка даты обязательной выплаты. Нужна для полнотекстового поиска
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsSortable = true, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        [NotMapped]
        public string DateString
        {
            get { return Date.ToString(Constants.DisplayDateFormat); }
            set
            {
                try
                {
                    Date = DateTime.ParseExact(value, Constants.DisplayDateFormat, CultureInfo.InvariantCulture);
                }
                catch
                {
                    Date = new DateTime();
                }
            }
        }


        /// <summary>
        /// Объект связанного Ресурспака
        /// </summary>
        public ResourcePack ResourcePack { get; set; }

    }
}