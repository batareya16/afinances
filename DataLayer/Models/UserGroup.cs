﻿using System;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using Lucene.Linq.Search;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность группы пользователей
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "UserGroup")]
    [Document]
    public class UserGroup : BaseModel, IIndexable
    {
        /// <summary>
        /// Имя группы пользователей
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.UserGroup), Name = "Name")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Name { get; set; }

        /// <summary>
        /// Флаг, показывающий приватная группа пользователей или нет
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.UserGroup), Name = "Private")]
        public bool Private { get; set; }

        /// <summary>
        /// Владелец группы пользователей
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.UserGroup), Name = "OwnerUsername")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public string OwnerUsername { get; set; }

        /// <summary>
        /// Привязанная коллекция связки пользователей - группа пользователей
        /// </summary>
        public virtual ICollection<User_UserGroup> User_UserGroups { get; set; }

        /// <summary>
        /// Привязанная коллекция связки пользователей - группа пользователей в виде строки для Lucene поиска
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = false, Analyzer = typeof(WhitespaceAnalyzer))]
        [NotMapped]
        public string User_UserGroupsString
        {
            get
            {
                return User_UserGroups == null
                    ? string.Empty
                    : string.Join(" ", User_UserGroups.Select(x => x.UserName));
            }
            set
            {
                User_UserGroups = value
                    .Split(' ')
                    .Select(x => new User_UserGroup() {UserName = x, UserGroupId = Id})
                    .ToList();
            }
        }
    }
}