﻿namespace DataLayer.Models
{
    /// <summary>
    /// Сущность репозитория модулей
    /// </summary>
    public class ModuleRepository : BaseModel
    {
        /// <summary>
        /// Название репозитория
        /// </summary>
        public string Name { get; set; }
    }
}