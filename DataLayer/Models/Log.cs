﻿using System;
using DataLayer.Infrastructure;
using Lucene.Linq.Mapping;
using Lucene.Net.Analysis;
using System.Globalization;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources;
using Lucene.Linq.Search;
using LogResource = DataLayer.Resources.Models.Log;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность лога
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "LogName")]
    [Document]
    public class Log : BaseModel, IIndexable
    {
        /// <summary>
        /// Описание лога
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        [Display(ResourceType = typeof(LogResource), Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// Дата и время лога
        /// </summary>
        [Display(ResourceType = typeof(LogResource), Name = "DateAndTime")]
        public DateTime DateAndTime { get; set; }

        /// <summary>
        /// Строка даты и времени лога. Нужна для полнотекстового поиска
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsSortable = true, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        [NotMapped]
        public string DateTimeString
        {
            get { return DateAndTime.ToString(Constants.DisplayDateTimeFormat); }
            set
            {
                try
                {
                    DateAndTime = DateTime.ParseExact(value, Constants.DisplayDateTimeFormat, CultureInfo.InvariantCulture);
                }
                catch
                {
                    DateAndTime = new DateTime();
                }
            }
        }

        /// <summary>
        /// Статус/Тип лога
        /// </summary>
        [Display(ResourceType = typeof(LogResource), Name = "LogStatus")]
        public LogStatus LogStatus { get; set; }

        /// <summary>
        /// Имя инициатора лога
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        [Display(ResourceType = typeof(LogResource), Name = "UserName")]
        public string UserName { get; set; }
    }
}