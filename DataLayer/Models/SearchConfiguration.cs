﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность конфигурации для полнотекстового поиска
    /// </summary>
    public class SearchConfiguration
    {
        /// <summary>
        /// Поисковой запрос
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// Количество элементов для взятия 
        /// </summary>
        public byte Take { get; set; }
        
        /// <summary>
        /// Необходимый номер страницы для взятия результатов
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Имена типов, которые участвуют в поиске
        /// </summary>
        public IEnumerable<string> TypeNames
        {
            get
            {
                return TypeNamesJson != null
                ? new JavaScriptSerializer().Deserialize<List<string>>(TypeNamesJson)
                : Enumerable.Empty<string>();
            }
        }

        /// <summary>
        /// Результаты, которые требуется исключить из поиска
        /// </summary>
        public IEnumerable<string> ExcludeResults { get; set; }

        /// <summary>
        /// Дополнительные параметры фильтрации поиска формата "Имя Поля" = "Значение"
        /// </summary>
        public Dictionary<string, string> FieldsConditions { get; set; }

        /// <summary>
        /// Имена типов, которые участвуют в поиске, представленные в виде JSON строки для парсинга со страницы
        /// </summary>
        public string TypeNamesJson { get; set; }
    }
}