﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using Lucene.Linq.Mapping;
using System.Globalization;
using Lucene.Net.Analysis;
using Lucene.Linq.Search;

namespace DataLayer.Models
{
    /// <summary>
    /// Сущность кредита
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "CreditName")]
    [Document]
    public class Credit : BaseModel, IIndexable
    {
        /// <summary>
        /// Идентификатор связанного объекта Ресурспака
        /// </summary>
        [ForeignKey("ResourcePack")]
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "ResourcePack")]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Дата начала кредита
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "StartDate")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Строка даты начала кредита. Нужна для полнотекстового поиска
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsSortable = true, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "StartDate")]
        [NotMapped]
        public string StartDateString
        {
            get { return StartDate.ToString(Constants.DisplayDateFormat); }
            set
            {
                try
                {
                    StartDate = DateTime.ParseExact(value, Constants.DisplayDateFormat, CultureInfo.InvariantCulture);
                }
                catch
                {
                    StartDate = new DateTime();
                }
            }
        }

        /// <summary>
        /// Дата окончания кредита
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "EndDate")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Строка даты окончания кредита. Нужна для полнотекстового поиска
        /// </summary>
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsSortable = true, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "EndDate")]
        [NotMapped]
        public string EndDateString
        {
            get { return EndDate.ToString(Constants.DisplayDateFormat); }
            set
            {
                try
                {
                    EndDate = DateTime.ParseExact(value, Constants.DisplayDateFormat, CultureInfo.InvariantCulture);
                }
                catch
                {
                    EndDate = new DateTime();
                }
            }
        }

        /// <summary>
        /// Сумма кредита
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "Summ")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public double Summ { get; set; }

        [NotMapped]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(KeywordAnalyzer))]
        public long RoundedSumm { get { return (long)Math.Round(Summ); } set { } } 

        /// <summary>
        /// Описание кредита
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "Description")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Description { get; set; }

        /// <summary>
        /// Название кредита
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "Name")]
        [Field(FieldIndex.Tokenized, FieldStore.Yes, IsDefault = true, Analyzer = typeof(WhitespaceAnalyzer))]
        public string Name { get; set; }

        /// <summary>
        /// Оплачен ли кредит или нет
        /// </summary>
        [Display(ResourceType = typeof(Resources.Models.Credit), Name = "IsPayed")]
        public bool IsPayed { get; set; }


        /// <summary>
        /// Связанный объект Ресурспака
        /// </summary>
        public ResourcePack ResourcePack { get; set; }
    }
}