﻿using System;
using Lucene.Linq.Storage.EntityFramework;
using Lucene.Net.Store;
using System.IO;
using StructureMap;
using DataLayer.Interfaces;
using DataLayer.Models;

namespace DataLayer
{
    /// <summary>
    /// Сущность для управления коллекцией поисковых индексов
    /// </summary>
    public class SearchIndexSet
    {
        /// <summary>
        /// Объект фабрики доступа к папке хранения индексов полнотекстового поиска
        /// </summary>
        public static LockFactory LockFactory = new NoLockFactory();

        /// <summary>
        /// Получает коллекцию поисковых индексов
        /// </summary>
        /// <returns>Возвращает коллекцию поисковых индексов</returns>
        public EfDatabaseIndexSet<DatabaseContext> GetIndex(Type modelType)
        {
            return InitializeIndexSet(modelType);
        }

        /// <summary>
        /// Инициализирует индексы поиска
        /// </summary>
        private EfDatabaseIndexSet<DatabaseContext> InitializeIndexSet(Type modelType)
        {
            var databaseContext = ObjectFactory.GetInstance<IDatabaseContext>() as DatabaseContext;
            var indexSet = new EfDatabaseIndexSet<DatabaseContext>(
                new SimpleFSDirectory(
                    new DirectoryInfo(string.Format("{0}/{1}",
                        Constants.SearchIndexDirectory,
                        modelType.Name)),
                    LockFactory),
                databaseContext);
            if (modelType == typeof(User) && !indexSet.Exists<User>()) indexSet.Add(typeof(User));
            return indexSet;
        }
    }
}