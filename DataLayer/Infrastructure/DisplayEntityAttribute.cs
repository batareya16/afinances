﻿using System;
using System.Resources;
using System.Globalization;

namespace DataLayer.Infrastructure
{
    /// <summary>
    /// Атрибут для привязки имени имени сущности к классу сущности
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DisplayEntityAttribute : Attribute
    {
        /// <summary>
        /// Тип ресурса
        /// </summary>
        private Type resource;

        /// <summary>
        /// Имя ключа для имени сущности
        /// </summary>
        private string keyName;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        /// <param name="resource">Тип ресурса</param>
        /// <param name="keyName">Имя ключа для имени сущности</param>
        public DisplayEntityAttribute(Type resource, string keyName)
        {
            this.resource = resource;
            this.keyName = keyName;
        }

        /// <summary>
        /// Получить имя сущности
        /// </summary>
        /// <returns>Возвращает имя сущности</returns>
        public string GetName()
        {
            ResourceManager manager = new ResourceManager(resource);
            return manager.GetString(keyName, CultureInfo.CurrentCulture);
        }
    }
}