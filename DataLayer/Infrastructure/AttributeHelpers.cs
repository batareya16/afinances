﻿using System;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Infrastructure
{
    /// <summary>
    /// Методы для работы с атрибутами
    /// </summary>
    public static class AttributeHelpers
    {
        /// <summary>
        /// Получает значение поля Name атрибута DisplayEntityAttribute у класса
        /// </summary>
        /// <param name="entityType">Тип класса, где надо получить значение поля</param>
        /// <returns>Возвращает значение поля Name атрибута DisplayEntityAttribute</returns>
        public static string GetEntityName(Type entityType)
        {
            MemberInfo memberInfo = entityType;
            object[] attributes = memberInfo.GetCustomAttributes(true);

            foreach (object attribute in attributes)
            {
                DisplayEntityAttribute entityAttribute = attribute as DisplayEntityAttribute;

                if (entityAttribute != null)
                    return entityAttribute.GetName();
            }

            return string.Empty;
        }

        /// <summary>
        /// Получает значение DisplayAttribute атрибута 
        /// </summary>
        /// <param name="type">Тип класса, где надо получить значение</param>
        /// <param name="propertyName">Имя свойства, где надо получить значение</param>
        /// <returns>Возвращает значение DisplayAttribute атрибута</returns>
        public static string GetDisplayName(Type type, string propertyName)
        {
            var fieldInfo = type.GetProperty(propertyName);

            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null) return string.Empty;
            return descriptionAttributes.Length > 0
                ? descriptionAttributes[0].ResourceType != null
                    ? lookupResource(descriptionAttributes[0].ResourceType, descriptionAttributes[0].Name)
                    : descriptionAttributes[0].Name
                : propertyName;
        }

        /// <summary>
        /// Достает по возможности значение из ресурса по ключу 
        /// </summary>
        /// <param name="resourceManagerProvider">Тип ресурса, где надо достать значение</param>
        /// <param name="resourceKey">Ключ, по которому надо вытащить значение ресурса</param>
        /// <returns>Возвращает значение из ресурса по ключу или сам ключ, если достать не удалось</returns>
        private static string lookupResource(Type resourceManagerProvider, string resourceKey)
        {
            foreach (PropertyInfo staticProperty in resourceManagerProvider.GetProperties())
            {
                if (staticProperty.PropertyType == typeof(System.Resources.ResourceManager))
                {
                    System.Resources.ResourceManager resourceManager =
                        (System.Resources.ResourceManager)staticProperty.GetValue(null, null);
                    return resourceManager.GetString(resourceKey);
                }
            }

            return resourceKey;
        }
    }
}