﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.Infrastructure
{
    /// <summary>
    /// Класс, который определяет методы-хелперы для полнотекстового поиска
    /// </summary>
    public static class SearchHelper
    {
        /// <summary>
        /// Преобразует входную поисковую строку в запрос для полнотекстового поиска Lucene
        /// </summary>
        /// <param name="configuration">Объект конфигурации поиска</param>
        /// <returns>Возвращает преобразованную строку в запрос для поиска</returns>
        public static string GenerateQuery(SearchConfiguration configuration)
        {
            return GenerateQuery(configuration, false);
        }

        /// <summary>
        /// Преобразует входную поисковую строку во Fuzzy запрос для полнотекстового поиска Lucene
        /// </summary>
        /// <param name="configuration">Объект конфигурации поиска</param>
        /// <returns>Возвращает преобразованную строку в запрос для поиска</returns>
        public static string GenerateFuzzyQuery(SearchConfiguration configuration)
        {
            return GenerateQuery(configuration, true);
        }

        /// <summary>
        /// Преобразует имя типа модели для поиска в объект типа
        /// </summary>
        /// <param name="name">Имя типа модели</param>
        /// <returns>Возвращает тип модели</returns>
        public static Type ParseModelType(this string name)
        {
            var type = Type.GetType(name, false, true);
            if (type == null) throw new ArgumentException("Parsing model type error! Please check searching models types!");
            return type;
        }

        /// <summary>
        /// Преобразует входную поисковую строку в запрос для полнотекстового поиска Lucene
        /// </summary>
        /// <param name="configuration">Объект конфигурации поиска</param>
        /// <param name="allFuzzySearch">Параметр, показывающий является ли поиск абсолютным Fuzzy поиском</param>
        /// <returns>Возвращает преобразованную строку в запрос для поиска</returns>
        private static string GenerateQuery(SearchConfiguration configuration, bool allFuzzySearch)
        {
            if (string.IsNullOrEmpty(configuration.Query)) return "*:*";
            var searchWords = GetSearchWords(configuration.Query);
            for (int i = 0; i < searchWords.Count; i++)
            {
                searchWords[i] += string.Format(
                    "~{0}{1}",
                    searchWords[i].Any(char.IsDigit) && !allFuzzySearch
                        ? Constants.NonFuzzySearchAccuracy
                        : Constants.FuzzySearchAccuracy,
                    i < searchWords.Count - 1
                        ? "+"
                        : string.Empty);
            }

            var query = string.Concat(searchWords);
            if (configuration.FieldsConditions != null)
                foreach (var item in configuration.FieldsConditions) query += string.Format(" AND {0}:\"{1}\"", item.Key, item.Value);
            if (configuration.ExcludeResults != null)
                foreach (var item in configuration.ExcludeResults) query += string.Format(" -\"{0}\"", item);

            return query;
        }

        private static List<string> GetSearchWords(string queryString)
        {
            var regex = new Regex(Constants.SearchUnAllowedSymbolsRegex);
            var processedQueryString = regex.Replace(queryString, string.Empty);
            return processedQueryString.Split(' ').ToList();
        }
    }
}