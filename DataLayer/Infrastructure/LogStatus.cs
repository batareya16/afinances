﻿namespace DataLayer.Infrastructure
{
    /// <summary>
    /// Статус(тип) добавляемого лога
    /// </summary>
    public enum LogStatus
    {
        /// <summary>
        /// Автоматический лог, нужен для журналирования программы
        /// </summary>
        AutoLog,

        /// <summary>
        /// Оповещение о ручном изменении сущностей базы данных
        /// </summary>
        ChangeEntity,

        /// <summary>
        /// Предупреждение
        /// </summary>
        Warning,

        /// <summary>
        /// Ошибка
        /// </summary>
        Error
    }
}