﻿using System;
using System.Data.Entity;
using DataLayer.Models;
using DataLayer.Interfaces;
using System.Threading.Tasks;

namespace DataLayer
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public DatabaseContext()
        {
            this.Configuration.ProxyCreationEnabled = false; 
        }

        /// <summary>
        /// Таблица бюджетов
        /// </summary>
        public DbSet<Budget> Budgets { get; set; }

        /// <summary>
        /// Таблица кредитов
        /// </summary>
        public DbSet<Credit> Credits { get; set; }

        /// <summary>
        /// Таблица обязательных платежей
        /// </summary>
        public DbSet<MandatoryCashOff> MandatoryCashOffs { get; set; }

        /// <summary>
        /// Таблица прибыли
        /// </summary>
        public DbSet<Income> Incomes { get; set; }

        /// <summary>
        /// Таблица логов
        /// </summary>
        public DbSet<Log> Logs { get; set; }

        /// <summary>
        /// Таблица ресурсов
        /// </summary>
        public DbSet<Resource> Resources { get; set; }

        /// <summary>
        /// Таблица ресурспаков
        /// </summary>
        public DbSet<ResourcePack> ResourcePacks { get; set; }

        /// <summary>
        /// Таблица типов ресурсов
        /// </summary>
        public DbSet<ResourceType> ResourceTypes { get; set; }

        /// <summary>
        /// Таблица затрат
        /// </summary>
        public DbSet<Spending> Spendings { get; set; }

        /// <summary>
        /// Таблица модулей
        /// </summary>
        public DbSet<Module> Modules { get; set; }

        /// <summary>
        /// Таблица модулей, привязанных к ресурсам
        /// </summary>
        public DbSet<ModuleResource> ModuleResources { get; set; }

        /// <summary>
        /// Таблица репозиториев модулей
        /// </summary>
        public DbSet<ModuleRepository> Repositories { get; set; }

        /// <summary>
        /// Таблица дефолтных ресурспаков, привязанных к пользователям
        /// </summary>
        public DbSet<CurrentResourcePack> CurrentUserResourcePacks { get; set; }

        /// <summary>
        /// Таблица групп пользователей
        /// </summary>
        public DbSet<UserGroup> UserGroups { get; set; }

        /// <summary>
        /// Таблица привязок пользователей к группам пользователей
        /// </summary>
        public DbSet<User_UserGroup> User_UserGroups { get; set; }

        /// <summary>
        /// Метод, который выполняется при инициализации(создании) базы даных
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public void InitializeDatabase(DatabaseContext context)
        {
            if (!context.Database.Exists() || !context.Database.CompatibleWithModel(false))
            {
                if (context.Database.Exists())
                {
                    context.Database.Delete();
                }
                context.Database.Create();

                //TODO: Check this!
                context.Database.ExecuteSqlCommand("CREATE INDEX IX_Budgets_Date ON dbo.Budgets ( BudgetDateTime )");
            }
        }

        public System.Threading.Tasks.Task<int> SaveChangesAsync()
        {
            base.SaveChanges();
            return new Task<int>(new Func<int>(() => { return 0; }));
        }
    }
}