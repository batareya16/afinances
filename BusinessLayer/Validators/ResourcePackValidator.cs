﻿using System.ComponentModel.DataAnnotations;
using BusinessLayer.Interfaces;
using StructureMap;
using System.Web.Security;
using BusinessLayer.Resources;

namespace BusinessLayer.Validators
{
    /// <summary>
    /// Класс, который содержит подклассы атрибутов-валидаторов для сущности ресурспака
    /// </summary>
    public class ResourcePackValidator
    {
        /// <summary>
        /// Валидатор, который проверяет нулевой идентификатор юзергруппы
        /// </summary>
        public class UserGroupRequired : ValidationAttribute
        {
            /// <summary>
            /// Базовый конструктор
            /// </summary>
            public UserGroupRequired()
            {
            }

            /// <summary>
            /// Проверяет владельца группы пользователей
            /// </summary>
            /// <param name="value">Упакованный идентификатор группы пользователя</param>
            /// <returns>Возвращает результат проверки владельца группы пользователей</returns>
            public override bool IsValid(object value)
            {
                return value != null & (long)value != 0;
            }

            /// <summary>
            /// Возвращает сообщение об ошибке проверки группы пользователей
            /// </summary>
            /// <param name="name">Имя поля (параметр не используется, служит для override)</param>
            /// <returns>Возвращает сообщение об ошибке проверки группы пользователей</returns>
            public override string FormatErrorMessage(string name)
            {
                return string.Format(Errors.Required, name);
            }
        }
    }
}