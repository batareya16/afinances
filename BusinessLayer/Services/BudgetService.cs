﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models;
using DataLayer.Interfaces;
using BusinessLayer.Entities;
using BusinessLayer.Infrastructure;
using BusinessLayer.Interfaces;
using StructureMap;
using DataLayer;
using AutoMapper;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с бюджетами
    /// </summary>
    /// <typeparam name="TE">Тип сущности денежного оборота</typeparam>
    public class BudgetService<TE> : IBudgetService<TE>
        where TE : GenericEntity
    {
        /// <summary>
        /// Репозиторий бюджета
        /// </summary>
        private IGenericRepository<Budget> budgetRepository;

        /// <summary>
        /// Репозиторий кредитов
        /// </summary>
        private IGenericRepository<Credit> creditRepository;

        /// <summary>
        /// Репозиторий ресурсов
        /// </summary>
        private IGenericRepository<Resource> resourceRepository;

        /// <summary>
        /// сервис ресурсов
        /// </summary>
        private IResourcesService resourceService;

        /// <summary>
        /// Репозиторий ресурспаков
        /// </summary>
        private IGenericRepository<ResourcePack> resourcePackRepository;

        /// <summary>
        /// Стандартный конструктор
        /// </summary>
        public BudgetService()
        {
            budgetRepository = ObjectFactory.GetInstance<IGenericRepository<Budget>>();
            creditRepository = ObjectFactory.GetInstance<IGenericRepository<Credit>>();
            resourceRepository = ObjectFactory.GetInstance<IGenericRepository<Resource>>();
            resourcePackRepository = ObjectFactory.GetInstance<IGenericRepository<ResourcePack>>();
            resourceService = ObjectFactory.GetInstance<IResourcesService>();
        }

        /// <summary>
        /// Обновляет значение бюджета при обновлении прибыли/убытка
        /// </summary>
        /// <param name="entity">Сущность прибыли/убытка</param>
        /// <param name="prevSumm">Сумма прибыли/затраты до изменения</param>
        public void UpdateMoneyTurnoverTrackBudget(TE entity, double prevSumm)
        {
            var moneyTurnover = entity as GenericMoneyTurnoverEntity;
            DateTime date = moneyTurnover.DateAndTime;
            while (date.Date <= DateTime.Today)
            {
                TrackBudgetOnSpecificDate(moneyTurnover, date, prevSumm);
                date = date.AddDays(1);
            }
        }

        /// <summary>
        /// Обновляет значение бюджета при добавлении списка прибылей/убытков
        /// </summary>
        /// <param name="entities">Лист сущностей прибыли/убытков</param>
        public void AddMoneyTurnoverTrackBudgets(List<GenericMoneyTurnoverEntity> entities)
        {
            if (!entities.Any()) return;
            var sortedEntities = new Stack<GenericMoneyTurnoverEntity>(entities.OrderByDescending(x => x.DateAndTime));

            var first = sortedEntities.Peek();
            var date = first.DateAndTime.Date;
            var currentSum = 0.0;
            while (date.Date <= DateTime.Today)
            {
                while (sortedEntities.Any() && sortedEntities.Peek().DateAndTime.Date <= date)
                    currentSum = sortedEntities.Peek() is IncomeRequestEntity
                        ? currentSum + Math.Abs(sortedEntities.Pop().Summ)
                        : currentSum - Math.Abs(sortedEntities.Pop().Summ);
                first.Summ = currentSum;
                TrackBudgetOnSpecificDate(first, date, 0);
                date = date.AddDays(1);
            }
        }

        /// <summary>
        /// Получает из базы данных лист кредитов ресурспака и рассчитывает актуальные значения суммы
        /// </summary>
        public IEnumerable<BudgetCreditEntity> GetActuatCreditsForResourcePackId(DateTime actualDate, long resourcePackId)
        {
            var resultCredits = new List<BudgetCreditEntity>();
            var creditEntities = creditRepository.Get(
                x => x.ResourcePackId == resourcePackId
                    && x.StartDate <= actualDate
                    && x.IsPayed == false);

            foreach (var credit in creditEntities)
            {
                if (credit.EndDate >= actualDate || !credit.IsPayed)
                {
                    int creditDays = credit.EndDate.Subtract(credit.StartDate).Days;
                    int daysToNow = credit.EndDate >= actualDate
                        ? actualDate.Subtract(credit.StartDate).Days
                        : creditDays;
                    credit.Summ = credit.Summ / creditDays * daysToNow;
                    var creditEntity = Mapper.Map<BudgetCreditEntity>(credit);
                    creditEntity.LeftToSubstract =
                        CreditHelper.GetLeftToSubstract(Mapper.Map<CreditRequestEntity>(credit));
                    resultCredits.Add(creditEntity);
                }
            }

            return resultCredits;
        }

        //private IEnumerable<BudgetCreditEntity> Calculate

        /// <summary>
        /// Получает актуальные на введенную дату (оптимально) значения бюджета сгруппированные по Ресурспакам
        /// </summary>
        /// <param name="maxDate">Дата для получения для нее бюджетов ресурспаков</param>
        /// <returns>Возвращает актуальные на введенную дату (оптимально) значения бюджета сгруппированные по Ресурспакам</returns>
        public IEnumerable<BudgetResourcePackEntity> GetBudgetResourcePacks(DateTime? maxDate = null)
        {
            DateTime currDate = (maxDate == null) ? DateTime.Today : (DateTime)maxDate;
            var resources = resourceRepository.GetWithInclude(x => x.ResourcePack);
            var resourcePacks = resources.Select(x => x.ResourcePack).GroupBy(x => x.Id, (key, group) => group.First()).ToList();
            resourcePacks.ForEach(x => x.Resources = resources.Where(res => res.ResourcePackId == x.Id));
            var budgetResourcePacks = new List<BudgetResourcePackEntity>();
            foreach (var resPack in resourcePacks) budgetResourcePacks.Add(GetActualBudgetByResourcePack(currDate, resPack));
            return budgetResourcePacks;
        }

        /// <summary>
        /// Получает значения бюджета ресурспака на определенное количество дней
        /// </summary>
        /// <param name="toDate">Конечная дата получения значений бюджета</param>
        /// <param name="dayCount">Количество дней, на которые надо получить значения бюджета ресурспака</param>
        /// <param name="ResourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает значения бюджета ресурспака на определенное количество дней</returns>
        public List<TimelyBudgetEntity> GetDailyBudgets(DateTime toDate, int dayCount, long ResourcePackId)
        {
            var resPack = resourcePackRepository.FindByIdWithInclude(ResourcePackId);
            resPack.Resources = resourceRepository.Get(x => x.ResourcePackId == ResourcePackId);
            List<TimelyBudgetEntity> dailyBudgets = new List<TimelyBudgetEntity>();
            for (int i = dayCount - 1; i >= 0; i--)
            {
                DateTime tempDate = toDate.AddDays(-i);
                var budgetResPack = GetActualBudgetByResourcePack(tempDate, resPack);
                if (budgetResPack != null)
                    dailyBudgets.Add(new TimelyBudgetEntity()
                    {
                        DateTime = tempDate,
                        Balance = budgetResPack.Balance,
                        SummaryCreditSum = budgetResPack.SummaryCredit,
                        Credits = budgetResPack.Credits
                    });
            }

            return dailyBudgets;
        }

        /// <summary>
        /// Получает значения бюджета ресурспака на определенное количество месяцев (Макс. допустимое кол-во - 24)
        /// </summary>
        /// <param name="fromMonth">Конечный месяц получения значений бюджета</param>
        /// <param name="monthCount">Количество месяцев, на которые надо получить значения бюджета ресурспака</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает значения бюджета ресурспака на определенное количество месяцев</returns>
        public List<TimelyBudgetEntity> GetMonthlyBudgets(DateTime toMonth, int monthCount, long resourcePackId)
        {
            if (monthCount > Constants.MaxMonthsBudgetsAvailableForRequest) monthCount = Constants.MaxMonthsBudgetsAvailableForRequest;
            List<TimelyBudgetEntity> monthlyBudgets = new List<TimelyBudgetEntity>();
            var resourcePack = resourcePackRepository.FindById(resourcePackId);
            resourcePack.Resources = resourceService.GetResourcePackResources(resourcePackId);
            for (int i = monthCount - 1; i >= 0; i--)
            {
                DateTime tempDate = toMonth.SubstractAndGetYearAndMonth(i);
                var monthlyCredits = GetAverageMonthlyCreditsFromDatabase(tempDate, resourcePack);
                var avgMonthBudget = GetAverageMonthBudgetFromDatabase(tempDate, resourcePack);
                monthlyBudgets.Add(new TimelyBudgetEntity()
                {
                    Balance = avgMonthBudget.Balance,
                    DateTime = tempDate,
                    SummaryCreditSum = monthlyCredits.Count() > 0 ? monthlyCredits.Average(x => x.LeftToSubstract) : 0,
                    Credits = monthlyCredits
                });
            }

            return monthlyBudgets;
        }

        /// <summary>
        /// Обновляет значение бюджета при обновлении прибыли/убытка на конкретную дату
        /// </summary>
        /// <param name="moneyTurnover">Сущность прибыли/убытка</param>
        /// <param name="date">Дата, на которую надо обновить бюджет</param>
        /// <param name="prevSumm">Сумма прибыли/затраты до изменения</param>
        private void TrackBudgetOnSpecificDate(GenericMoneyTurnoverEntity moneyTurnover, DateTime date, double prevSumm)
        {
            var budget = budgetRepository.Get(
                   x => x.BudgetDateTime == date
                   && x.ResourceId == moneyTurnover.ResourceId)
               .SingleOrDefault();

            var actualBudget = GetActualBudgetByResourceId(date, moneyTurnover.ResourceId);
            if (budget == null)
                budget = new Budget()
                {
                    ResourceId = moneyTurnover.ResourceId,
                    Balance = actualBudget != null ? actualBudget.Balance : 0
                };

            budget.BudgetDateTime = date;
            budget.Balance += moneyTurnover is IncomeRequestEntity
                                ? moneyTurnover.Summ - prevSumm
                                : moneyTurnover is SpendingRequestEntity
                                    ? prevSumm - Math.Abs(moneyTurnover.Summ)
                                    : 0;
            if (budget.Id > 0)
                budgetRepository.Update(budget);
            else
                budgetRepository.Create(budget);
        }

        /// <summary>
        /// Получает среднемесячные значения бюджета для определенного ресурспака на определенную дату (месяц)
        /// </summary>
        /// <param name="date">Дата(месяц), для которой надо получить среднемесячные значения бюджета</param>
        /// <param name="resourcePack">Ресурспак</param>
        /// <returns>Возвращает среднемесячные значения бюджета для определенного ресурспака на определенную дату (месяц)</returns>
        private BudgetResourcePackEntity GetAverageMonthBudgetFromDatabase(DateTime date, ResourcePack resourcePack)
        {
            var resourceIds = resourcePack.Resources.Select(x => x.Id);
            var firstDayInMonth = new DateTime(date.Year, date.Month, 1);
            var daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
            var budgetDate = new DateTime(date.Year, date.Month, daysInMonth);
            var dailyBudgetsFromDB = budgetRepository.Get(
                x => x.BudgetDateTime >= firstDayInMonth
                && x.BudgetDateTime <= budgetDate
                && resourceIds.Contains(x.ResourceId)).ToList();
            if (!dailyBudgetsFromDB.Any(x => x.BudgetDateTime == firstDayInMonth))
                dailyBudgetsFromDB.Add(Mapper.Map<Budget>(GetActualBudgetByResourcePack(firstDayInMonth, resourcePack)));

            Budget lastActualBudget = dailyBudgetsFromDB.First(x => x.BudgetDateTime == firstDayInMonth);
            List<Budget> dailyBudgets = new List<Budget>();
            for (int day = 1; day <= daysInMonth; day++)
            {
                var actualBudget = dailyBudgetsFromDB.FirstOrDefault(x => x.BudgetDateTime == new DateTime(date.Year, date.Month, day));
                if (actualBudget == null) actualBudget = lastActualBudget;
                lastActualBudget = actualBudget;
                dailyBudgets.Add(actualBudget);
            }
            return new BudgetResourcePackEntity()
            {
                ResourcePackId = resourcePack.Id,
                ResourcePackName = resourcePack.Name,
                Balance = dailyBudgets.Sum(x => x.Balance) / (double)daysInMonth
            };
        }

        /// <summary>
        /// Получает сумму, которую надо вычесть за кредиты на определенную дату
        /// </summary>
        /// <param name="date">Дата, на которую надо получить сумму</param>
        /// <param name="credits">Коллекция кредитов, для которой надо получить сумму</param>
        /// <returns>Возвращает сумму, которую надо вычесть за кредиты на определенную дату</returns>
        private double GetSummaryCreditSumForDate(DateTime date, IEnumerable<BudgetCreditEntity> credits)
        {
            return Mapper.Map<IEnumerable<CreditRequestEntity>>(credits)
                .Aggregate<CreditRequestEntity, double>(0, (x, y) => x + CreditHelper.GetLeftToSubstract(y, date));
        }

        /// <summary>
        /// Получает коллекцию кредитов для среднемесячной истории бюджета на определенный месяц
        /// </summary>
        /// <param name="date">Дата, на которую надо получить коллекцию кредитов (месяц)</param>
        /// <param name="resourcePack">Ресурспак</param>
        /// <returns>Возвращает коллекцию кредитов для среднемесячной истории бюджета на определенный месяц</returns>
        private IEnumerable<BudgetCreditEntity> GetAverageMonthlyCreditsFromDatabase(DateTime date, ResourcePack resourcePack)
        {
            var startDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            var endDate = new DateTime(date.Year, date.Month, 1);
            var credits = creditRepository.Get(x =>
                x.StartDate <= startDate
                && x.EndDate >= endDate
                && x.ResourcePackId == resourcePack.Id);
            return Mapper.Map<IEnumerable<BudgetCreditEntity>>(credits);
        }

        /// <summary>
        /// Получает актуальное значение бюджета на определенную дату для определенного ресурспака
        /// </summary>
        /// <param name="actualDate">Дата, на которую надо получить значение бюджета</param>
        /// <param name="resourcePack">Ресурспак</param>
        /// <returns>Возвращает объект бюджета ресурспака на определенную дату</returns>
        private BudgetResourcePackEntity GetActualBudgetByResourcePack(DateTime actualDate, ResourcePack resourcePack)
        {
            var budgets = new List<Budget>();
            foreach (var resource in resourcePack.Resources) budgets.Add(GetActualBudgetByResourceId(actualDate, resource.Id));
            var budgetResourcePack = new BudgetResourcePackEntity()
            {
                ResourcePackId = resourcePack.Id,
                Balance = budgets.Sum(s => s != null ? s.Balance : 0),
                ResourcePackName = resourcePack.Name,
                BudgetDateTime = actualDate
            };

            budgetResourcePack.Credits = GetActuatCreditsForResourcePackId(actualDate, budgetResourcePack.ResourcePackId).ToList();
            budgetResourcePack.SummaryCredit = budgetResourcePack.Credits.Where(x => !x.IsPayed).Sum(x => x.Summ);
            return budgetResourcePack;
        }

        /// <summary>
        /// Получает актуальное значение бюджета на определенную дату для определенного ресурса
        /// </summary>
        /// <param name="actualDate">Дата, на которую надо получить значение бюджета</param>
        /// <param name="resourceId">Идентификатор ресурса</param>
        /// <returns>Возвращает объект бюджета на определенную дату для определенного ресурса</returns>
        private Budget GetActualBudgetByResourceId(DateTime actualDate, long resourceId)
        {
            return budgetRepository.LastOrDefaultWithInclude<DateTime>(
                x => x.BudgetDateTime,
                x => x.ResourceId == resourceId && x.BudgetDateTime <= actualDate,
                x => x.Resource);
        }
    }
}