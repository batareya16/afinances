﻿using System.Collections.Generic;
using AutoMapper;
using BusinessLayer.Entities;
using BusinessLayer.Interfaces;
using DataLayer.Interfaces;
using DataLayer.Models;
using StructureMap;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис управления типами ресурсов
    /// </summary>
    public class ResourceTypeService : BaseEntityService<ResourceType, ResourceTypeEntity>, IResourceTypeService
    {
        /// <summary>
        /// Сервис управления текущим ресурспаком
        /// </summary>
        private ICurrentResourcePackService currentResourcePackService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public ResourceTypeService()
            : base(
            ObjectFactory.GetInstance<IGenericRepository<ResourceType>>(),
            ObjectFactory.GetInstance<ILoggingService>(),
            ObjectFactory.GetInstance<ISearchService>())
        {
            currentResourcePackService = ObjectFactory.GetInstance<ICurrentResourcePackService>();
        }

        /// <summary>
        /// Получить коллекцию типов ресурсов для группы пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        /// <returns>Возвращает коллекцию типов ресурсов для группы пользователей</returns>
        public IEnumerable<ResourceTypeEntity> GetForUserGroup(long userGroupId)
        {
            return Mapper.Map<IEnumerable<ResourceTypeEntity>>(EntityRepository.Get(x => x.UserGroupId == userGroupId));
        }

        /// <summary>
        /// Получает коллекцию типов ресурсов, привязанных к текущей группе пользователей
        /// </summary>
        /// <param name="userGroupId">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию типов ресурсов, привязанных к текущей группе пользователей</returns>
        public IEnumerable<ResourceTypeEntity> GetForCurrentUserGroup(string userName)
        {
            var userGroupId = currentResourcePackService.GetCurrentResourcePack(userName).CurrentUserGroupId.Value;
            return GetForUserGroup(userGroupId);
        }
    }
}