﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.Entities.Scheduler;
using DataLayer.Models;
using StructureMap;
using BusinessLayer.Interfaces;
using BusinessLayer.Entities;
using AutoMapper;
using System.IO;
using DataLayer.Interfaces;
using DataLayer;
using BusinessLayer.Infrastructure;
using BusinessLayer.Entities.Module;
using ModuleIncome = BusinessLayer.Entities.Module.Income;
using IncomeEntity = DataLayer.Models.Income;
using Newtonsoft.Json;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с модулями
    /// </summary>
    public class ModuleService : IModuleService
    {
        /// <summary>
        /// Объект блокировки для доступа к БД при сохранении прибыли из работы модуля
        /// </summary>
        private static object locker = new object();

        /// <summary>
        /// Сервис для работы с прибылью
        /// </summary>
        private ICashFlowService<IncomeEntity, IncomeRequestEntity> incomeService;

        /// <summary>
        /// Сервис для работы с убытками
        /// </summary>
        private ICashFlowService<Spending, SpendingRequestEntity> spendingService;

        /// <summary>
        /// Сервис для работы с бюджетом
        /// </summary>
        private IBudgetService<IncomeRequestEntity> budgetService;

        /// <summary>
        /// Сервис для работы с модулями
        /// </summary>
        private IGenericRepository<Module> moduleRepository;

        /// <summary>
        /// Сервис для работы с модулями, привязанными к ресурсам
        /// </summary>
        private IGenericRepository<ModuleResource> moduleResourceRepository;

        /// <summary>
        /// Сервис для работы с планировщиком задач модулей
        /// </summary>
        private ISchedulerService schedulerService;

        /// <summary>
        /// Сервис для работы с песочницами
        /// </summary>
        private ISandboxService sandboxService;

        /// <summary>
        /// Сервис для работы с ресурспаками
        /// </summary>
        private IResourcePackService resourcePackService;

        /// <summary>
        /// Сервис для работы с текущим ресурспаком
        /// </summary>
        private ICurrentResourcePackService currentResourcePackService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public ModuleService()
        {
            incomeService = ObjectFactory.GetInstance<ICashFlowService<IncomeEntity, IncomeRequestEntity>>();
            spendingService = ObjectFactory.GetInstance<ICashFlowService<Spending, SpendingRequestEntity>>();
            schedulerService = ObjectFactory.GetInstance<ISchedulerService>();
            sandboxService = ObjectFactory.GetInstance<ISandboxService>();
            moduleRepository = ObjectFactory.GetInstance<IGenericRepository<Module>>();
            moduleResourceRepository = ObjectFactory.GetInstance<IGenericRepository<ModuleResource>>();
            resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            budgetService = ObjectFactory.GetInstance<IBudgetService<IncomeRequestEntity>>();
            currentResourcePackService = ObjectFactory.GetInstance<ICurrentResourcePackService>();
        }

        /// <summary>
        /// Добавляет модуль из файла .DLL в папку модулей и создает запись в БД
        /// </summary>
        /// <param name="file">Отправленный файл через POST-запрос</param>
        /// <returns>Возвращает сущность созданного модуля</returns>
        public Module AddModuleFromFile(HttpPostedFileBase file)
        {
            var path = Constants.ModulesDirectoryPath + Path.GetFileNameWithoutExtension(file.FileName);
            string moduleName = Path.GetFileNameWithoutExtension(file.FileName) + ".dll";
            foreach (var item in moduleRepository.GetWithTracking(x => x.Name == moduleName))
            {
                moduleRepository.Remove(item);
            }

            IArchiver archiver = new ZipArchiver();
            var moduleFilePath = Path.Combine(path, moduleName);
            if (archiver.Unpack(file.InputStream, path) && File.Exists(moduleFilePath))
            {
                var module = GetModuleInformation(moduleFilePath);
                if (module != null)
                {
                    module.Name = moduleName;
                    moduleRepository.Create(module);
                    return module;
                }
            }

            if (Directory.Exists(path)) Directory.Delete(path, true);
            return null;
        }

        /// <summary>
        /// Удаляет запись модуля и зависимые сущности из БД, удаляет файл модуля, а также останавливает все работы
        /// </summary>
        /// <param name="id">Идентификатор модуля</param>
        public void RemoveModule(long id)
        {
            var module = moduleRepository.FindByIdWithTracking(id);
            var moduleResources = moduleResourceRepository.GetWithTracking(x => x.ModuleId == module.Id);
            foreach (var item in moduleResources)
            {
                schedulerService.RemoveTimerJob(item.Id);
            }

            var modulePath = GetModulePathByName(module.Name);
            sandboxService.DisposeSandboxes(modulePath);
            moduleRepository.Remove(module);
            if (Directory.Exists(Path.GetDirectoryName(modulePath)))
            {
                Directory.Delete(Path.GetDirectoryName(modulePath), true);
            }
        }

        /// <summary>
        /// Получает страницу списка установленных модулей
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает коллекцию установленных модулей</returns>
        public IEnumerable<Module> GetModules(int page)
        {
            return moduleRepository.Page(page, x => true);
        }

        /// <summary>
        /// Получает модуль, привязанный к ресурсу из БД по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает объект модуля, привязанного к ресурсу</returns>
        public ModuleResource GetModuleResource(long id)
        {
            var moduleResource = moduleResourceRepository.FindById(id);
            moduleResource.AdditionalConfiguration =
                ModuleAdditConfigHelper.ProcessConfigToDisplay(moduleResource.AdditionalConfiguration);
            return moduleResource;
        }

        /// <summary>
        /// Получает модуль, привязанный к ресурсу из БД по идентификатору модуля и ресурса
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="resourceId">Идентификатор ресурса</param>
        /// <returns>Возвращает объект модуля, привязанного к ресурсу</returns>
        public ModuleResource GetModuleResource(long moduleId, long resourceId)
        {
            var moduleResource = moduleResourceRepository
                .Get(x => x.ModuleId == moduleId && x.ResourceId == resourceId)
                .Single();
            moduleResource.AdditionalConfiguration =
                ModuleAdditConfigHelper.ProcessConfigToDisplay(moduleResource.AdditionalConfiguration);
            return moduleResource;
        }

        /// <summary>
        /// Получает коллекцию ресурспаков, к которым привязан определенный модуль
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <returns>Возвращает коллекцию ресурспаков, к которым привязан определенный модуль</returns>
        public IEnumerable<ResourcePackEntity> GetModuleLinkedResourcePacks(long moduleId)
        {
            var moduleResourcepacksIds = moduleResourceRepository
                .GetWithInclude(x => x.ModuleId == moduleId, x => x.Resource)
                .Select(x => x.Resource.ResourcePackId)
                .Distinct();
            return Mapper.Map<IEnumerable<ResourcePackEntity>>(resourcePackService.GetByIds(moduleResourcepacksIds));
        }

        /// <summary>
        /// Получает определенный модуль по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор модуля</param>
        /// <returns>Возвращает объект модуля</returns>
        public Module GetModule(long id)
        {
            return moduleRepository.FindById(id);
        }

        /// <summary>
        /// Получает список модулей, привязанных к ресурсу по идентификатору модуля в рамках группы пользователей
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="includeTracking">Флаг, который показывает надо ли трекать объекты при получении.
        /// Например, для последующей их модификации</param>
        /// <remarks>
        /// Аккуратно использовать данный метод! AdditionalData в нем не подменяется
        /// При отдаче данных пользователю предусмотреть подмену AdditionalData как в "GetModuleResource"
        /// </remarks>
        /// <returns>Возвращает коллекцию модулей, привязанных к ресурсу</returns>
        public IEnumerable<ModuleResource> GetModuleModuleResources(long moduleId, string userName, bool includeTracking = false)
        {
            var resourcesIds = resourcePackService.GetResourcesForCurrentUserGroup(userName).Select(x => x.Id);
            return includeTracking
                ? moduleResourceRepository.GetWithTracking(x => x.ModuleId == moduleId && resourcesIds.Contains(x.ResourceId))
                : moduleResourceRepository.Get(x => x.ModuleId == moduleId && resourcesIds.Contains(x.ResourceId));
        }

        /// <summary>
        /// Привязывает модуль к списку ресурсов
        /// </summary>
        /// <param name="resourcesIds">Список идентификаторов ресурсов, к которым требуется привязать модуль</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="moduleId">Идентификатор модуля</param>
        public void LinkModuleToResources(IEnumerable<long> resourcesIds, string userName, long moduleId)
        {
            var module = GetModule(moduleId);
            var moduleConfiguration = GetModuleConfiguration(GetModulePathByName(module.Name));
            if (moduleConfiguration == null) return;
            moduleConfiguration.ModuleId = moduleId;
            var existingModuleResources = GetModuleModuleResources(moduleId, userName, includeTracking: true);
            var existingModuleResourcesIds = existingModuleResources.Select(x => x.ResourceId);
            var resourcesToAdd = resourcesIds.Where(x => !existingModuleResourcesIds.Contains(x));
            foreach (var resourceId in resourcesToAdd)
            {
                LinkModuleToResource(moduleConfiguration, resourceId);
            }
            var resourcesToRemove = existingModuleResourcesIds.Except(resourcesIds);
            foreach (var resourceId in resourcesToRemove)
            {
                moduleResourceRepository.Remove(existingModuleResources.Single(x => x.ResourceId == resourceId));
            }
        }

        /// <summary>
        /// Обновляет модуль, привязанный к ресурсу
        /// </summary>
        /// <param name="config">Объект конфигурации модуля</param>
        /// <remarks>
        /// Аккуратно использовать данный метод! AdditionalData в нем не подменяется
        /// При отдаче данных пользователю предусмотреть подмену AdditionalData как в "GetModuleResource"
        /// </remarks>
        /// <returns>Возвращает обновленный объект конфигурации модуля</returns>
        public ModuleConfiguration UpdateResourceModule(ModuleConfiguration config)
        {
            var moduleResource = UpdateModuleResourceInner(config);
            moduleResource.Module = moduleRepository.FindById(config.ModuleId);
            schedulerService.RemoveTimerJob(moduleResource.Id);
            if (moduleResource.IsActive) CreateModuleTimerJob(moduleResource);
            return Mapper.Map<ModuleConfiguration>(moduleResource);
        }

        /// <summary>
        /// Инициализирует модуль, привязанный к ресурсу
        /// </summary>
        /// <param name="module">Объект модуля, привязанного к ресурсу</param>
        public void InitializeResourceModule(ModuleResource module)
        {
            CreateModuleTimerJob(module);
        }

        /// <summary>
        /// Получает остаток времени до начала работы модуля, привязанного к ресурсу
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает количество миллисекунд до начала работы модуля, привязанного к ресурсу</returns>
        public long GetModuleResourceTimerJobRemain(long moduleResourceId)
        {
            return schedulerService.GetTimerJobRemain(moduleResourceId);
        }

        /// <summary>
        /// Немедленно запускает работу модуля, привязанного к ресурсу
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает false, если планировщик на нашел задачу,
        /// или true - если планировщик пересоздал задачу успешно</returns>
        public bool ImmediatelyRunJob(long moduleResourceId)
        {
            if (schedulerService.GetTimerJobRemain(moduleResourceId) == -1) return false;
            schedulerService.RemoveTimerJob(moduleResourceId);
            var moduleResource = moduleResourceRepository.FindByIdWithInclude(moduleResourceId, x => x.Module);
            CreateModuleTimerJob(moduleResource);
            return true;
        }

        /// <summary>
        /// Запуск активированных модулей
        /// </summary>
        public void ActivateModules()
        {
            var moduleResources = GetActiveModuleResources();
            foreach (var moduleResource in moduleResources)
            {
                InitializeResourceModule(moduleResource);
            }
        }

        /// <summary>
        /// Получает список активных модулей, привязанных к ресурсам
        /// </summary>
        /// <remarks>
        /// Аккуратно использовать данный метод! AdditionalData в нем не подменяется
        /// При отдаче данных пользователю предусмотреть подмену AdditionalData как в "GetModuleResource"
        /// </remarks>
        /// <returns>Возвращает коллекцию всех активных модулей, привязанных к ресурсам</returns>
        private IEnumerable<ModuleResource> GetActiveModuleResources()
        {
            return moduleResourceRepository.GetWithInclude(x => x.IsActive, x => x.Module);
        }

        /// <summary>
        /// Обновляет только модуль, привязанный к ресурсу (без доп. действий вроде сброса таймеров)
        /// </summary>
        /// <param name="config">Объект конфигурации модуля</param>
        /// <param name="isFromModule">Флаг, который показывает, происходит ли обновление из-за внешнего модуля</param>
        /// <returns>Возвращает обновленный объект модуля, привязанного к ресурсу</returns>
        private ModuleResource UpdateModuleResourceInner(ModuleConfiguration config, bool isFromModule = false)
        {
            var moduleResource = moduleResourceRepository
                .GetWithTracking(x => x.ModuleId == config.ModuleId && x.ResourceId == config.ResourceId)
                .SingleOrDefault();

            if (moduleResource != null)
            {
                moduleResource.Hours = config.Hours;
                moduleResource.Minutes = config.Minutes;
                moduleResource.IsActive = config.IsActive;
                moduleResource.AdditionalConfiguration = ModuleAdditConfigHelper.ProcessConfigToSave(
                    config.AdditionalConfiguration,
                    moduleResource.AdditionalConfiguration,
                    isFromModule);
                moduleResource.Module = null;
                moduleResourceRepository.Update(moduleResource);
            }

            return moduleResource;
        }

        /// <summary>
        /// Привязывает модуль к ресурсу
        /// </summary>
        /// <param name="moduleConfiguration">Сущность конфигурации модуля</param>
        /// <param name="resourceId">Идентификатор ресурса</param>
        private void LinkModuleToResource(ModuleConfiguration moduleConfiguration, long resourceId)
        {
            if (!ValidationHelper.IsValid<ModuleConfiguration>(moduleConfiguration)
                || !ValidationHelper.IsModuleConfigTimeValid(moduleConfiguration)) return;
            var moduleResource = new ModuleResource()
            {
                ModuleId = moduleConfiguration.ModuleId,
                ResourceId = resourceId,
                IsDelayedJob = moduleConfiguration.IsDelayedJob,
                Hours = moduleConfiguration.Hours,
                Minutes = moduleConfiguration.Minutes,
                IsActive = false,
                AdditionalConfiguration = moduleConfiguration.AdditionalConfiguration
            };
            moduleResourceRepository.Create(moduleResource);
        }

        /// <summary>
        /// Получает путь к файлу модуля по имени модуля
        /// </summary>
        /// <param name="moduleName">Строка имени модуля</param>
        /// <returns>Возвращает строку пути к файлу модуля</returns>
        private string GetModulePathByName(string moduleName)
        {
            return Path.Combine(
                Constants.ModulesDirectoryPath,
                Path.GetFileNameWithoutExtension(moduleName),
                moduleName);
        }

        /// <summary>
        /// Создает работу модуля, привязанного к ресурсу в планировщике задач
        /// </summary>
        /// <param name="module">Объект модуля, привязанного к ресурсу</param>
        private void CreateModuleTimerJob(ModuleResource module)
        {
            if (!module.IsActive) return;
            var job = new TimerJob()
            {
                ModuleResourceId = module.Id,
                TimerCallbackAction = () => GetDataFromModule(module.ResourceId, module.ModuleId, module.Module.Name),
                TimerDelay = new TimeSpan(module.Hours, module.Minutes, 0),
                StartTimer = DateTime.Now
            };
            schedulerService.CreateTimerJob(job);
        }

        /// <summary>
        /// Получает конфигурацию из модуля и десериализует ее в объект модуля
        /// </summary>
        /// <param name="modulePath">Путь к файлу модуля</param>
        /// <returns>Возвращает десериализованный объект модуля</returns>
        private Module GetModuleInformation(string modulePath)
        {
            var serializedModuleConfig = ParseModuleConfiguration(modulePath);
            return serializedModuleConfig == null
                ? null
                : CommonHelper.DeserializeFromXML<Module>(serializedModuleConfig, Constants.ModuleClassesNames.ModuleConfiguration);
        }

        /// <summary>
        /// Получает конфигурацию из модуля и десериализует ее в сущность конфигурации модуля
        /// </summary>
        /// <param name="modulePath">Путь к файлу модуля</param>
        /// <returns>Возвращает десериализованный объект конфигурации модуля</returns>
        private ModuleConfiguration GetModuleConfiguration(string modulePath)
        {
            return ConvertConfigurationFromModule(ParseModuleConfiguration(modulePath));
        }

        /// <summary>
        /// Десериализует конфигурацию из модуля
        /// </summary>
        /// <param name="serializedModuleConfig">Сериализзованная строка конфигурации модуля</param>
        /// <returns>Возвращает десериализованный объект конфигурации модуля</returns>
        private ModuleConfiguration ConvertConfigurationFromModule(string serializedModuleConfig)
        {
            if (serializedModuleConfig == null) return null;
            var config = CommonHelper.DeserializeFromXML<ModuleConfiguration>(
                            serializedModuleConfig,
                            Constants.ModuleClassesNames.ModuleConfiguration);
            var additionalParams = Serializer.GetCustomConfigurationNodesJson(
                Serializer.GetJsonConfigurationProp(
                    Serializer.XmlToJson(serializedModuleConfig)));
            config.AdditionalConfiguration = additionalParams;
            return config;
        }

        /// <summary>
        /// Получает конфигурацию из модуля, обернутого в песочницу
        /// </summary>
        /// <param name="modulePath">Путь к файлу модуля</param>
        /// <returns>Возвращает сериализованный объект конфигурации</returns>
        private string ParseModuleConfiguration(string modulePath)
        {
            try
            {
                var sandbox = sandboxService.CreateSandbox(modulePath);
                var assemblyName = sandbox.AssemblyName;
                var emptyArr = new object[0];
                var result = sandbox.Executor.Execute(
                    modulePath,
                    sandbox.AssemblyName + ".Configuration",
                    "GetConfiguration",
                    ref emptyArr);
                sandboxService.DisposeSandbox(sandbox);
                return (string)result;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Получает данные прибыли/убытков из модуля, оборачивая модуль в песочницу
        /// </summary>
        /// <param name="resourceId">Идентификатор ресурса</param>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="moduleName">Имя модуля</param>
        private void GetDataFromModule(long resourceId, long moduleId, string moduleName)
        {
            try
            {
                var modulePath = GetModulePathByName(moduleName);
                var sandbox = sandboxService.CreateSandbox(modulePath);
                ParseAndAddDataFromAssembly(resourceId, moduleId, sandbox);
                sandboxService.DisposeSandbox(sandbox);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Получает данные прибыли/убытков из модуля в песочнице
        /// </summary>
        /// <param name="resourceId">Идентификатор ресурса</param>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="sandbox">Сущность песочницы</param>
        private void ParseAndAddDataFromAssembly(long resourceId, long moduleId, SandboxEntity sandbox)
        {
            var assemblyName = sandbox.AssemblyName;
            var moduleResource = GetDecryptedModuleResource(moduleId, resourceId);
            var config = Mapper.Map<ModuleConfiguration>(moduleResource);
            Mapper.Map(moduleResource.Module, config);
            var executorParameters = new object[] { SerializeModuleConfigurationForModuleSending(config) };
            var receivedXml = sandbox.Executor.Execute(sandbox.Name, assemblyName + ".Main", "GetData", ref executorParameters) as string;
            var receivedCollection = Serializer.Deserialize<List<ModuleIncome>>(receivedXml);
            if (receivedCollection == null) throw new InvalidCastException("Received data from module cannot be deserialized");

            var collectionForBudgetUpdate = new List<GenericMoneyTurnoverEntity>();
            foreach (var income in receivedCollection)
            {
                income.ResourceId = resourceId;
                lock (locker)
                {
                    if (income.Summ >= 0)
                    {
                        bool valid;
                        var incomeEntity = MapAndValidateReceivedIncome<IncomeRequestEntity>(income, out valid);
                        if (valid)
                        {
                            collectionForBudgetUpdate.Add(incomeEntity);
                            incomeService.Add(incomeEntity, Constants.SystemUsername, preventUpdatingBudget: true);
                        }
                    }
                    else
                    {
                        bool valid;
                        var spendingEntity = MapAndValidateReceivedIncome<SpendingRequestEntity>(income, out valid);
                        if (valid)
                        {
                            collectionForBudgetUpdate.Add(spendingEntity);
                            spendingService.Add(spendingEntity, Constants.SystemUsername, preventUpdatingBudget: true);
                        }
                    }
                }
            }

            UpdateModuleResourceAfterModuleJob(config, executorParameters[0] as string);
            budgetService.AddMoneyTurnoverTrackBudgets(collectionForBudgetUpdate);
        }

        /// <summary>
        /// Обновляет доп. конфигурацию модуля после выполнения работы модуля
        /// </summary>
        /// <param name="originalModuleCfg">Конфиг. модуля до обновления работы (который в БД)</param>
        /// <param name="newModuleCfgAsXML">Конфиг. модуля, который опционально мог обновить модуль в виде XML</param>
        private void UpdateModuleResourceAfterModuleJob(ModuleConfiguration originalModuleCfg, string newModuleCfgAsXML)
        {
            var newModuleCfg = ConvertConfigurationFromModule(newModuleCfgAsXML);
            originalModuleCfg.AdditionalConfiguration = newModuleCfg.AdditionalConfiguration;
            UpdateModuleResourceInner(originalModuleCfg, isFromModule: true);
        }

        /// <summary>
        /// Сериализует конфигурацию модуля для отправки модулю при получении прибыли/убытков
        /// </summary>
        /// <returns>Возвращает сериализованный объект конфигурации модуля</returns>
        private string SerializeModuleConfigurationForModuleSending(ModuleConfiguration moduleCfg)
        {
            var configStr =
                Serializer.RemoveNodesFromCfgToModuleSending(
                Serializer.MergeJsons(JsonConvert.SerializeObject(moduleCfg), moduleCfg.AdditionalConfiguration));
            return Serializer.JsonToXml(string.Format("{{ Configuration: {0} }}", configStr));
        }

        /// <summary>
        /// Получает конфигурацию модуля, привязанного к ресурсу и дешифрует его доп.конфигурацию
        /// (Нужно только для отправки модулю через ReflectionExecutor)
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="resourceId">Идентификатор ресурса</param>
        /// <returns>Возвращает дешифрованный объект модуля, привязанного к ресурсу</returns>
        private ModuleResource GetDecryptedModuleResource(long moduleId, long resourceId)
        {
            var moduleResource = moduleResourceRepository
                .GetWithInclude(x => x.ModuleId == moduleId && x.ResourceId == resourceId, x => x.Module)
                .Single();
            moduleResource.AdditionalConfiguration =
                Serializer.DecryptEncryptedConfigurationNodes(moduleResource.AdditionalConfiguration);
            return moduleResource;
        }

        /// <summary>
        /// Маппит и валидирует принятую сущность прибыли/убытка из модуля
        /// </summary>
        /// <typeparam name="T">Тип, в который надо замаппить (прибыль или убыток)</typeparam>
        /// <param name="receivedIncome">Принятый из модуля объект прибыли/убытка</param>
        /// <param name="valid">Флаг. который показывает, валиден ли принятый объект прибыли/убытка</param>
        /// <returns>Возвращает замапленный объект прибыли/убытка из модуля
        /// (валидность устанавливается во входной параметр "valid")</returns>
        private T MapAndValidateReceivedIncome<T>(ModuleIncome receivedIncome, out bool valid)
            where T: GenericMoneyTurnoverEntity
        {
            var entity = Mapper.Map<T>(receivedIncome);
            // Need because in 'GenericMoneyTurnoverEntity' ResourcePackId property - Required
            entity.ResourcePackId = 0;
            entity.Summ = Math.Abs(entity.Summ);
            entity.DateAndTime = entity.DateAndTime.Date;
            valid = ValidationHelper.IsValid<T>(entity);
            return entity;
        }
    }
}
