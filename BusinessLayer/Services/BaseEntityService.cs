﻿using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Interfaces;
using DataLayer.Models;
using DataLayer.Interfaces;
using BusinessLayer.Entities;
using AutoMapper;
using Lucene.Linq.Mapping;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Базовый сервис для сущностей
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    /// <typeparam name="TE">Тип бизнес-модели сущности</typeparam>
    public class BaseEntityService<T,TE> : IBaseEntityService<T,TE>
        where T: BaseModel
        where TE: GenericEntity
    {
        /// <summary>
        /// Показывает, участвует ли данный тип сущности в полнотекстовом поиске 
        /// </summary>
        protected bool IsSearchableEntity { get; set; }

        /// <summary>
        /// Репозиторий сущности, к которой привязан сервис
        /// </summary>
        protected IGenericRepository<T> EntityRepository { get; set; }

        /// <summary>
        /// Сервис полнотекстового поиска
        /// </summary>
        protected ISearchService SearchService;

        /// <summary>
        /// Сервис для логгирования
        /// </summary>
        protected ILoggingService LoggingService { get; set; }

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        /// <param name="entityRepository">Репозиторий сущности</param>
        /// <param name="loggingService">Сервис для логгирования</param>
        /// <param name="searchable">Значение, показывающее участвует ли тип сущности в полнотекстовом поиске</param>
        public BaseEntityService(
            IGenericRepository<T> entityRepository,
            ILoggingService loggingService,
            ISearchService searchService)
        {
            EntityRepository = entityRepository;
            LoggingService = loggingService;
            SearchService = searchService;
            IsSearchableEntity = isHaveModelTypeLuceneDocumentAttribute();
        }

        /// <summary>
        /// Добавляет сущность в таблицу БД
        /// </summary>
        /// <param name="entity">Сущность для добавления</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <returns>Возвращает ИД сущности</returns>
        public virtual long Add(TE entity, string userName)
        {
            var model = Mapper.Map<T>(entity);
            EntityRepository.Create(model);
            LoggingService.TrackCreatingObject(model, userName);
            if (IsSearchableEntity) SearchService.AddToIndex(model);
            return model.Id;
        }

        /// <summary>
        /// удаляет сущность из таблицы БД
        /// </summary>
        /// <param name="entity">Сущность для удаления</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public virtual void Remove(long id, string userName)
        {
            var entity = EntityRepository.FindByIdWithTracking(id);
            EntityRepository.Remove(entity);
            LoggingService.TrackRemovingObject(entity, userName);
            if (IsSearchableEntity) SearchService.RemoveFromIndex(entity);
        }

        /// <summary>
        /// обновляет сущность в таблице БД
        /// </summary>
        /// <param name="entity">Сущность для обновления</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public virtual void Update(TE entity, string userName)
        {
            var oldEntity = EntityRepository.FindById(entity.Id);
            var newEntity = Mapper.Map<T>(entity);
            EntityRepository.Update(newEntity);
            LoggingService.TrackObjectChanges(oldEntity, newEntity, userName);
            if (IsSearchableEntity) SearchService.UpdateInIndex(oldEntity, newEntity);
        }

        /// <summary>
        /// Получает сущность по идентификатору с зависимыми сущностями
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        public virtual T FindByIdWithRelatedData(long id)
        {
            return FindById(id);
        }

        /// <summary>
        /// Находит сущность по уникальному идентификатору
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <returns>Возвращает сущность</returns>
        public T FindById(long id)
        {
            return EntityRepository.FindById(id);
        }

        /// <summary>
        /// Находит список сущностей в базе данных согласно списку идентификаторов сущностей
        /// </summary>
        /// <param name="ids">Список идентификаторов сущностей</param>
        /// <returns>Возвращает коллекцию сущностей</returns>
        public IEnumerable<T> GetByIds(IEnumerable<long> ids)
        {
            return EntityRepository.Get(x => ids.Contains(x.Id));
        }

        /// <summary>
        /// Проверяет, есть ли у типа модели сервиса атрибут для полнотекстового поиска
        /// </summary>
        /// <returns>Возвращает логическое значение, показывающее есть ли у типа модели сервиса атрибут для полнотекстового поиска</returns>
        private bool isHaveModelTypeLuceneDocumentAttribute()
        {
            return typeof(T).GetCustomAttributes(typeof(DocumentAttribute), inherit: true).Any();
        }
    }
}