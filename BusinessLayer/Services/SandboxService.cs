﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using BusinessLayer.Infrastructure;
using BusinessLayer.Interfaces;
using DataLayer;
using BusinessLayer.Entities;
using System.Runtime.Remoting;
using System.IO;
using System.Net;
using System.Web;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис управления песочницами подключаемых модулей
    /// </summary>
    public class SandboxService : ISandboxService
    {
        /// <summary>
        /// Пул объектов песочниц
        /// </summary>
        private static List<SandboxEntity> sandboxesPool = new List<SandboxEntity>();

        /// <summary>
        /// Выполнить метод внутри песочницы
        /// </summary>
        /// <param name="modulePath">Путь к подключаемому модулю</param>
        /// <param name="typeName">Тип объекта, в котором надо выполнить метод</param>
        /// <param name="method">Имя метода для выполнения</param>
        /// <param name="parameters">Параметры, передаваемые методу (по ссылке - ref)</param>
        /// <returns>Возвращает результат выполнения метода внутри песочницы</returns>
        public object SandboxExecute(string modulePath, string typeName, string method, params object[] parameters)
        {
            if (sandboxesPool.All(x => x.Name != modulePath)) throw new KeyNotFoundException(string.Format("Sandboxes pool does not contains \"{0}\" module", modulePath));
            var sandbox = sandboxesPool.First(x => x.Name == modulePath);
            var exec = sandbox.Executor;
            var result = exec.Execute(modulePath, typeName, method, ref parameters);
            return result;
        }

        /// <summary>
        /// Создает новый объект песочницы в пуле песочниц
        /// </summary>
        /// <param name="modulePath">Путь к подключаемому модулю</param>
        /// <returns>Возвращает объект созданной песочницы</returns>
        public SandboxEntity CreateSandbox(string modulePath)
        {
            var setup = new AppDomainSetup();
            setup.ApplicationBase = modulePath;

            var permSet = new PermissionSet(PermissionState.Unrestricted);
            var domain = AppDomain.CreateDomain(modulePath, null, setup, permSet);
            ObjectHandle handle = Activator.CreateInstanceFrom(
                domain,
                typeof(ReflectionExecutor).Assembly.ManifestModule.FullyQualifiedName, typeof(ReflectionExecutor).FullName);
            ReflectionExecutor exec = (ReflectionExecutor)handle.Unwrap();
            exec.LoadAssembly(modulePath);
            var sandbox = new SandboxEntity(domain, exec, Path.GetFileNameWithoutExtension(modulePath), modulePath);
            sandboxesPool.Add(sandbox);
            return sandbox;
        }

        /// <summary>
        /// Удаляет объект песочницы из пула песочниц
        /// </summary>
        /// <param name="sandbox">Объект песочницы для удаления</param>
        public void DisposeSandbox(SandboxEntity sandbox)
        {
            sandboxesPool.Remove(sandbox);
            if (sandboxesPool.All(x => x.Name != sandbox.Name)) AppDomain.Unload(sandbox.Domain);
        }

        /// <summary>
        /// Удаляет объект песочниц из пула песочниц по имени сборки подключаемого модуля
        /// </summary>
        /// <param name="assemblyName">Имя сборки подключаемого модуля</param>
        public void DisposeSandboxes(string assemblyName)
        {
            var sandboxes = sandboxesPool.Where(x => x.Name == assemblyName);
            if (sandboxes.Any())
            {
                sandboxesPool.RemoveAll(item => sandboxes.Contains(item));
                AppDomain.Unload(sandboxes.First().Domain);
            }

        }
    }
}