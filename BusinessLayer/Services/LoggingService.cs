﻿using System;
using System.Collections.Generic;
using System.Linq;
using KellermanSoftware.CompareNetObjects;
using DataLayer.Interfaces;
using DataLayer.Models;
using StructureMap;
using BusinessLayer.Interfaces;
using DataLayer.Infrastructure;
using BusinessLayer.Resources;
using System.Reflection;
using System.Text.RegularExpressions;
using DataLayer;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с логами
    /// </summary>
    public class LoggingService : ILoggingService
    {
        /// <summary>
        /// Репозиторий логов
        /// </summary>
        private IGenericRepository<Log> logsRepository;

        /// <summary>
        /// Сервис полнотекстового поиска
        /// </summary>
        private ISearchService searchService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public LoggingService()
        {
            this.logsRepository = ObjectFactory.GetInstance<IGenericRepository<Log>>();
            searchService = ObjectFactory.GetInstance<ISearchService>();
        }

        /// <summary>
        /// Зафиксировать изменения полей объекта в базе данных
        /// </summary>
        /// <param name="from">Исходный объект</param>
        /// <param name="to">Измененный объект</param>
        /// <param name="userName">Имя инициатора лога</param>
        public void TrackObjectChanges(object from, object to, string userName)
        {
            if (from.GetType() != to.GetType()) throw new InvalidCastException();
            CompareLogic compareLogic = new CompareLogic();
            compareLogic.Config.MaxDifferences = Int32.MaxValue;
            ComparisonResult result = compareLogic.Compare(from, to);
            if (!result.AreEqual)
            {
                string log = string.Format(Strings.EntityHasBeenChanged, AttributeHelpers.GetEntityName(from.GetType()));
                foreach (var item in result.Differences)
                {
                    log += string.Format(
                        Strings.FieldHasBeenChanged,
                        AttributeHelpers.GetDisplayName(item.ParentObject1.GetType(), item.PropertyName),
                        item.Object1Value,
                        item.Object2Value);
                }

                CreateLog(log, userName);
            }
        }

        /// <summary>
        /// Зафиксировать создание нового объекта в базе даныых
        /// </summary>
        /// <param name="obj">Создаваемый объект</param>
        /// <param name="userName">Имя инициатора лога</param>
        public void TrackCreatingObject(object obj, string userName)
        {
            string log = string.Format(Strings.CreatedEntity, AttributeHelpers.GetEntityName(obj.GetType()));
            var objectType = obj.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(objectType.GetProperties());

            foreach (PropertyInfo prop in props.Where(
                p => !Regex.IsMatch(p.Name, Constants.PropertyNameUnallowedLoggingRegex)
                    && !p.GetCustomAttributes(typeof(NotMappedAttribute), inherit: false).Any()
                    && (p.PropertyType.IsPrimitive
                    || p.PropertyType.Equals(typeof(string)))))
            {
                string stringValue = prop.GetValue(obj, null).ToString();
                log += string.Format(
                        Strings.EntityField,
                        AttributeHelpers.GetDisplayName(objectType, prop.Name),
                        stringValue);
            }

            CreateLog(log, userName);
        }

        /// <summary>
        /// Зафиксировать удаление объекта из базы даныых
        /// </summary>
        /// <param name="obj">Удаленный объект</param>
        /// <param name="userName">Имя инициатора лога</param>
        public void TrackRemovingObject(object obj, string userName)
        {
            string log = string.Format(Strings.RemovedEntity, AttributeHelpers.GetEntityName(obj.GetType()));
            var objectType = obj.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(objectType.GetProperties());

            foreach (PropertyInfo prop in props.Where(
                p => !Regex.IsMatch(p.Name, Constants.PropertyNameUnallowedLoggingRegex)
                    && (p.PropertyType.IsPrimitive
                    || p.PropertyType.Equals(typeof(string)))))
            {
                string stringValue = prop.GetValue(obj, null).ToString();
                log += string.Format(
                        Strings.EntityField,
                        AttributeHelpers.GetDisplayName(objectType, prop.Name),
                        stringValue);
            }

            CreateLog(log, userName);
        }

        /// <summary>
        /// Создает лог в базе данных
        /// </summary>
        /// <param name="log">Строка лога</param>
        /// <param name="userName">Имя инициатора лога</param>
        private void CreateLog(string log, string userName)
        {
            var result = new Log()
            {
                DateAndTime = DateTime.Now,
                Description = log,
                UserName = userName,
                LogStatus = LogStatus.ChangeEntity
            };
            logsRepository.Create(result);
            searchService.AddToIndex(result);
        }

        /// <summary>
        /// Получить "страницу" логов пользователя
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public IEnumerable<Log> GetPageOfLogsOfUser(string userName, int page)
        {
            return logsRepository.Page(page, x => x.UserName == userName);
        }
    }
}