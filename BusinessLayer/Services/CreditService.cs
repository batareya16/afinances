﻿using System;
using System.Collections.Generic;
using StructureMap;
using DataLayer.Interfaces;
using DataLayer.Models;
using BusinessLayer.Entities;
using AutoMapper;
using BusinessLayer.Interfaces;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с кредитами
    /// </summary>
    public class CreditService : CashFlowService<Credit, CreditRequestEntity>, ICreditService
    {
        /// <summary>
        /// Репозиторий ресурсов
        /// </summary>
        private IGenericRepository<Resource> resourcesRepository;

        /// <summary>
        /// Стандартный конструктор
        /// </summary>
        public CreditService()
            : base(
            ObjectFactory.GetInstance<IGenericRepository<Credit>>(),
            ObjectFactory.GetInstance<ILoggingService>(),
            ObjectFactory.GetInstance<IGenericRepository<Credit>>(),
            ObjectFactory.GetInstance<IBudgetService<CreditRequestEntity>>(),
            ObjectFactory.GetInstance<ISearchService>())
        {
            resourcesRepository = ObjectFactory.GetInstance<IGenericRepository<Resource>>();
        }

        /// <summary>
        /// Получает кредит по уникальному идентификатору с зависимыми полями
        /// </summary>
        /// <param name="id">Идентификатор кредита</param>
        /// <returns>Возвращает объект кредита</returns>
        public override Credit FindByIdWithRelatedData(long id)
        {
            return EntityRepository.FindByIdWithInclude(id, x => x.ResourcePack);
        }

        /// <summary>
        /// Метод-заглушка (Не реализовано)
        /// </summary>
        public override IEnumerable<Credit> GetDaily(DateTime toDate, int dayCount, long resourcePackId, long? resourceId = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Достает страницу кредитов, учитывая идентификатор ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает страницу значений кредитов ресурспака</returns>
        public IEnumerable<Credit> GetPageByResourcePackId(long resourcePackId, int page)
        {
            return EntityRepository.Page(page, x => x.ResourcePackId == resourcePackId);
        }

        /// <summary>
        /// Ставит флаг 'IsPayed' кредиту по идентификатору
        /// </summary>
        /// <param name="creditId">Идентификатор кредита</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public void CloseCredit(long creditId, string userName)
        {
            var credit = Mapper.Map<CreditRequestEntity>(FindById(creditId));
            credit.EndDate = DateTime.Now;
            credit.IsPayed = true;
            Update(credit, userName);
        }
    }
}