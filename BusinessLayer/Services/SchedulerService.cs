﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using BusinessLayer.Entities.Scheduler;
using BusinessLayer.Interfaces;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис планировщика работ модулей
    /// </summary>
    public class SchedulerService : ISchedulerService
    {
        /// <summary>
        /// Пул объектов таймеров - работ модулей
        /// </summary>
        private static Dictionary<TimerJob, Timer> timersPool = new Dictionary<TimerJob, Timer>();

        /// <summary>
        /// Создает таймер работы модуля и добавляет в пул таймеров
        /// </summary>
        /// <param name="job">Сущность работы таймера</param>
        public void CreateTimerJob(TimerJob job)
        {
            if (timersPool.Any(x => x.Key.ModuleResourceId == job.ModuleResourceId))
                throw new ArgumentException("This module-resource already exists in Timer jobs pool");
            TimerCallback callback = new TimerCallback((object obj) => job.TimerCallbackAction());
            Timer timer = new Timer(callback, null, TimeSpan.Zero, job.TimerDelay);
            timersPool.Add(job, timer);
        }

        /// <summary>
        /// Удаляет таймер работы модуля из пула таймеров
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        public void RemoveTimerJob(long moduleResourceId)
        {
            var job = GetTimerJob(moduleResourceId);
            if (job == null) return;
            var timer = timersPool[job];
            timersPool.Remove(job);
            timer.Dispose();
        }

        /// <summary>
        /// Получает остаток времени до начала работы таймера
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает остаток времени в миллисекундах до начала работы таймера</returns>
        public long GetTimerJobRemain(long moduleResourceId)
        {
            var timerJob = GetTimerJob(moduleResourceId);
            if (timerJob == null) return -1;
            var endJobTime = timerJob.StartTimer;
            while (endJobTime < DateTime.Now) endJobTime += timerJob.TimerDelay;
            return Convert.ToInt64((endJobTime - DateTime.Now).TotalMilliseconds);
        }

        /// <summary>
        /// Получает таймер из пула таймеров по идентификатору модуля, привязанного к ресурсу
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает объект работы таймера</returns>
        private TimerJob GetTimerJob(long moduleResourceId)
        {
            return timersPool.FirstOrDefault(x => x.Key.ModuleResourceId == moduleResourceId).Key;
        }
    }
}