﻿using DataLayer.Models;
using DataLayer.Interfaces;
using StructureMap;
using BusinessLayer.Interfaces;
using BusinessLayer.Entities;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис ресурспаков
    /// </summary>
    public class ResourcePackService : BaseEntityService<ResourcePack, ResourcePackEntity>, IResourcePackService
    {
        /// <summary>
        /// Репозиторий ресурсов
        /// </summary>
        private IGenericRepository<Resource> resourceRepository;

        /// <summary>
        /// Репозиторий пользователей, привязанных к группам пользователей (связки пользователь - группа пользователей)
        /// </summary>
        private IGenericRepository<User_UserGroup> userUserGroupRepository;

        /// <summary>
        /// Сервис управления текущим ресурспаком
        /// </summary>
        private ICurrentResourcePackService currentResourcePackService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public ResourcePackService()
            : base(
            ObjectFactory.GetInstance<IGenericRepository<ResourcePack>>(),
            ObjectFactory.GetInstance<ILoggingService>(),
            ObjectFactory.GetInstance<ISearchService>())
        {
            resourceRepository = ObjectFactory.GetInstance<IGenericRepository<Resource>>();
            userUserGroupRepository = ObjectFactory.GetInstance<IGenericRepository<User_UserGroup>>();
            currentResourcePackService = ObjectFactory.GetInstance<ICurrentResourcePackService>();
        }

        /// <summary>
        /// Находит ресурспак в базе данных по идентификатору ресурса
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурса</param>
        /// <returns>Возвращает объект ресурспака</returns>
        public ResourcePack GetByResource(long resourceId)
        {
            return resourceRepository.FindByIdWithInclude(resourceId, x => x.ResourcePack).ResourcePack;
        }

        /// <summary>
        /// Находит ресурспак в базе данных по идентификатору ресурса с зависимыми полями
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурса</param>
        /// <returns>Возвращает объект ресурспака</returns>
        public ResourcePack GetByResourceWithRelatedData(long resourceId)
        {
            return resourceRepository.FindByIdWithInclude(
                resourceId,
                x => x.ResourcePack,
                x => x.ResourcePack.UserGroup)
                .ResourcePack;
        }

        /// <summary>
        /// Получает страницу данных ресурспаков из базы данных
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию ресурспаков</returns>
        public IEnumerable<ResourcePack> GetPage(int page, string userName)
        {
            var userGroupsIds = userUserGroupRepository.Get(x => x.UserName == userName).Select(x => x.UserGroupId);
            return EntityRepository.Page(page, x => userGroupsIds.Contains(x.UserGroupId));
        }

        /// <summary>
        /// Получает коллекцию ресурспаков, привязанных к определенной группе пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        /// <remarks>
        /// Аккуратно использовать данный метод!
        /// При вычетке не учитывается наличие пользователя в данной группе пользователей.
        /// При отдаче данных пользователю предусмотреть это!
        /// </remarks>
        /// <returns>Возвращает коллекцию ресурспаков, привязанных к определенной группе пользователей</returns>
        public IEnumerable<ResourcePackEntity> GetUserGroupResourcePacks(long userGroupId)
        {
            return Mapper.Map<IEnumerable<ResourcePackEntity>>(
                EntityRepository.Get(x => x.UserGroupId == userGroupId));
        }

        /// <summary>
        /// Получает коллекцию ресурспаков, привязанных к текущей группе пользователей
        /// </summary>
        /// <param name="userGroupId">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию ресурспаков, привязанных к текущей группе пользователей</returns>
        public IEnumerable<ResourcePackEntity> GetForCurrentUserGroup(string userName)
        {
            var userGroupId = currentResourcePackService.GetCurrentResourcePack(userName).CurrentUserGroupId.Value;
            return GetUserGroupResourcePacks(userGroupId);
        }

        /// <summary>
        /// Получает коллекцию ресурсов, привязанных к текущей группе пользователей
        /// </summary>
        /// <param name="userGroupId">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию ресурсов, привязанных к текущей группе пользователей</returns>
        public IEnumerable<ResourceEntity> GetResourcesForCurrentUserGroup(string userName)
        {
            var userGroupId = currentResourcePackService.GetCurrentResourcePack(userName).CurrentUserGroupId.Value;
            return Mapper.Map<IEnumerable<ResourceEntity>>(
                EntityRepository.GetWithInclude(x => x.UserGroupId == userGroupId, x => x.Resources).SelectMany(x => x.Resources));
        }

        /// <summary>
        /// Получает сущность ресурспака с зависимыми полями
        /// </summary>
        /// <param name="id">Идентификатор ресурспака</param>
        /// <returns>Возвращает сущность ресурспака с зависимыми полями</returns>
        public override ResourcePack FindByIdWithRelatedData(long id)
        {
            return EntityRepository.FindByIdWithInclude(id, x => x.UserGroup);
        }

        /// <summary>
        /// Обновляет сущность ресурспака в базе данных
        /// </summary>
        /// <param name="entity">Сущность ресурспака</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override void Update(ResourcePackEntity entity, string userName)
        {
            entity.UserGroup = null;
            entity.UserGroupId = FindById(entity.Id).UserGroupId;
            base.Update(entity, userName);
        }

        /// <summary>
        /// Добавляет сущность ресурспака в базу данных
        /// </summary>
        /// <param name="entity">Сущность ресурспака</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <returns>Возвращает идентификатор созданного ресурспака</returns>
        public override long Add(ResourcePackEntity entity, string userName)
        {
            entity.UserGroup = null;
            return base.Add(entity, userName);
        }
    }
}