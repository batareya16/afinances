﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Interfaces;
using DataLayer.Models;
using StructureMap;
using BusinessLayer.Entities;
using AutoMapper;
using BusinessLayer.Interfaces;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с затратами
    /// </summary>
    public class SpendingService :
        CashFlowService<Spending, SpendingRequestEntity>,
        ICashFlowService<Spending, SpendingRequestEntity>
    {
        

        /// <summary>
        /// Стандартный конструктор
        /// </summary>
        public SpendingService()
            : base(
            ObjectFactory.GetInstance<IGenericRepository<Credit>>(),
            ObjectFactory.GetInstance<ILoggingService>(),
            ObjectFactory.GetInstance<IGenericRepository<Spending>>(),
            ObjectFactory.GetInstance<IBudgetService<SpendingRequestEntity>>(),
            ObjectFactory.GetInstance<ISearchService>())
        {
        }

        /// <summary>
        /// Обновляет объект убытка в базе данных
        /// </summary>
        /// <param name="income">Объект убытка</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override void Update(SpendingRequestEntity spending, string userName)
        {
            var oldSpending = EntityRepository.FindById(spending.Id);
            BudgetService.UpdateMoneyTurnoverTrackBudget(spending, oldSpending.Price);
            base.Update(spending, userName);
        }

        /// <summary>
        /// Удаляет объект расхода из базы данных
        /// </summary>
        /// <param name="id">Идентификатор расхода</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override void Remove(long id, string userName)
        {
            var spending = EntityRepository.FindById(id);
            var spendingPrice = spending.Price;
            spending.Price = 0;
            BudgetService.UpdateMoneyTurnoverTrackBudget(Mapper.Map<SpendingRequestEntity>(spending), spendingPrice);
            base.Remove(id, userName);
        }

        /// </summary>
        /// <param name="id">Идентификатор расхода</param>
        /// <returns>Возвращает объект расхода</returns>
        public override Spending FindByIdWithRelatedData(long id)
        {
            return EntityRepository.FindByIdWithInclude(id, x => x.Resource);
        }

        /// <summary>
        /// Получает значения затрат ресурспака или ресурса на определенное количество дней
        /// </summary>
        /// <param name="toDate">Конечная дата получения значений затрат</param>
        /// <param name="dayCount">Количество дней, на которые надо получить значения затрат</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="resourceId">Идентификатор ресурса (оптимально)</param>
        /// <returns>Возвращает значения затрат ресурспака или ресурса на определенное количество дней</returns>
        public override IEnumerable<Spending> GetDaily(DateTime toDate, int dayCount, long resourcePackId, long? resourceId = null)
        {
            var fromDate = toDate.AddDays(-dayCount);
            var resourcePackSpendings = EntityRepository.GetWithInclude(x =>
                x.DateAndTime <= toDate
                && x.DateAndTime > fromDate
                && x.Resource.ResourcePackId == resourcePackId,
                x => x.Resource);

            return resourceId != null && resourceId != 0
                ? resourcePackSpendings.Where(x => x.ResourceId == resourceId)
                : resourcePackSpendings;
        }

        /// <summary>
        /// Добавить сущность в базу данных
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override long Add(SpendingRequestEntity entity, string userName)
        {
            return Add(entity, userName, false);
        }

        /// <summary>
        /// Добавить сущность в базу данных
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="preventUpdatingBudget">Предотвратить обновление бюджета</param>
        public override long Add(SpendingRequestEntity entity, string userName, bool preventUpdatingBudget)
        {
            var newEntity = Mapper.Map<Spending>(entity);
            EntityRepository.Create(newEntity);
            if (!preventUpdatingBudget) BudgetService.UpdateMoneyTurnoverTrackBudget(entity, 0);
            if (entity.DateAndTime < DateTime.Today)
                LoggingService.TrackCreatingObject(newEntity, userName);
            if (IsSearchableEntity) SearchService.AddToIndex(newEntity);
            return newEntity.Id;
        }
    }
}