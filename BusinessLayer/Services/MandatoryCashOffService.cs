﻿using System;
using System.Collections.Generic;
using DataLayer.Models;
using DataLayer.Interfaces;
using StructureMap;
using BusinessLayer.Interfaces;
using BusinessLayer.Entities;
using AutoMapper;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис запланированных платежей
    /// </summary>
    public class MandatoryCashOffService : CashFlowService<MandatoryCashOff, MandatoryCashOffEntity>, IMandatoryCashOffService
    {
        /// <summary>
        /// Репозиторий ресурсов
        /// </summary>
        private IGenericRepository<Resource> resourceRepository;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public MandatoryCashOffService()
            : base(
                ObjectFactory.GetInstance<IGenericRepository<Credit>>(),
                ObjectFactory.GetInstance<ILoggingService>(),
                ObjectFactory.GetInstance<IGenericRepository<MandatoryCashOff>>(),
                ObjectFactory.GetInstance<IBudgetService<MandatoryCashOffEntity>>(),
                ObjectFactory.GetInstance<ISearchService>())
        {
            resourceRepository = ObjectFactory.GetInstance<IGenericRepository<Resource>>();
        }

        /// <summary>
        /// Получает значения запланированных платежей ресурспака на определенное количество дней
        /// </summary>
        /// <param name="toDate">Конечная дата получения значений</param>
        /// <param name="dayCount">Количество дней, на которые надо получить значения</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="resourceId">Идентификатор ресурса (заглушка)</param>
        /// <returns>Возвращает значения запланированных платежей ресурспака на определенное количество дней</returns>
        public override IEnumerable<MandatoryCashOff> GetDaily(DateTime toDate, int dayCount, long resourcePackId, long? resourceId = null)
        {
            var fromDate = toDate.AddDays(-dayCount);
            return EntityRepository.Get(x =>
                x.Date <= toDate
                && x.Date > fromDate
                && x.ResourcePackId == resourcePackId);
        }

        /// <summary>
        /// Добавить запл.платеж определенного ресурспака в базу данных
        /// </summary>
        /// <param name="request">Сущность запл. платежа</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override long Add(MandatoryCashOffEntity request, string userName)
        {
            var mandatoryCashOff = Mapper.Map<MandatoryCashOff>(request);
            EntityRepository.Create(mandatoryCashOff);
            if (IsSearchableEntity) SearchService.AddToIndex(mandatoryCashOff);
            return mandatoryCashOff.Id;
        }

        /// <summary>
        /// Находит запл.платеж по уникальному идентификатору c зависимыми полями
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Возвращает сущность запл.платежа</returns>
        public override MandatoryCashOff FindByIdWithRelatedData(long id)
        {
            return EntityRepository.FindByIdWithInclude(id, x => x.ResourcePack);
        }

        /// <summary>
        /// Достает страницу обязательных платежей, учитывая идентификатор ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает страницу значений запланированных платежей ресурспака</returns>
        public IEnumerable<MandatoryCashOff> GetPageByResourcePackId(long resourcePackId, int page)
        {
            return EntityRepository.Page(page, x => x.ResourcePackId == resourcePackId);
        }
    }
}