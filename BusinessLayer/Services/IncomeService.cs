﻿using System;
using System.Collections.Generic;
using System.Linq;
using StructureMap;
using DataLayer.Interfaces;
using DataLayer.Models;
using BusinessLayer.Entities;
using AutoMapper;
using BusinessLayer.Interfaces;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с прибылью
    /// </summary>
    public class IncomeService :
        CashFlowService<Income, IncomeRequestEntity>,
        ICashFlowService<Income, IncomeRequestEntity>
    {
        /// <summary>
        /// Стандартный конструктор
        /// </summary>
        public IncomeService()
            : base(
            ObjectFactory.GetInstance<IGenericRepository<Credit>>(),
            ObjectFactory.GetInstance<ILoggingService>(),
            ObjectFactory.GetInstance<IGenericRepository<Income>>(),
            ObjectFactory.GetInstance<IBudgetService<IncomeRequestEntity>>(),
            ObjectFactory.GetInstance<ISearchService>())
        {
        }

        /// <summary>
        /// Находит прибыль по уникальному идентификатору c зависимыми полями
        /// </summary>
        /// <param name="id">Идентификатор прибыли</param>
        /// <returns>Возвращает объект прибыли</returns>
        public override Income FindByIdWithRelatedData(long id)
        {
            return EntityRepository.FindByIdWithInclude(id, x => x.Resource);
        }

        /// <summary>
        /// Обновляет объект прибыли в базе данных
        /// </summary>
        /// <param name="income">Объект прибыли</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override void Update(IncomeRequestEntity income, string userName)
        {
            var oldIncome = EntityRepository.FindById(income.Id);
            BudgetService.UpdateMoneyTurnoverTrackBudget(income, oldIncome.Summ);
            base.Update(income, userName);
        }

        /// <summary>
        /// Удаляет объект прибыли из базы данных
        /// </summary>
        /// <param name="id">Идентификатор прибыли</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override void Remove(long id, string userName)
        {
            var income = EntityRepository.FindById(id);
            var incomeSumm = income.Summ;
            income.Summ = 0;
            BudgetService.UpdateMoneyTurnoverTrackBudget(Mapper.Map<IncomeRequestEntity>(income), incomeSumm);
            base.Remove(id, userName);
        }

        /// <summary>
        /// Получает значения прибыли ресурспака или ресурса на определенное количество дней
        /// </summary>
        /// <param name="toDate">Конечная дата получения значений прибыли</param>
        /// <param name="dayCount">Количество дней, на которые надо получить значения прибыли</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="resourceId">Идентификатор ресурса (оптимально)</param>
        /// <returns>Возвращает значения прибыли ресурспака или ресурса на определенное количество дней</returns>
        public override IEnumerable<Income> GetDaily(DateTime toDate, int dayCount, long resourcePackId, long? resourceId = null)
        {
            var fromDate = toDate.AddDays(-dayCount);
            var resourcePackIncomes = EntityRepository.GetWithInclude(x =>
                x.DateAndTime <= toDate
                && x.DateAndTime > fromDate
                && x.Resource.ResourcePackId == resourcePackId,
                x => x.Resource);

            return resourceId != null && resourceId != 0
                ? resourcePackIncomes.Where(x => x.ResourceId == resourceId)
                : resourcePackIncomes;
        }

        /// <summary>
        /// Добавить сущность в базу данных
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override long Add(IncomeRequestEntity entity, string userName)
        {
            return Add(entity, userName, false);
        }

        /// <summary>
        /// Добавить сущность в базу данных
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="preventUpdatingBudget">Предотвратить обновление бюджета</param>
        public override long Add(IncomeRequestEntity entity, string userName, bool preventUpdatingBudget)
        {
            var newEntity = Mapper.Map<Income>(entity);
            EntityRepository.Create(newEntity);
            if(!preventUpdatingBudget) BudgetService.UpdateMoneyTurnoverTrackBudget(entity, 0);
            if (entity.DateAndTime < DateTime.Today)
                LoggingService.TrackCreatingObject(newEntity, userName);
            if (IsSearchableEntity) SearchService.AddToIndex(newEntity);
            return newEntity.Id;
        }
    }
}