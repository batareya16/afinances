﻿using System.Collections.Generic;
using DataLayer.Models;
using DataLayer.Interfaces;
using StructureMap;
using BusinessLayer.Interfaces;
using BusinessLayer.Entities;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис ресурсов
    /// </summary>
    public class ResourcesService :
        BaseEntityService<Resource,ResourceEntity>,
        IResourcesService
    {
        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public ResourcesService()
            :base(
            ObjectFactory.GetInstance<IGenericRepository<Resource>>(),
            ObjectFactory.GetInstance<ILoggingService>(),
            ObjectFactory.GetInstance<ISearchService>())
        {
        }

        /// <summary>
        /// Обновляет сущность ресурса в базе данных
        /// </summary>
        /// <param name="entity">Сущность ресурса</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override void Update(ResourceEntity entity, string userName)
        {
            entity.ResourcePackId = FindById(entity.Id).ResourcePackId;
            base.Update(entity, userName);
        }

        /// <summary>
        /// Получает все ресурсы для данного ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает список ресурсов</returns>
        public IEnumerable<Resource> GetResourcePackResources(long resourcePackId)
        {
            return EntityRepository.Get(x => x.ResourcePackId == resourcePackId);
        }

        /// <summary>
        /// Получает ресурс из базы данных с зависимыми полями
        /// </summary>
        /// <param name="id">Идентификатор ресурса</param>
        /// <returns>Возвращает ресурс</returns>
        public override Resource FindByIdWithRelatedData(long id)
        {
            return EntityRepository.FindByIdWithInclude(id, x => x.ResourcePack, x => x.ResourceType);
        }
    }
}