﻿using System.Collections.Generic;
using System.Linq;
using DataLayer.Interfaces;
using DataLayer.Models;
using BusinessLayer.Entities.User;
using StructureMap;
using BusinessLayer.Interfaces;
using AutoMapper;
using BusinessLayer.Entities;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с группами пользователей
    /// </summary>
    public class UserGroupService : BaseEntityService<UserGroup, UserGroupEntity>, IUserGroupService
    {
        /// <summary>
        /// Репозиторий привязки пользователь - группа пользователей
        /// </summary>
        private IGenericRepository<User_UserGroup> userUserGroupRepository;

        /// <summary>
        /// Стандартный конструктор
        /// </summary>
        public UserGroupService()
            : base(
            ObjectFactory.GetInstance<IGenericRepository<UserGroup>>(),
            ObjectFactory.GetInstance<ILoggingService>(),
            ObjectFactory.GetInstance<ISearchService>())
        {
            userUserGroupRepository = ObjectFactory.GetInstance<IGenericRepository<User_UserGroup>>();
        }

        /// <summary>
        /// Добавляет сущность группы пользователей, а также зависимые привязки пользователь-группа пользователей в БД
        /// </summary>
        /// <param name="entity">Сущность группы пользователей</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <returns>Возвращает идентификатор созданной группы пользователей</returns>
        public override long Add(UserGroupEntity entity, string userName)
        {
            SetupUserGroupForCreate(entity, userName);
            return base.Add(entity, userName);
        }

        /// <summary>
        /// Обновляет сущность группы пользователей, а также зависимые привязки пользователь-группа пользователей в БД
        /// </summary>
        /// <param name="entity">Сущность группы пользователей</param>
        /// <param name="userName">Имя текущего пользователя</param>
        public override void Update(UserGroupEntity entity, string userName)
        {
            UpdateUserGroupUsers(entity, userName);
            UpdateUserGroupSearchIndex(entity);
            base.Update(entity, userName);
        }

        /// <summary>
        /// Получает группу пользователей из базы данных с зависимыми сущностями пользователей
        /// </summary>
        /// <param name="id">Идентификатор группы пользователей</param>
        /// <returns>Возвращает сущность группы пользователей</returns>
        public UserGroupEntity FindByIdWithRelatedUsers(long id)
        {
            var userGroup = Mapper.Map<UserGroupEntity>(base.FindByIdWithRelatedData(id));
            userGroup.Users = Mapper.Map<IEnumerable<BaseUserEntity>>(userUserGroupRepository.Get(x => x.UserGroupId == userGroup.Id));
            return userGroup;
        }

        /// <summary>
        /// Получает страницу групп пользователей, привязанных к конкретному пользователю
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="userName">Имя пользователя, для которого надо получить группы</param>
        /// <returns>Возвращает страницу коллекции групп пользователей</returns>
        public IEnumerable<UserGroupEntity> GetPageForUser(int page, string userName)
        {
            return Mapper.Map<IEnumerable<UserGroupEntity>>(userUserGroupRepository
                .PageWithInclude(page, x => x.UserName == userName, x => x.UserGroup)
                .GroupBy(x => x.UserGroupId).Select(x => x.First().UserGroup));
        }

        /// <summary>
        /// Получает значение, которое показывает, входит ли данный пользователь в группу пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Возвращает значение, которое показывает, входит ли данный пользователь в группу пользователей</returns>
        public bool ContainsUserGroupUser(long userGroupId, string userName)
        {
            return userUserGroupRepository.Any(x => x.UserGroupId == userGroupId && x.UserName == userName);
        }

        /// <summary>
        /// Добавляет в БД связки группы пользователей - пользователь, устанавливает необходимые значения для создания группы пользователей
        /// </summary>
        /// <param name="entity">Сущность группы пользователей</param>
        /// <param name="currUserName">Имя пользователя</param>
        private void SetupUserGroupForCreate(UserGroupEntity entity, string currUserName)
        {
            entity.OwnerUsername = currUserName;
            entity.User_UserGroups = entity.Users
                .Select(x => new User_UserGroup() { UserGroupId = entity.Id, UserName = x.UserName })
                .ToList();

            if (!entity.Users.Any(x => x.UserName == currUserName))
            {
                entity.User_UserGroups.Add(new User_UserGroup() { UserGroupId = entity.Id, UserName = currUserName });
            }
        }

        /// <summary>
        /// Обновляет пользователей, привязанных к группе пользователей
        /// </summary>
        /// <param name="entity">Сущность группы пользователей</param>
        /// <param name="currUserName">Имя пользователя</param>
        private void UpdateUserGroupUsers(UserGroupEntity entity, string currUserName)
        {
            entity.OwnerUsername = currUserName;
            var userNames = entity.Users.Select(x => x.UserName).ToList();
            if (!userNames.Contains(currUserName)) userNames.Add(currUserName);
            var oldUserUserGroups = userUserGroupRepository
                .Get(x => x.UserGroupId == entity.Id)
                .ToList();

            entity.User_UserGroups = userNames
                .Select(x =>
                {
                    var oldUserGroup = oldUserUserGroups.FirstOrDefault(gr => gr.UserName == x);
                    return new User_UserGroup() {
                        UserGroupId = entity.Id,
                        UserName = x,
                        Id = oldUserGroup != null ? oldUserGroup.Id : 0
                    };
                }).ToArray();

            var itemsIdsToRemove = oldUserUserGroups.Where(x => !userNames.Contains(x.UserName)).Select(x => x.Id);
            userUserGroupRepository.Remove(group => itemsIdsToRemove.Contains(group.Id));
            userUserGroupRepository.CreateOrUpdate((User_UserGroup[])entity.User_UserGroups);
        }

        /// <summary>
        /// Обновляет группу пользователей в индексе полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность группы пользователей</param>
        private void UpdateUserGroupSearchIndex(UserGroupEntity entity)
        {
            IsSearchableEntity = false;
            var oldEntity = EntityRepository.FindById(entity.Id);
            var newEntity = Mapper.Map<UserGroup>(entity);
            SearchService.UpdateInIndex(oldEntity, newEntity);
            entity.User_UserGroups = null;
        }
    }
}