﻿using DataLayer.Models;
using BusinessLayer.Entities;
using AutoMapper;
using DataLayer.Interfaces;
using StructureMap;
using System.Linq;
using BusinessLayer.Interfaces;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис для работы с дефолтным ресурспаком у пользователя
    /// </summary>
    public class CurrentResourcePackService : ICurrentResourcePackService
    {
        /// <summary>
        /// Репозиторий дефолтных ресурспаков
        /// </summary>
        private IGenericRepository<CurrentResourcePack> entityRepository;
        
        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public CurrentResourcePackService ()
        {
            entityRepository = ObjectFactory.GetInstance<IGenericRepository<CurrentResourcePack>>();
        }

        /// <summary>
        /// Сохранить дефолтный ресурспак пользователя
        /// </summary>
        /// <param name="currentResourcePack">Сущность дефолтного ресурспака</param>
        public void SaveCurrentResourcePack(CurrentResourcePackEntity currentResourcePack)
        {
            var dbItems = entityRepository.GetWithTracking(x => x.UserName == currentResourcePack.UserName);
            foreach (var item in dbItems) entityRepository.Remove(item);
            currentResourcePack.Id = 0;
            entityRepository.Create(Mapper.Map<CurrentResourcePack>(currentResourcePack));
        }

        /// <summary>
        /// Получить дефолтный ресурспак из базы данных
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Возвращает объект привязанного текущего ресурспака</returns>
        public CurrentResourcePackEntity GetCurrentResourcePack(string userName)
        {
            return Mapper.Map<CurrentResourcePackEntity>(entityRepository
                .GetWithInclude(x => x.UserName == userName, x => x.ResourcePack, x => x.UserGroup)
                .FirstOrDefault());
        }
    }
}