﻿using System;
using System.Collections.Generic;
using DataLayer.Models;
using DataLayer.Interfaces;
using BusinessLayer.Interfaces;
using BusinessLayer.Entities;

namespace BusinessLayer.Services
{   
    /// <summary>
    /// Общий сервис денежного оборота
    /// </summary>
    public abstract class CashFlowService<T,TE> : BaseEntityService<T,TE>, ICashFlowService<T,TE>
        where T : BaseModel
        where TE : GenericEntity
    {   
        /// <summary>
        /// Репозиторий кредитов
        /// </summary>
        protected IGenericRepository<Credit> CreditRepository { get; set; }

        /// <summary>
        /// Сервис бюджета
        /// </summary>
        protected IBudgetService<TE> BudgetService { get; set; }

        /// <summary>
        /// Стандартный конструктор
        /// </summary>
        /// <param name="сreditRepository">Репозиторий кредитов</param>
        public CashFlowService(
            IGenericRepository<Credit> сreditRepository,
            ILoggingService loggingService,
            IGenericRepository<T> cashFlowEntityRepository,
            IBudgetService<TE> budgetService,
            ISearchService searchService)
            : base(cashFlowEntityRepository, loggingService, searchService)
        {
            CreditRepository = сreditRepository;
            BudgetService = budgetService;
        }

        /// <summary>
        /// Получает значения на определенное количество дней
        /// </summary>
        /// <param name="toDate">Конечная дата получения значений</param>
        /// <param name="dayCount">Количество дней, на которые надо получить значения</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="resourceId">Идентификатор ресурса (оптимально)</param>
        /// <returns>Возвращает значения на определенное количество дней</returns>
        public abstract IEnumerable<T> GetDaily(DateTime toDate, int dayCount, long resourcePackId, long? resourceId = null);

        /// <summary>
        /// Добавить сущность в базу данных
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="preventUpdatingBudget">Предотвратить обновление бюджета</param>
        public virtual long Add(TE entity, string userName, bool preventUpdatingBudget)
        {
            throw new NotImplementedException();
        }
    }
}