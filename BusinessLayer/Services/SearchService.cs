﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Interfaces;
using StructureMap;
using DataLayer.Models;
using DataLayer.Infrastructure;
using BusinessLayer.Interfaces;
using DataLayer;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Сервис полнотекстового поиска
    /// </summary>
    public class SearchService : ISearchService
    {
        /// <summary>
        /// Провайдер полнотекстового поиска
        /// </summary>
        private ISearchProvider searchProvider;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public SearchService()
        {
            searchProvider = ObjectFactory.GetInstance<ISearchProvider>();
        }

        /// <summary>
        /// Ищет объекты согласно конфигурации в поисковом индексе
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <returns>Возвращает объект результатов поиска</returns>
        public SearchResult Search(SearchConfiguration configuration)
        {
            var result = new SearchResult() { Page = configuration.Page };
            foreach (var typeName in configuration.TypeNames)
            {
                var type = typeName.ParseModelType();
                var typeSearchResult = searchProvider.Search(configuration, type);
                result.Results.Add(type, typeSearchResult);
                result.Count += typeSearchResult.Cast<object>().Count();
            }
            return result;
        }

        /// <summary>
        /// Ищет объекты исключительно Fuzzy-поиском согласно конфигурации в поисковом индексе
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <returns>Возвращает объект результатов поиска</returns>
        public SearchResult SearchFuzzyOnly(SearchConfiguration configuration)
        {
            var result = new SearchResult() { Page = configuration.Page };
            foreach (var typeName in configuration.TypeNames)
            {
                var type = typeName.ParseModelType();
                var typeSearchResult = searchProvider.SearchFuzzy(configuration, type);
                result.Results.Add(type, typeSearchResult);
                result.Count += typeSearchResult.Cast<object>().Count();
            }
            return result;
        }

        /// <summary>
        /// Добавляет объект в индекс полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность добавляемого объекта</param>
        public void AddToIndex(BaseModel entity)
        {
            searchProvider.AddToIndex(entity);
        }

        /// <summary>
        /// Удаляет объект из индекса полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность удаляемого объекта</param>
        public void RemoveFromIndex(BaseModel entity)
        {
            searchProvider.RemoveFromIndex(entity);
        }

        /// <summary>
        /// Обновляет объект в индексе полнотекстового поиска
        /// </summary>
        /// <param name="oldentity">Старая сущность обновляемого объекта</param>
        /// <param name="newEntity">Новая сущность обновляемого объекта</param>
        public void UpdateInIndex(BaseModel oldEntity, BaseModel newEntity)
        {
            searchProvider.UpdateInIndex(oldEntity, newEntity);
        }

        /// <summary>
        /// Получает все типы моделей, которые могут использоваться в поиске
        /// </summary>
        /// <returns>Возвращает коллекцию типов для поиска</returns>
        public IEnumerable<Type> GetSearchableModelsTypes()
        {
            return SearchProvider.GetAllSearchableModelTypes();
        }
    }
}