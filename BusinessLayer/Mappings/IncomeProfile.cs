﻿using AutoMapper;
using BusinessLayer.Entities;
using DataLayer.Models;

namespace BusinessLayer.Mappings
{
    /// <summary>
    /// Маппинг для сущностей прибыли
    /// </summary>
    public class IncomeProfile : Profile
    {
        public IncomeProfile()
        {
            Mapper.CreateMap<IncomeRequestEntity, Income>();
            Mapper.CreateMap<Income, IncomeRequestEntity>();
            Mapper.CreateMap<Income, SpendingRequestEntity>();
        }
    }
}