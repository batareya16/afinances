﻿using AutoMapper;
using DataLayer.Models;
using BusinessLayer.Entities;

namespace BusinessLayer.Mappings
{
    /// <summary>
    /// Маппинг для сущностей кредитов
    /// </summary>
    public class CreditProfile : Profile
    {
        public CreditProfile()
        {
            Mapper.CreateMap<CreditRequestEntity, Credit>();
            Mapper.CreateMap<Credit, CreditRequestEntity>();
        }
    }
}