﻿using AutoMapper;
using DataLayer.Models;
using BusinessLayer.Entities.Module;
using BusinessLayer.Entities;
using ModuleIncome = BusinessLayer.Entities.Module.Income;

namespace BusinessLayer.Mappings
{
    /// <summary>
    /// Маппинг для сущностей модулей
    /// </summary>
    public class ModuleProfile : Profile
    {
        public ModuleProfile()
        {
            Mapper.CreateMap<ModuleResource, ModuleConfiguration>();
            Mapper.CreateMap<ModuleConfiguration, ModuleResource>();
            Mapper.CreateMap<Module, ModuleConfiguration>();
            Mapper.CreateMap<ModuleIncome, IncomeRequestEntity>();
            Mapper.CreateMap<ModuleIncome, SpendingRequestEntity>();
        }
    }
}