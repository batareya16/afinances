﻿using AutoMapper;
using BusinessLayer.Entities;
using DataLayer.Models;

namespace BusinessLayer.Mappings
{
    /// <summary>
    /// Маппинг для сущностей затрат
    /// </summary>
    public class SpendingProfile : Profile
    {
        public SpendingProfile()
        {
            Mapper.CreateMap<SpendingRequestEntity, Spending>()
                .ForMember(x => x.Price, opt => opt.MapFrom(e => e.Summ));
            Mapper.CreateMap<Spending, SpendingRequestEntity>()
                .ForMember(x => x.Summ, opt => opt.MapFrom(e => e.Price));
            Mapper.CreateMap<Spending, IncomeRequestEntity>()
                .ForMember(x => x.Summ, opt => opt.MapFrom(e => e.Price));
        }
    }
}