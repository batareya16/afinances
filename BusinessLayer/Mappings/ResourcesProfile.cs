﻿using AutoMapper;
using BusinessLayer.Entities;
using DataLayer.Models;

namespace BusinessLayer.Mappings
{
    /// <summary>
    /// Маппинг для сущностей ресурсов, ресурспаков, типов ресурсов
    /// </summary>
    public class ResourcesProfile : Profile
    {
        public ResourcesProfile()
        {
            Mapper.CreateMap<ResourcePackEntity, ResourcePack>();
            Mapper.CreateMap<ResourcePack, ResourcePackEntity>();
            Mapper.CreateMap<ResourceTypeEntity, ResourceType>();
            Mapper.CreateMap<ResourceType, ResourceTypeEntity>();
            Mapper.CreateMap<ResourceEntity, Resource>();
            Mapper.CreateMap<Resource, ResourceEntity>();
        }
    }
}