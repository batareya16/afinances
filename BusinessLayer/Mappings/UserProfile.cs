﻿using AutoMapper;
using DataLayer.Models;
using BusinessLayer.Entities.User;
using BusinessLayer.Entities;

namespace BusinessLayer.Mappings
{
    /// <summary>
    /// Маппинг для сущностей пользователей
    /// </summary>
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            Mapper.CreateMap<UserGroup, UserGroupEntity>();
            Mapper.CreateMap<UserGroupEntity, UserGroup>();
            Mapper.CreateMap<User_UserGroup, BaseUserEntity>();
            Mapper.CreateMap<CurrentResourcePackEntity, CurrentResourcePack>();
            Mapper.CreateMap<CurrentResourcePack, CurrentResourcePackEntity>();
        }
    }
}