﻿using AutoMapper;
using DataLayer.Models;
using BusinessLayer.Entities;

namespace BusinessLayer.Mappings
{
    /// <summary>
    /// Маппинг для сущностей бюджета
    /// </summary>
    public class BudgetProfile : Profile
    {
        public BudgetProfile()
        {
            Mapper.CreateMap<BudgetResourcePackEntity, Budget>();
        }
    }
}