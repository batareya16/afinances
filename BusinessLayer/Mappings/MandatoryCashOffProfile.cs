﻿using DataLayer.Models;
using BusinessLayer.Entities;
using AutoMapper;

namespace BusinessLayer.Mappings
{
    public class MandatoryCashOffProfile : Profile
    {
        public MandatoryCashOffProfile()
        {
            Mapper.CreateMap<MandatoryCashOff, MandatoryCashOffEntity>()
                .ForMember(d => d.DateAndTime, opt => opt.MapFrom(s => s.Date));
            Mapper.CreateMap<MandatoryCashOffEntity, MandatoryCashOff>()
                .ForMember(d => d.Date, opt => opt.MapFrom(s => s.DateAndTime));
        }
    }
}