﻿using System.ComponentModel.DataAnnotations;
using BusinessLayer.Resources;
using DataLayer.Resources.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет информацию для аутентификации в программу
    /// </summary>
    public class LogOnEntity : GenericEntity
    {
        /// <summary>
        /// Имя пользователя для аутентификации
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Display(ResourceType = typeof(Account), Name = "UserName")]
        public string UserName { get; set; }

        /// <summary>
        /// Пароль для аутентификации
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Account), Name = "Password")]
        public string Password { get; set; }
    }
}