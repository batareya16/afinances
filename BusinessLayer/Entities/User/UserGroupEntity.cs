﻿using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using BusinessLayer.Resources;
using Newtonsoft.Json;
using DataLayer.Models;
using UserGroupResource = DataLayer.Resources.Models.UserGroup;
using DataLayer;

namespace BusinessLayer.Entities.User
{
    /// <summary>
    /// Сущность группы пользователей
    /// </summary>
    public class UserGroupEntity : GenericEntity
    {
        /// <summary>
        /// Имя группы пользователей
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Display(ResourceType = typeof(UserGroupResource), Name = "Name")]
        [RegularExpression(Constants.UserNameRegex, ErrorMessageResourceName = "IncorrectFormat", ErrorMessageResourceType = typeof(Errors))]
        public string Name { get; set; }

        /// <summary>
        /// Владелец группы пользователей
        /// </summary>
        [Display(ResourceType = typeof(UserGroupResource), Name = "OwnerUsername")]
        public string OwnerUsername { get; set; }

        /// <summary>
        /// Флаг, показывающий приватная группа пользователей или нет
        /// </summary>
        [Display(ResourceType = typeof(UserGroupResource), Name = "Private")]
        public bool Private { get; set; }

        /// <summary>
        /// Пользователи, привязанные к группе пользователей в JSON формате
        /// </summary>
        public string UsersJson { get; set; }

        /// <summary>
        /// Ресурспаки, привязанные к группе пользователей в JSON формате
        /// </summary>
        public string ResourcePacksJson { get; set; }

        /// <summary>
        /// Ресурспаки, привязанные к группе пользователей
        /// </summary>
        public IEnumerable<ResourcePackEntity> ResourcePacks
        {
            get
            {
                return ResourcePacksJson != null
                    ? JsonConvert.DeserializeObject<IEnumerable<ResourcePackEntity>>(ResourcePacksJson)
                    : Enumerable.Empty<ResourcePackEntity>();
            }
            set
            {
                UsersJson = JsonConvert.SerializeObject(((IEnumerable<ResourcePackEntity>)value));
            }
        }

        /// <summary>
        /// Пользователи, привязанные к группе пользователей
        /// </summary>
        public IEnumerable<BaseUserEntity> Users
        {
            get
            {
                // The second comparison need to avoid Json deserializarion error
                return UsersJson != null && UsersJson != "[]"
                    ? JsonConvert
                        .DeserializeObject<IEnumerable<string>>(UsersJson)
                        .Select(x => new BaseUserEntity() { UserName = x })
                    : Enumerable.Empty<BaseUserEntity>();
            }
            set
            {
                UsersJson = JsonConvert.SerializeObject(((IEnumerable<BaseUserEntity>)value).Select(x => x.UserName));
            }
        }

        /// <summary>
        /// Привязанная коллекция связки пользователей - группа пользователей (нужен для маппинга в модель)
        /// </summary>
        public ICollection<User_UserGroup> User_UserGroups { get; set; }
    }
}