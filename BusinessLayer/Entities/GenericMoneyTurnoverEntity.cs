﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BusinessLayer.Resources;
using DataLayer;
using BusinessLayer.Infrastructure;
using DataLayer.Resources.Models;
using ResourceModel = DataLayer.Models.Resource;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Общий класс сущностей прибыли и убытка
    /// </summary>
    public abstract class GenericMoneyTurnoverEntity : GenericEntity
    {
        /// <summary>
        /// Уникальный идентификатор девайса
        /// </summary>
        public Guid DeviceGuid { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [Display(ResourceType=typeof(MoneyTurnover), Name="Description")]
        [MaxLength(Constants.DescriptionMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public string Description { get; set; }

        /// <summary>
        /// Сумма операции
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Summ")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Range(Constants.MinMoneyValue, Constants.MaxMoneyValue, ErrorMessageResourceName = "Range", ErrorMessageResourceType = typeof(Errors))]
        public double Summ { get; set; }

        /// <summary>
        /// Остаток бюджета
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Remainder")]
        public double Remainder { get; set; }

        /// <summary>
        /// Идентификатор ресурса
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Resource")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public long ResourceId { get; set; }

        /// <summary>
        /// Идентификатор ресурспака
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "ResourcePack")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Дата прибыли или убытка
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        [CheckAdequateDate(typeof(GenericMoneyTurnoverEntity), "DateAndTime")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public DateTime DateAndTime { get; set; }

        /// <summary>
        /// Ресурс прибыли или убытка
        /// </summary>
        public ResourceModel Resource { get; set; }

        /// <summary>
        /// Список ресурсов для выбора, в какой добавить значения прибыли или убытка
        /// </summary>
        public IEnumerable<ResourceModel> Resources { get; set; }
    }
}