﻿using System;
using System.Collections.Generic;
using DataLayer.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения бюджета за конкретную дату
    /// </summary>
    public class TimelyBudgetEntity : GenericEntity
    {
        /// <summary>
        /// Дата
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Общая сумма бюджета
        /// </summary>
        public double Balance { get; set; }

        /// <summary>
        /// Сумма кредитов
        /// </summary>
        public double SummaryCreditSum { get; set; }

        /// <summary>
        /// Коллекция кредитов
        /// </summary>
        public IEnumerable<BudgetCreditEntity> Credits { get; set; }
    }
}