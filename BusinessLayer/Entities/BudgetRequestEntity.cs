﻿using System.Collections.Generic;
using DataLayer.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения бюджета (запрос бюджета из базы или данные к занесению)
    /// </summary>
    public class BudgetRequestEntity : GenericBudgetEntity
    {
        /// <summary>
        /// Лимит бюджета
        /// </summary>
        public double Limit { get; set; }

        /// <summary>
        /// Обязательные платежи бюджета
        /// </summary>
        public List<MandatoryCashOff> MandatoryCashOffs { get; set; }
    }
}