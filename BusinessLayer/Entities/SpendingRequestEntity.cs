﻿using System.ComponentModel.DataAnnotations;
using DataLayer;
using BusinessLayer.Resources;
using DataLayer.Resources.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения расхода (запрос прибыли из базы или данные к занесению)
    /// </summary>
    public class SpendingRequestEntity : GenericMoneyTurnoverEntity
    {
        /// <summary>
        /// Место расхода средств
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Place")]
        [MaxLength(Constants.NameMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public string Place { get; set; }
    }
}