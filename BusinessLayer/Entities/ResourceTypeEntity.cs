﻿using System.ComponentModel.DataAnnotations;
using BusinessLayer.Resources;
using DataLayer;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using BusinessLayer.Entities.User;
using ResourceTypeResource = DataLayer.Resources.Models.ResourceType;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность для добавления и изменения типа ресурсов
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "ResourceTypeName")]
    public class ResourceTypeEntity : GenericEntity
    {
        /// <summary>
        /// Название типа ресурсов
        /// </summary>
        [Display(ResourceType = typeof(ResourceTypeResource), Name = "Name")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [MaxLength(Constants.NameMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор привязанной группы пользователей
        /// </summary>
        [Display(ResourceType = typeof(ResourceTypeResource), Name = "UserGroup")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public long UserGroupId { get; set; }

        /// <summary>
        /// Связанная группа пользователей
        /// </summary>
        public UserGroupEntity UserGroup { get; set; }
    }
}