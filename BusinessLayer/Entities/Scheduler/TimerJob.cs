﻿using System;

namespace BusinessLayer.Entities.Scheduler
{
    /// <summary>
    /// Сущность работы таймера планировщика задач
    /// </summary>
    public class TimerJob
    {
        /// <summary>
        /// Делегат работы таймера
        /// </summary>
        public Action TimerCallbackAction { get; set; }

        /// <summary>
        /// Интервал работы таймера
        /// </summary>
        public TimeSpan TimerDelay { get; set; }

        /// <summary>
        /// Дата/время начала отсчета таймера
        /// </summary>
        public DateTime StartTimer { get; set; }

        /// <summary>
        /// Идентификатор модуля, привязанного к ресурсу
        /// </summary>
        public long ModuleResourceId { get; set; }
    }
}