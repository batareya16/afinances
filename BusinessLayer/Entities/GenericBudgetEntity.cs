﻿using System.Collections.Generic;
using DataLayer.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Общий класс сущностей, связанных с бюджетом
    /// </summary>
    public abstract class GenericBudgetEntity : GenericEntity
    {
        /// <summary>
        /// Баланс бюджета
        /// </summary>
        public double Balance { get; set; }

        /// <summary>
        /// Список крудтов бюджета
        /// </summary>
        public List<BudgetCreditEntity> Credits { get; set; }
    }
}