﻿using System;
using BusinessLayer.Infrastructure;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность песочницы
    /// </summary>
    public class SandboxEntity
    {
        /// <summary>
        /// Домен приложения (объект песочницы) для исполнения работ модулей
        /// </summary>
        public AppDomain Domain { get; set; }

        /// <summary>
        /// Прокси-объект между сервером и модулем (исполнитель работ)
        /// </summary>
        public ReflectionExecutor Executor { get; set; }

        /// <summary>
        /// Имя сборки песочницы
        /// </summary>
        public string AssemblyName { get; set; }

        /// <summary>
        /// Имя песочницы
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        /// <param name="domain">Домен приложения (объект песочницы) для исполнения работ модулей</param>
        /// <param name="executor">Прокси-объект между сервером и модулем (исполнитель работ)</param>
        /// <param name="assemblyName">Имя сборки песочницы</param>
        /// <param name="name">Имя песочницы</param>
        public SandboxEntity(AppDomain domain, ReflectionExecutor executor, string assemblyName, string name)
        {
            Domain = domain;
            Executor = executor;
            AssemblyName = assemblyName;
            Name = name;
        }
    }
}