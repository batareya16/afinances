﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;
using BusinessLayer.Resources;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using DataLayer;
namespace BusinessLayer.Entities
{
    /// <summary>
    /// Базовая сущность пользователя
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "UserName")]
    public class BaseUserEntity : GenericEntity
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [RegularExpression(Constants.UserNameRegex, ErrorMessageResourceName = "IncorrectFormat", ErrorMessageResourceType = typeof(Errors))]
        [Display(ResourceType = typeof(Account), Name = "UserName")]
        public string UserName { get; set; }
    }
}
