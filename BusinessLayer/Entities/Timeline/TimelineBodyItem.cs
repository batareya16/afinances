﻿namespace BusinessLayer.Entities.Timeline
{
    /// <summary>
    /// Сущность для HTML элементов внутри панели на таймлайне
    /// </summary>
    public class TimelineBodyItem
    {
        /// <summary>
        /// HTML тег элемента
        /// </summary>
        public string tag { get; set; }

        /// <summary>
        /// Содержимое HTML тега элемента
        /// </summary>
        public string content { get; set; }
    }
}