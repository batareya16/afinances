﻿namespace BusinessLayer.Entities.Timeline
{
    /// <summary>
    /// Расположение элемента таймлайна
    /// </summary>
    public enum TimelineFloat
    {
        right,
        left
    }
}