﻿namespace BusinessLayer.Entities.Timeline
{
    /// <summary>
    /// Цвет элемента таймлайна
    /// </summary>
    public enum TimelineColor
    {
        red,
        green
    }
}