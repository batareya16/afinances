﻿using System;
using DataLayer;

namespace BusinessLayer.Entities.Timeline
{
    /// <summary>
    /// Сущность элемента таймлайна
    /// </summary>
    public class TimelineItem
    {
        /// <summary>
        /// Дата элемента таймлайна
        /// </summary>
        public DateTime TimelineTime { get; set; }

        /// <summary>
        /// Представление даты таймлайна в виде форматированной строки
        /// </summary>
        public string time { get { return TimelineTime.ToString(Constants.JavascriptDateFormat); }}

        /// <summary>
        /// Строковое представление содержимого шапки элемента таймлайна
        /// </summary>
        public string header { get; set; }

        /// <summary>
        /// Строковое представление содержимого футера элемента таймлайна
        /// </summary>
        public string footer { get; set; }

        /// <summary>
        /// Расположение элемента таймлайна
        /// </summary>
        public TimelineFloat TimelineFloat { get; set; }

        /// <summary>
        /// Цвет шапки элемента таймлайна
        /// </summary>
        public TimelineColor TimelineColor { get; set; }

        /// <summary>
        /// Строковое представление цвета шапки элемента таймлайна
        /// </summary>
        public string color { get { return TimelineColor.ToString(); } }

        /// <summary>
        /// Строковое представление позиции элемента таймлайна
        /// </summary>
        public string @float { get { return TimelineFloat.ToString(); } }

        /// <summary>
        /// Массив HTML тегов тела элемента таймлайна
        /// </summary>
        public TimelineBodyItem[] body { get; set; }
    }
}