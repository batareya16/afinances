﻿namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность модальной панели
    /// </summary>
    public class ModalPanelEntity
    {
        /// <summary>
        /// Заголовок модальной панели
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Имя частичного представления - тела модальной панели
        /// </summary>
        public string PartialBodyViewName { get; set; }

        /// <summary>
        /// Модель частичного представления
        /// </summary>
        public object Model { get; set; }

        /// <summary>
        /// Идентификатор модального окна
        /// </summary>
        public string Id { get; set; }
    }
}