﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataLayer;
using BusinessLayer.Resources;
using DataLayer.Infrastructure;
using DataLayer.Resources;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность обязательной выплаты
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "MandatoryCashOffName")]
    public class MandatoryCashOffEntity : GenericMoneyTurnoverEntity
    {
        /// <summary>
        /// Место выплаты
        /// </summary>
        [Display(Name = "Место")]
        [MaxLength(Constants.NameMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public string Place { get; set; }

        /// <summary>
        /// Связанная сущность ресурса
        /// </summary>
        public ResourcePackEntity ResourcePack { get; set; }

        /// <summary>
        /// Список доступных ресурспаков
        /// </summary>
        public IEnumerable<ResourcePackEntity> ResourcePacks { get; set; }
    }
}