﻿using System;
using System.Collections.Generic;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using System.ComponentModel.DataAnnotations;
using CreditResource = DataLayer.Resources.Models.Credit;
using BusinessLayer.Resources;
using DataLayer;
using BusinessLayer.Infrastructure;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения кредита (запрос кредита из базы или данные к занесению)
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "CreditName")]
    public class CreditRequestEntity : GenericEntity
    {
        /// <summary>
        /// Идентификатор ресурспака
        /// </summary>
        [Display(ResourceType = typeof(CreditResource), Name = "ResourcePack")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Дата начала кредита
        /// </summary>
        [Display(ResourceType = typeof(CreditResource), Name = "StartDate")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [CreditStartDate(typeof(CreditRequestEntity), "StartDate", "EndDate")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата окончания кредита
        /// </summary>
        [Display(ResourceType = typeof(CreditResource), Name = "EndDate")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [CreditEndDate(typeof(CreditRequestEntity), "EndDate", "StartDate")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Оплачен ли кредит или нет
        /// </summary>
        [Display(ResourceType = typeof(CreditResource), Name = "IsPayed")]
        public bool IsPayed { get; set; }

        /// <summary>
        /// Сумма кредита
        /// </summary>
        [Display(ResourceType = typeof(CreditResource), Name = "Summ")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Range(Constants.MinMoneyValue, Constants.MaxMoneyValue, ErrorMessageResourceName = "Range", ErrorMessageResourceType = typeof(Errors))]
        public double Summ { get; set; }

        /// <summary>
        /// Название кредита
        /// </summary>
        [Display(ResourceType = typeof(CreditResource), Name = "Name")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public string Name { get; set; }

        /// <summary>
        /// Описание кредита
        /// </summary>
        [Display(ResourceType = typeof(CreditResource), Name = "Description")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public string Description { get; set; }

        /// <summary>
        /// Связанная сущность ресурса
        /// </summary>
        public ResourcePackEntity ResourcePack { get; set; }

        /// <summary>
        /// Список доступных ресурспаков
        /// </summary>
        public IEnumerable<ResourcePackEntity> ResourcePacks { get; set; }
    }
}