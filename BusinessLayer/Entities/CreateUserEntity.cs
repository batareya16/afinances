﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BusinessLayer.Resources;
using DataLayer;
using DataLayer.Resources.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения для создания нового пользователя
    /// </summary>
    public class CreateUserEntity : BaseUserEntity
    {
        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [StringLength(
            Constants.NameMaxLength,
            ErrorMessageResourceName = "RangeReverseParams",
            ErrorMessageResourceType = typeof(Errors),
            MinimumLength = Constants.MinPasswordLength)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Account), Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждения пароля пользователя
        /// </summary>
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Account), Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessageResourceName = "PasswordsNotMatch", ErrorMessageResourceType = typeof(Errors))]
        public string ConfirmPassword { get; set; }
    }
}
