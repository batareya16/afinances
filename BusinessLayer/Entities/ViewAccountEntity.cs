﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность для просмотра информации о пользователе
    /// </summary>
    public class ViewAccountEntity : BaseUserEntity
    {
        /// <summary>
        /// Имя пользователя, который создал данного пользователя
        /// </summary>
        [Display(ResourceType = typeof(Account), Name = "UserParent")]
        public string UserParent { get; set; }

        /// <summary>
        /// Дата последнего входа в систему
        /// </summary>
        [Display(ResourceType = typeof(Account), Name = "LastLoginDate")]
        public DateTime LastLoginDate { get; set; }

        /// <summary>
        /// Дата создания пользователя
        /// </summary>
        [Display(ResourceType = typeof(Account), Name = "CreationDate")]
        public DateTime CreationDate { get; set; }
    }
}
