﻿using System;
namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения бюджета в контексте конкретного ресурспака
    /// </summary>
    public class BudgetResourcePackEntity : GenericBudgetEntity
    {
        /// <summary>
        /// Общая сумма кредита
        /// </summary>
        public double SummaryCredit { get; set; }

        /// <summary>
        /// Идентификатор ресурспака
        /// </summary>
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Имя ресурспака
        /// </summary>
        public string ResourcePackName { get; set; }

        /// <summary>
        /// Дата измерения бюджета
        /// </summary>
        public DateTime BudgetDateTime { get; set; }
    }
}