﻿using System;
using System.Collections.Generic;
using DataLayer.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения бюджета в контексте конкретного ресурса
    /// </summary>
    public class BudgetResourceEntity : GenericBudgetEntity
    {
        /// <summary>
        /// Общая сумма кредита
        /// </summary>
        public double SummaryCredit { get; set; }

        /// <summary>
        /// Список бюджетов данного ресурса
        /// </summary>
        public IEnumerable<Budget> Budgets { get; set; }

        /// <summary>
        /// Идентификатор ресурспака
        /// </summary>
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Имя ресурспака
        /// </summary>
        public string ResourcePackName { get; set; }

        /// <summary>
        /// Дата измерения бюджета
        /// </summary>
        public DateTime Date { get; set; }
    }
}