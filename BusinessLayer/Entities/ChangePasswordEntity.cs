﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BusinessLayer.Resources;
using DataLayer;
using DataLayer.Resources.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность для изменения пароля пользователя
    /// </summary>
    public class ChangePasswordEntity : GenericEntity
    {
        /// <summary>
        /// Старый пароль пользователя
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Account), Name = "CurrentPassword")]
        public string OldPassword { get; set; }

        /// <summary>
        /// Новый пароль пользователя
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [StringLength(
            Constants.NameMaxLength,
            MinimumLength = Constants.MinPasswordLength,
            ErrorMessageResourceName = "RangeReverseParams",
            ErrorMessageResourceType = typeof(Errors))]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Account), Name = "NewPassword")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Подтверждение нового пароля пользователя
        /// </summary>
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Account), Name = "ConfirmNewPassword")]
        [Compare("NewPassword", ErrorMessageResourceType=typeof(Errors), ErrorMessageResourceName = "PasswordsNotMatch")]
        public string ConfirmPassword { get; set; }
    }
}