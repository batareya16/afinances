﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BusinessLayer.Entities.Module
{
    /// <summary>
    /// Сущность для привязки модуля к ресурсам
    /// </summary>
    public class LinkModuleResourcesEntity
    {
        /// <summary>
        /// Список доступных ресурспаков для выбора для модуля
        /// </summary>
        public IEnumerable<ResourcePackEntity> ResourcePacks { get; set; }

        /// <summary>
        /// Уникальный идентификатор ресурспака
        /// </summary>
        [Display(ResourceType = typeof(DataLayer.Resources.Models.Module), Name = "Resourcepack")]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Уникальный идентификатор ресурса
        /// </summary>
        [Display(ResourceType = typeof(DataLayer.Resources.Models.Module), Name = "Resource")]
        public long ResourceId { get; set; }

        /// <summary>
        /// Уникальный идентификатор модуля
        /// </summary>
        public long ModuleId { get; set; }

        /// <summary>
        /// Отображаемое имя модуля
        /// </summary>
        [Display(ResourceType = typeof(DataLayer.Resources.Models.Module), Name = "DisplayName")]
        public string DisplayModuleName { get; set; }

        /// <summary>
        /// Список выбранных ресурсов для модуля
        /// </summary>
        public IEnumerable<ResourceEntity> SelectedResources { get; set; }

        /// <summary>
        /// Список выбранных идентификаторов ресурсов для модуля
        /// </summary>
        public IEnumerable<long> SelectedResourcesIds { get; set; }

        /// <summary>
        /// Список выбранных идентификаторов ресурсов для модуля в виде строки JSON
        /// </summary>
        public string SelectedResourcesIdsJson
        {
            get
            {
                return JsonConvert.SerializeObject(SelectedResourcesIds);
            }
            set
            {
                SelectedResourcesIds = JsonConvert.DeserializeObject<IEnumerable<long>>(value);
            }
        }
    }
}