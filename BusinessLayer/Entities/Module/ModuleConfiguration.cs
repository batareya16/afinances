﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;
using BusinessLayer.Resources;
using BusinessLayer.Infrastructure;

namespace BusinessLayer.Entities.Module
{
    /// <summary>
    /// Сущность конфигурации модуля
    /// </summary>
    public class ModuleConfiguration : GenericEntity
    {
        /// <summary>
        /// Идентификатор модуля
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "Module")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [ModuleCfgItemRemove]
        public long ModuleId { get; set; }

        /// <summary>
        /// Название модуля
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "ModuleName")]
        public string ModuleName { get; set; }

        /// <summary>
        /// Доп. информация для установки модуля
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "ModuleName")]
        public string ModuleAdditionalInfo { get; set; }

        /// <summary>
        /// Идентификатор ресурспака
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "ResourcePack")]
        [ModuleCfgItemRemove]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Идентификатор ресурспака
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "Resource")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [ModuleCfgItemRemove]
        public long ResourceId { get; set; }

        /// <summary>
        /// Флаг, который показывает, запущен модуль или нет
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "IsActive")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public bool IsActive { get; set; }

        /// <summary>
        /// Флаг, который показывает, отложенная работа модуля или запланированная
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "IsDelayedJob")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [ModuleCfgItemRemove]
        public bool IsDelayedJob { get; set; }

        /// <summary>
        /// Часы для планировщика задач модулей
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "Hours")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Range(0, 23, ErrorMessageResourceName = "Range", ErrorMessageResourceType = typeof(Errors))]
        public byte Hours { get; set; }

        /// <summary>
        /// Минуты для планировщика задач модулей
        /// </summary>
        [Display(ResourceType = typeof(ModuleResource), Name = "Minutes")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Range(0, 59, ErrorMessageResourceName = "Range", ErrorMessageResourceType = typeof(Errors))]
        public byte Minutes { get; set; }

        /// <summary>
        /// JSON-строка дополнительной конфигурации для модуля
        /// </summary>
        [ModuleCfgItemRemove]
        public string AdditionalConfiguration { get; set; }

        /// <summary>
        /// Список ресурспаков, привязанных к модулю
        /// </summary>
        [XmlIgnore]
        [ModuleCfgItemRemove]
        public IEnumerable<ResourcePackEntity> ResourcePacks { get; set; }

        /// <summary>
        /// Список ресурсов, привязанных к модулю
        /// </summary>
        [XmlIgnore]
        [ModuleCfgItemRemove]
        public IEnumerable<ResourceEntity> Resources { get; set; } 
    }
}