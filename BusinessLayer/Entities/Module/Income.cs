﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer;
using DataLayer.Resources.Models;
using BusinessLayer.Resources;
using BusinessLayer.Infrastructure;

namespace BusinessLayer.Entities.Module
{
    /// <summary>
    /// Сущность прибыли из модуля
    /// </summary>
    public class Income : GenericEntity
    {
        /// <summary>
        /// Описание
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Description")]
        [MaxLength(Constants.DescriptionMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public string Description { get; set; }

        /// <summary>
        /// Сумма операции
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Summ")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Range(Constants.MinMoneyValue, Constants.MaxMoneyValue, ErrorMessageResourceName = "Range", ErrorMessageResourceType = typeof(Errors))]
        public double Summ { get; set; }

        /// <summary>
        /// Остаток бюджета
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Remainder")]
        public double Remainder { get; set; }

        /// <summary>
        /// Идентификатор ресурса
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Resource")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public long ResourceId { get; set; }

        /// <summary>
        /// Дата прибыли или убытка
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "DateAndTime")]
        [CheckAdequateDate(typeof(GenericMoneyTurnoverEntity), "DateAndTime")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public DateTime DateAndTime { get; set; }

        /// <summary>
        /// Место дохода/расхода средств
        /// </summary>
        [Display(ResourceType = typeof(MoneyTurnover), Name = "Place")]
        [MaxLength(Constants.NameMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public string Place { get; set; }
    }
}