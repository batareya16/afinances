﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Базовый класс для сущностей
    /// </summary>
    public abstract class GenericEntity
    {
        /// <summary>
        /// Идентификатор сущности базы данных
        /// </summary>
        [Display(ResourceType = typeof(Common), Name = "Id")]
        public long Id { get; set; }
    }
}