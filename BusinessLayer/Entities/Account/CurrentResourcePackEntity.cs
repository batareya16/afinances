﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;
using BusinessLayer.Entities.User;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность, которая определяет сведения для изменения текущего ресурспака
    /// </summary>
    public class CurrentResourcePackEntity : GenericEntity
    {
        /// <summary>
        /// Коллекция доступных ресурспаков для выбора
        /// </summary>
        public IEnumerable<ResourcePackEntity> ResourcePacks { get; set; }

        /// <summary>
        /// Имя пользователя, к которому привязан текущий ресурспак
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Идентификатор привязанного текущего используемого ресурспака
        /// </summary>
        [Display(ResourceType = typeof(CurrentResourcePack), Name = "ResourcePack")]
        public long? CurrentResourcePackId { get; set; }

        /// <summary>
        /// Идентификатор привязанной текущей группы пользователей
        /// </summary>
        [Display(ResourceType = typeof(CurrentResourcePack), Name = "UserGroup")]
        public long? CurrentUserGroupId { get; set; }

        /// <summary>
        /// Объект привязанного текущего используемого ресурспака
        /// </summary>
        public ResourcePackEntity ResourcePack { get; set; }

        /// <summary>
        /// Объект привязанного текущей используемой группы пользователей
        /// </summary>
        public UserGroupEntity UserGroup { get; set; }
    }
}