﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using BusinessLayer.Resources;
using BusinessLayer.Validators;
using DataLayer;
using BusinessLayer.Entities.User;
using System.Collections.Generic;
using ResourcePackResource = DataLayer.Resources.Models.ResourcePack;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность для добавления и изменения ресурспака
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "ResourcePackName")]
    public class ResourcePackEntity : GenericEntity
    {
        /// <summary>
        /// Название ресурспака
        /// </summary>
        [Display(ResourceType = typeof(ResourcePackResource), Name = "Name")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [MaxLength(Constants.NameMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        public string Name { get; set; }

        /// <summary>
        /// Лимит средств для ресурспака
        /// </summary>
        [Display(ResourceType = typeof(ResourcePackResource), Name = "Limit")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [Range(Constants.MinMoneyValue, Constants.MaxMoneyValue, ErrorMessageResourceName = "Range", ErrorMessageResourceType = typeof(Errors))]
        public double Limit { get; set; }

        /// <summary>
        /// Идентификатор привязанной группы пользователей
        /// </summary>
        [Display(ResourceType = typeof(ResourcePackResource), Name = "UserGroup")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [ResourcePackValidator.UserGroupRequired]
        public long UserGroupId { get; set; }

        /// <summary>
        /// Связанная группа пользователей
        /// </summary>
        public UserGroupEntity UserGroup { get; set; }
    }
}