﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;
using DataLayer.Infrastructure;
using DataLayer.Resources;
using BusinessLayer.Resources;
using DataLayer;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность для создания или изменения ресурса
    /// </summary>
    [DisplayEntityAttribute(typeof(EntityNames), "ResourceName")]
    public class ResourceEntity : GenericEntity
    {
        /// <summary>
        /// Идентификатор связанного объекта типа ресурса
        /// </summary>
        [Display(ResourceType = typeof(Resource), Name = "ResourceType")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public long ResourceTypeId { get; set; }

        /// <summary>
        /// Название ресурса
        /// </summary>
        [Display(ResourceType = typeof(Resource), Name = "Name")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        [MaxLength(Constants.NameMaxLength, ErrorMessageResourceName = "MaxLength", ErrorMessageResourceType = typeof(Errors))]
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор связанного объекта Ресурспака
        /// </summary>
        [Display(ResourceType = typeof(Resource), Name = "ResourcePack")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Errors))]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Связанная сущность типа ресурса
        /// </summary>
        public ResourceTypeEntity ResourceType { get; set; }

        /// <summary>
        /// Связанная сущность ресурспака
        /// </summary>
        public ResourcePackEntity ResourcePack { get; set; }

        /// <summary>
        /// Все доступные для выбора сущности типов ресурсов
        /// </summary>
        public IEnumerable<ResourceTypeEntity> ResourceTypes { get; set; }

        /// <summary>
        /// Все доступные для выбора сущности ресурспаков
        /// </summary>
        public IEnumerable<ResourcePackEntity> ResourcePacks { get; set; }
    }
}