﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer.Resources.Models;

namespace BusinessLayer.Entities
{
    /// <summary>
    /// Сущность информации о кредите в контексте бюджета ресурспака
    /// </summary>
    public class BudgetCreditEntity : GenericEntity
    {
        /// <summary>
        /// Идентификатор связанного объекта Ресурспака
        /// </summary>
        [Display(ResourceType = typeof(Credit), Name = "ResourcePack")]
        public long ResourcePackId { get; set; }

        /// <summary>
        /// Дата начала кредита
        /// </summary>
        [Display(ResourceType = typeof(Credit), Name = "StartDate")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата окончания кредита
        /// </summary>
        [Display(ResourceType = typeof(Credit), Name = "EndDate")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Сумма кредита
        /// </summary>
        [Display(ResourceType = typeof(Credit), Name = "Summ")]
        public double Summ { get; set; }

        /// <summary>
        /// Описание кредита
        /// </summary>
        [Display(ResourceType = typeof(Credit), Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// Название кредита
        /// </summary>
        [Display(ResourceType = typeof(Credit), Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Оплачен ли кредит или нет
        /// </summary>
        [Display(ResourceType = typeof(Credit), Name = "IsPayed")]
        public bool IsPayed { get; set; }

        /// <summary>
        /// Количество средств, которое осталось вычесть
        /// </summary>
        public double LeftToSubstract { get; set; }
    }
}
