﻿using System;
using System.Linq.Expressions;
using System.Xml.Serialization;
using System.IO;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Общий класс, для базовых Helper-методов
    /// </summary>
    public static class CommonHelper
    {
        /// <summary>
        /// Метод, который генерирует HTML ссылку
        /// </summary>
        /// <param name="url">URL ссылки</param>
        /// <param name="text">отображаемый текст</param>
        /// <param name="id">Идентификатор HTML ссылки (опционально)</param>
        /// <returns>Возвращает HTML ссылку</returns>
        public static string GenerateLink(string url, string text, string id = null)
        {
            return "<a href=\"" + url + "\"" + (id != null ? "id=\"" + id + "\"" : string.Empty) + ">" + text + "</a>";
        }

        /// <summary>
        /// Метод, который получает имя объекта по двойному обобщенному предикату
        /// </summary>
        /// <typeparam name="T">тип 1</typeparam>
        /// <typeparam name="TT">тип 2</typeparam>
        /// <param name="accessor">объект</param>
        /// <returns>Возвращает имя объекта</returns>
        public static String nameof<T, TT>(this Expression<Func<T, TT>> accessor)
        {
            return nameof(accessor.Body);
        }

        /// <summary>
        /// Метод, который получает имя объекта по обобщенному предикату
        /// </summary>
        /// <typeparam name="T">тип</typeparam>
        /// <param name="accessor">объект</param>
        /// <returns>Возвращает имя объекта</returns>
        public static String nameof<T>(this Expression<Func<T>> accessor)
        {
            return nameof(accessor.Body);
        }

        /// <summary>
        /// Метод, который получает имя свойства по двойному обобщенному предикату
        /// </summary>
        /// <typeparam name="T">тип 1</typeparam>
        /// <typeparam name="TT">тип 2</typeparam>
        /// <param name="obj">Объект</param>
        /// <param name="propertyAccessor">Поле объекта</param>
        /// <returns>Возвращает имя объекта</returns>
        public static String nameof<T, TT>(this T obj, Expression<Func<T, TT>> propertyAccessor)
        {
            return nameof(propertyAccessor.Body);
        }

        /// <summary>
        /// Десериализует объект из строки XML в объект
        /// </summary>
        /// <typeparam name="T">Десериализуемый тип</typeparam>
        /// <param name="XMLString">Строка XML</param>
        /// <param name="rootElement">Имя корневого элемента (опционально)</param>
        /// <returns>Возвращает десериализованный объект</returns>
        public static T DeserializeFromXML<T>(string XMLString, string rootElement)
        {
            var serializer = rootElement == string.Empty || rootElement == null
                ? new XmlSerializer(typeof(T))
                : new XmlSerializer(typeof(T), new XmlRootAttribute(rootElement));
            var stringReader = new StringReader(XMLString);
            return (T)serializer.Deserialize(stringReader);
        }

        /// <summary>
        /// Метод, который получает имя объекта по выражению
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <returns>Возвращает имя объекта</returns>
        private static String nameof(Expression expression)
        {
            if (expression.NodeType == ExpressionType.MemberAccess)
            {
                var memberExpression = expression as MemberExpression;
                if (memberExpression == null)
                    return null;
                return memberExpression.Member.Name;
            }
            return null;
        }
    }
}