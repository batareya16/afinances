﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer;
using System.Reflection;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Аттрибут, который проверяет, находится ли дата, вводимая в поле даты начала кредита в адекватном диапазоне
    /// </summary>
    public class CreditStartDateAttribute : DateRangeAttribute
    {
        /// <summary>
        /// Имя поля даты окончания кредита
        /// </summary>
        private string creditEndDateFieldName;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        /// <param name="entityType">Тип объекта, в котором есть свойство с данным атрибутом</param>
        /// <param name="datePropertyName">Имя свойства с данным атрибутом</param>
        /// <param name="creditEndDateFieldName">Имя поля даты окончания кредита</param>
        public CreditStartDateAttribute(Type entityType, string datePropertyName, string creditEndDateFieldName)
            : base(entityType, datePropertyName, Constants.MinDate, Constants.MaxDate)
        {
            this.creditEndDateFieldName = creditEndDateFieldName;
        }

        /// <summary>
        /// Метод проверки валидации атрибута
        /// </summary>
        /// <param name="value">Сравниваемый объект</param>
        /// <param name="validationContext">Контекст валидации</param>
        /// <returns>Возвращает результат валидации даты в адекватном диапазоне</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime endDate = (DateTime)validationContext.ObjectInstance.GetType()
                    .GetProperty(creditEndDateFieldName, BindingFlags.Public | BindingFlags.Instance)
                    .GetValue(validationContext.ObjectInstance, null);
            this.MaxDate = endDate;

            return base.IsValid(value, validationContext);
        }
    }
}