﻿using System;
using System.Reflection;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Сущность Прокси-объекта между сервером и модулем (исполнитель работ)
    /// </summary>
    [Serializable]
    public class ReflectionExecutor : MarshalByRefObject
    {
        /// <summary>
        /// Выполнить метод через рефлексию и получить результат
        /// </summary>
        /// <param name="dll">Путь к файлу библиотеки, в которой требуется выполнить метод</param>
        /// <param name="typeName">Имя типа, внутри которого требуется выполнить метод</param>
        /// <param name="methodName">Имя выполняемого метода</param>
        /// <param name="parameters">Параметры, которые требуется передать (передаются по ссылке - ref)</param>
        /// <returns>Возвращает результат выполнения метода</returns>
        public object Execute(string dll, string typeName, string methodName, ref object[] parameters)
        {
            Assembly asm = Assembly.LoadFile(dll);
            object obj = asm.CreateInstance(typeName);
            Type type = obj.GetType();
            MethodInfo method = type.GetMethod(methodName);
            return method.Invoke(obj, parameters);
        }

        /// <summary>
        /// Загружает сборку динамически
        /// </summary>
        /// <param name="dll">Путь к файлу библиотеки, сборку которой надо загрузить</param>
        public void LoadAssembly(string dll)
        {
            Assembly.LoadFrom(dll);
        }
    }
}