﻿using System;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс, который определяет методы-хелперы для класса DateTime
    /// </summary>
    public static class DateHelper
    {
        /// <summary>
        /// Метод, который отнимает определенное количество месяцев от даты и получает только Год и Месяц
        /// </summary>
        /// <param name="date">Дата, из которой нужно отнять определенное количество месяцев и получить только Год и Месяц</param>
        /// <param name="monthCount">Количество месяцев, которое необходимо отнять</param>
        /// <returns>Возвращает объект Даты</returns>
        public static DateTime SubstractAndGetYearAndMonth(this DateTime date, int monthCount)
        {
            DateTime tempDate = new DateTime(date.Year, date.Month, 1).AddMonths(-monthCount);
            return new DateTime(tempDate.Year, tempDate.Month, System.DateTime.DaysInMonth(tempDate.Year, tempDate.Month));
        }
    }
}