using System.IO;
using Ionic.Zip;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс для работы с архивами формата ZIP
    /// </summary>
    public class ZipArchiver : IArchiver
    {
        /// <summary>
        /// Распаковывает файлы внутри архива ZIP в указанную директорию
        /// </summary>
        /// <param name="archiveStream">Поток ZIP архива</param>
        /// <param name="destFolder">Путь к папке выходных файлов</param>
        /// <returns>Возвращает false, если это не ZIP архив; иначе - true</returns>
        public bool Unpack(Stream archiveStream, string destFolder)
        {
            if (!ZipFile.IsZipFile(archiveStream, true)) return false;
            archiveStream.Position = 0;
            ZipFile zip = ZipFile.Read(archiveStream);
            Directory.CreateDirectory(destFolder);
            foreach (ZipEntry e in zip) e.Extract(destFolder, ExtractExistingFileAction.OverwriteSilently);
            return true;
        }
    }
}