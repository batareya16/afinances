using System.IO;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс для работы с архивами
    /// </summary
    public interface IArchiver
    {
        /// <summary>
        /// Распаковывает файлы внутри архива в указанную директорию
        /// </summary>
        /// <param name="archiveStream">Входной поток архива</param>
        /// <param name="destFolder">Путь к папке выходных файлов</param>
        /// <returns>Возвращает false, если это не архив нужного формата; иначе - true</returns>
        bool Unpack(Stream archiveStream, string destFolder);
    }
}