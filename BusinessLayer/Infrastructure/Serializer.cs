﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using BusinessLayer.Entities.Module;
using Newtonsoft.Json;
using DataLayer.Models;
using Newtonsoft.Json.Linq;
using DataLayer;
using System.Text.RegularExpressions;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс, который определяет методы-хелперы для сериализации/десериализации объектов
    /// </summary>
    public static class Serializer
    {
        /// <summary>
        /// Сериализует объект в строку XML
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект сериализации</param>
        /// <returns>Возвращает объект в виде строки XML</returns>
        public static string Serialize<T>(T obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Десериализует объект из строки XML
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="string">Объект десериализации</param>
        /// <returns>Возвращает десериализованный объект</returns>
        public static T Deserialize<T>(string @string)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(@string);
            return (T)serializer.Deserialize(reader);
        }

        /// <summary>
        /// Преобразовывает XML в JSON
        /// </summary>
        /// <param name="xml">Строка XML</param>
        /// <returns>Возвращает JSON строку</returns>
        public static string XmlToJson(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return JsonConvert.SerializeXmlNode(doc);
        }

        /// <summary>
        /// Преобразовывает JSON в XML
        /// </summary>
        /// <param name="json">Строка JSON</param>
        /// <returns>Возвращает XML строку</returns>
        public static string JsonToXml(string json)
        {
            XmlDocument doc = JsonConvert.DeserializeXmlNode(json);
            return doc.OuterXml;
        }

        /// <summary>
        /// Находит в строке JSON элемент непосредственно конфигурации модуля и возвращает его
        /// </summary>
        /// <param name="json">Строка JSON</param>
        /// <returns>Возвращает JSON строку конфигурации модуля</returns>
        public static string GetJsonConfigurationProp(string json)
        {
            return GetConfigJObj(json)["Configuration"].ToString();
        }

        /// <summary>
        /// Удаляет ненужные элементы из доп.конфигурации модуля, оставляя только кастомные
        /// </summary>
        /// <param name="jsonConfig">JSON строка доп.конфигурации модуля</param>
        /// <returns>Возвращает конфигурацию в виде JSON строки</returns>
        public static string GetCustomConfigurationNodesJson(string jsonConfig)
        {
            var configFieldNames = typeof(Module).GetProperties().Select(x => x.Name)
                .Union(typeof(ModuleResource).GetProperties().Select(x => x.Name))
                .Union(Constants.DefaultXmlNodeNames)
                .ToList();
            var configJObj = GetConfigJObj(jsonConfig);
            configFieldNames.ForEach(x => configJObj.Remove(x));
            return configJObj.ToString();
        }

        /// <summary>
        /// Расшифровывает зашифрованные поля доп.конфигурации модуля
        /// </summary>
        /// <param name="jsonConfig">JSON строка доп.конфигурации модуля</param>
        /// <returns>Возвращает конфигурацию в виде JSON строки</returns>
        public static string DecryptEncryptedConfigurationNodes(string jsonConfig)
        {
            var jObj = GetConfigJObj(jsonConfig);
            var encryptedProps = GetConfigurationPropsByRegex(jObj, Constants.ModulePasswordFieldRegex);
            foreach (var prop in encryptedProps)
            {
                prop.Value = ModuleResourceEncryptor.Decrypt(prop.Value.ToString());
            }
            return jObj.ToString();
        }

        /// <summary>
        /// Обрабатывает зашифрованные поля доп.конфигурации модуля при сохранении доп.конфигурации
        /// (Если изменено зашифр. поле, то он его подменяет на новое, при этом шифруя его, иначе - оставляет оригинал)
        /// </summary>
        /// <param name="newJsonConfig">Доп.когфигурация, которая пришла с клиента для обновления</param>
        /// <param name="originalJsonConfig">Доп.конфигурация, которая на данный момент хранится в БД</param>
        /// <returns>Возвращает обновленную обработанную доп.конфигурацию в виде строки</returns>
        public static string RestoreEncryptedConfigurationNodes(string newJsonConfig, string originalJsonConfig)
        {
            var newJObj = GetConfigJObj(newJsonConfig);
            var originalJObj = GetConfigJObj(originalJsonConfig);
            var encryptedPropsNew = GetConfigurationPropsByRegex(newJObj, Constants.ModulePasswordFieldRegex);
            var encryptedPropsOrig = GetConfigurationPropsByRegex(originalJObj, Constants.ModulePasswordFieldRegex);
            foreach (var newProp in encryptedPropsNew)
            {
                var origProp = encryptedPropsOrig.SingleOrDefault(x => x.Name == newProp.Name);
                newProp.Value = newProp.Value.ToString() == Constants.DefaultModulePasswordString
                        ? origProp.Value
                        : ModuleResourceEncryptor.Encrypt(newProp.Value.ToString());
            }

            return newJObj.ToString();
        }

        /// <summary>
        /// Обрабатывает скрытые поля доп.конфигурации модуля при сохранении доп.конфигурации
        /// (Восстанавливает их с оригинального объекта)
        /// </summary>
        /// <param name="newJsonConfig">Доп.когфигурация, которая пришла с клиента для обновления</param>
        /// <param name="originalJsonConfig">Доп.конфигурация, которая на данный момент хранится в БД</param>
        /// <returns>Возвращает обновленную обработанную доп.конфигурацию в виде строки</returns>
        public static string RestoreHiddenConfigurationNodes(string newJsonConfig, string originalJsonConfig)
        {
            var newJObj = GetConfigJObj(newJsonConfig);
            var originalJObj = GetConfigJObj(originalJsonConfig);
            var hiddenPropsOrig = GetConfigurationPropsByRegex(originalJObj, Constants.ModuleHiddenFieldRegex);
            foreach (var prop in hiddenPropsOrig)
            {
                newJObj.Remove(prop.Name);
                newJObj.Add(prop.Name, prop.Value);
            }

            return newJObj.ToString();
        }

        /// <summary>
        /// Подменяет зашифрованные поля доп.конфигурации модуля на фейковые при выводе клиенту
        /// </summary>
        /// <param name="jsonConfig">Доп.когфигурация модуля в виде JSON</param>
        /// <returns>Возвращает обновленную обработанную доп.конфигурацию в виде строки</returns>
        public static string RemoveEncryptedConfigurationNodes(string jsonConfig)
        {
            var configJObj = GetConfigJObj(jsonConfig);
            var passwordProps = GetConfigurationPropsByRegex(configJObj, Constants.ModulePasswordFieldRegex);
            foreach (var prop in passwordProps) prop.Value = Constants.DefaultModulePasswordString;
            return configJObj.ToString();
        }

        /// <summary>
        /// Удаляет скрытые поля доп.конфигурации модуля при выводе клиенту
        /// </summary>
        /// <param name="jsonConfig">Доп.когфигурация модуля в виде JSON</param>
        /// <returns>Возвращает обновленную обработанную доп.конфигурацию в виде строки</returns>
        public static string RemoveHiddenConfigurationNodes(string jsonConfig)
        {
            var configJObj = GetConfigJObj(jsonConfig);
            var hiddenPropsNames = GetConfigurationPropsByRegex(configJObj, Constants.ModuleHiddenFieldRegex)
                .Select(x => x.Name).ToList();
            foreach (var propName in hiddenPropsNames) configJObj.Remove(propName);
            return configJObj.ToString();
        }

        /// <summary>
        /// Удаляет специфичные поля конфигурации модуля при отправке модулю для получения прибыли/убытков
        /// </summary>
        /// <param name="jsonConfig">Строка JSON конфигурации модуля</param>
        /// <returns>Возвращает обработанную конфигурацию в виде строки</returns>
        public static string RemoveNodesFromCfgToModuleSending(string jsonConfig)
        {
            var configJObj = GetConfigJObj(jsonConfig);
            var props = typeof(ModuleConfiguration)
                .GetProperties()
                .Where(prop => Attribute.IsDefined(prop, typeof(ModuleCfgItemRemoveAttribute)));
            foreach (var prop in props) configJObj.Remove(prop.Name); 
            return configJObj.ToString();
        }

        /// <summary>
        /// Мержит две строки JSON в одну
        /// </summary>
        /// <param name="json1">первая строка JSON</param>
        /// <param name="json2">вторая строка JSON</param>
        /// <returns>Возвращает смерженную строку JSON</returns>
        public static string MergeJsons(string json1, string json2)
        {
            var jObj1 = GetConfigJObj(json1);
            var jObj2 = GetConfigJObj(json2);
            jObj1.Merge(jObj2, new JsonMergeSettings() { MergeArrayHandling = MergeArrayHandling.Union });
            return jObj1.ToString();
        }

        /// <summary>
        /// Получает элементы JSON конфигурации по регулярному выражению, применяемое на имя элемента (фильтрует эл-ты)
        /// </summary>
        /// <param name="configObj">Доп.когфигурация модуля в виде JSON</param>
        /// <param name="regex">Регулярное выражение, по которому надо фильтровать</param>
        /// <returns>Возвращает коллекцию эл-тов JSON конфигурации (JProperty)</returns>
        private static IEnumerable<JProperty> GetConfigurationPropsByRegex(JObject configObj, string regex)
        {
            return configObj.Properties().Where(x => Regex.IsMatch(x.Name, regex));
        }

        /// <summary>
        /// Получает JObject из строки JSON
        /// </summary>
        /// <param name="jsonConfig">Строка JSON</param>
        /// <returns>Возвращает JObject</returns>
        private static JObject GetConfigJObj(string jsonConfig)
        {
            return (JObject)JsonConvert.DeserializeObject(jsonConfig);
        }
    }
}