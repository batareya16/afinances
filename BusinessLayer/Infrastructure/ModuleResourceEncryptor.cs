﻿using ArtisanCode.SimpleAesEncryption;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс-хелпер для шифрования данных методом AES
    /// </summary>
    public static class ModuleResourceEncryptor
    {
        /// <summary>
        /// Шифрует строку методом AES
        /// </summary>
        /// <param name="data">Строка для шифрования</param>
        /// <returns>Возвращает шифротекст</returns>
        public static string Encrypt(string data)
        {
            var encryptor = new RijndaelMessageEncryptor();
            return encryptor.Encrypt(data);
        }

        /// <summary>
        /// Дешифрует строку методом AES
        /// </summary>
        /// <param name="data">Шифротекст</param>
        /// <returns>Возвращает исходный текст</returns>
        public static string Decrypt(string data)
        {
            var decryptor = new RijndaelMessageDecryptor();
            return decryptor.Decrypt(data);
        }
    }
}