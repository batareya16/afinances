﻿using DataLayer;
using System;
using BusinessLayer.Entities;
namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс, который определяет методы-хелперы для работы с кредитами
    /// </summary>
    public static class CreditHelper
    {
        /// <summary>
        /// Получает ежедневные вычеты по незавершенному кредиту
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <returns>Возвращает значение ежедневного вычета по кредиту</returns>
        public static double GetEveryDaySubtractions(CreditRequestEntity credit)
        {
            var creditDays = GetCreditDays(credit);
            if (creditDays == 0) return 0;
            return credit.Summ / creditDays;
        }

        /// <summary>
        /// Получает ежемесячные вычеты по незавершенному кредиту
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <returns>Возвращает значение ежемесячного вычета по кредиту</returns>
        public static double GetEverymonthSubtractions(CreditRequestEntity credit)
        {
            var creditDays = GetCreditDays(credit);
            if (credit.IsPayed) return 0;
            return credit.Summ / (Math.Max(creditDays, Constants.AverageDaysInMonth) / Constants.AverageDaysInMonth);
        }

        /// <summary>
        /// Получает остаток вычета по незавершенному кредиту
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <param name="date">Дата, для которой требуется получить остаток (опционально)</param>
        /// <returns>Возвращает значение остатка вычета по кредиту</returns>
        public static double GetLeftToSubstract(CreditRequestEntity credit, DateTime? date = null)
        {
            var creditDays = GetCreditDays(credit);
            var creditDaysPast = date == null ? GetCreditDaysPast(credit) : GetCreditDaysPast(credit, (DateTime)date);
            var moneyPerDay = GetEveryDaySubtractions(credit);
            if (credit.IsPayed) return 0;
            var result = (creditDays - Math.Min(creditDaysPast, creditDays)) * moneyPerDay;
            return Math.Min(result, credit.Summ);
        }

        /// <summary>
        /// Получает количество дней незавершенного кредита
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <returns>Возвращает значение количества дней кредита</returns>
        private static double GetCreditDays(CreditRequestEntity credit)
        {
            return !credit.IsPayed ? (credit.EndDate - credit.StartDate).TotalDays : 0;
        }

        /// <summary>
        /// Получает количество дней с момента начала незавершенного начатого кредита
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <returns>Возвращает значение количества дней с момента начала кредита</returns>
        private static double GetCreditDaysPast(CreditRequestEntity credit)
        {
            return CreditHelper.GetCreditDaysPast(credit, DateTime.Now);
        }

        /// <summary>
        /// Получает количество дней с момента начала незавершенного начатого кредита до определенного дня
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <param name="date">Дата, до которой требуется посчитать количество дней</param>
        /// <returns>Возвращает значение количества дней с момента начала кредита</returns>
        private static double GetCreditDaysPast(CreditRequestEntity credit, DateTime date)
        {
            var result = (date - credit.StartDate).TotalDays;
            return Math.Max(result, 0);
        }
    }
}
