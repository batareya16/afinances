﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Аттрибут, который проверяет, находится ли дата в адекватном диапазоне
    /// </summary>
    public class CheckAdequateDateAttribute : DateRangeAttribute
    {
        /// <summary>
        /// Базовый конструктор
        /// </summary>
        /// <param name="entityType">Тип объекта, в котором есть свойство с данным атрибутом</param>
        /// <param name="datePropertyName">Имя свойства с данным атрибутом</param>
        public CheckAdequateDateAttribute(Type entityType, string datePropertyName)
            : base(entityType, datePropertyName, Constants.MinDate, DateTime.Now)
        {
        }

        /// <summary>
        /// Метод проверки валидации атрибута
        /// </summary>
        /// <param name="value">Сравниваемый объект</param>
        /// <param name="validationContext">Контекст валидации</param>
        /// <returns>Возвращает результат валидации даты в адекватном диапазоне</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            this.MaxDate = DateTime.Now;
            return base.IsValid(value, validationContext);
        }
    }
}