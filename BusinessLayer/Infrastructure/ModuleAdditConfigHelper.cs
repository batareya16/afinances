﻿namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс, который определяет методы-хелперы для доп.конфигурации модулей
    /// </summary>
    public static class ModuleAdditConfigHelper
    {
        /// <summary>
        /// Обрабатывает доп.конфигурацию модуля для обновления/сохранения в БД
        /// </summary>
        /// <param name="newJsonConfig">Доп.когфигурация, которая пришла с клиента для обновления</param>
        /// <param name="originalJsonConfig">Доп.конфигурация, которая на данный момент хранится в БД</param>
        /// <param name="preventRestoringHidden">Флаг, показывающий, надо ли отменить восстановление скрытых полей</param>
        /// <returns>Возвращает обновленную обработанную доп.конфигурацию в виде строки</returns>
        public static string ProcessConfigToSave(string newJsonConfig, string originalJsonConfig, bool preventRestoringHidden = false)
        {
            var resultConfig = Serializer.RestoreEncryptedConfigurationNodes(
                            Serializer.GetCustomConfigurationNodesJson(newJsonConfig),
                            originalJsonConfig);
            return preventRestoringHidden
                ? resultConfig
                : Serializer.RestoreHiddenConfigurationNodes(resultConfig, originalJsonConfig);
        }

        /// <summary>
        /// Обрабатывает доп.конфигурацию модуля для вывода клиенту
        /// </summary>
        /// <param name="jsonConfig">Доп.конфигурация, которую надо вывести клиенту</param>
        /// <returns>Возвращает обновленную обработанную доп.конфигурацию в виде строки</returns>
        public static string ProcessConfigToDisplay(string jsonConfig)
        {
            return Serializer.RemoveHiddenConfigurationNodes(
                Serializer.RemoveEncryptedConfigurationNodes(jsonConfig));
        }
    }
}