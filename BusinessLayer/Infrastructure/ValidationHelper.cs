﻿using System.Collections.Generic;
using BusinessLayer.Entities;
using System.ComponentModel.DataAnnotations;
using BusinessLayer.Entities.Module;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс, который определяет методы-хелперы для вадидации сущностей
    /// </summary>
    public static class ValidationHelper
    {
        /// <summary>
        /// Определяет, является ли сущность валидной или нет
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <returns>Возвращает логический результат валидности сущности</returns>
        public static bool IsValid<T>(this T entity) where T: GenericEntity
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(entity, null, null);
            return Validator.TryValidateObject(entity, context, results, true);
        }

        /// <summary>
        /// Проверяет валидность значений часов и минут у конфигурации модуля
        /// </summary>
        /// <param name="config">Сущность конфигурации модуля</param>
        /// <returns>Возвращает значение валидности конфигурации модуля</returns>
        public static bool IsModuleConfigTimeValid(this ModuleConfiguration config)
        {
            return !(config.Hours == 0 && config.Minutes == 0);
        }
    }
}