﻿using System;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Аттрибут, который проверяет, требуется ли удалить данное поле
    /// при отправке конфигурации модулю для получения прибыли/убытка
    /// </summary>
    public class ModuleCfgItemRemoveAttribute : Attribute
    {
    }
}