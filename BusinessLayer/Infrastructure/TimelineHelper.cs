﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Entities.Timeline;
using BusinessLayer.Entities;
using DataLayer.Resources;
using DataLayer;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Класс, который определяет методы-хелперы для таймлайна
    /// </summary>
    public static class TimelineHelper
    {
        /// <summary>
        /// Сгенерировать данные для таймлайна
        /// </summary>
        /// <returns>Возвращает пустой лист данных для таймлайна</returns>
        public static List<TimelineItem> CreateTimeline()
        {
            return new List<TimelineItem>();
        }

        /// <summary>
        /// Сгенерировать данные для таймлайна
        /// </summary>
        /// <param name="incomes">Лист прибылей</param>
        /// <param name="spendings">Лист убытков</param>
        /// <returns>Возвращает данные прибылей и убытков для таймлайна</returns>
        public static List<TimelineItem> CreateTimeline(
            IEnumerable<IncomeRequestEntity> incomes,
            IEnumerable<SpendingRequestEntity> spendings)
        {
            var incomesAndSpendings = new List<GenericMoneyTurnoverEntity>();
            var dates = incomes
                .Select(x => x.DateAndTime)
                .Union(spendings.Select(x => x.DateAndTime))
                .Distinct()
                .OrderByDescending(x => x.Date);

            foreach (var date in dates)
            {
                incomesAndSpendings.AddRange(SortIncomesAndSpendingsForTimeline(
                    incomes.Where(x => x.DateAndTime == date),
                    spendings.Where(x => x.DateAndTime == date)));
            }

            var timeline = CreateTimeline();
            foreach (var item in incomesAndSpendings)
            {
                timeline.AddItemToTimeline(item);
            }

            return timeline;
        }

        /// <summary>
        /// Добавить элемент таймлайна
        /// </summary>
        /// <param name="timeline">Сущность таймлайна</param>
        /// <param name="item">Сущность прибыли или убытка</param>
        private static void AddItemToTimeline(this List<TimelineItem> timeline, GenericMoneyTurnoverEntity item)
        {
            var itemType = item.GetType();
            if (itemType == typeof(IncomeRequestEntity))
                timeline.AddIncomeItem(item as IncomeRequestEntity);
            else if (itemType == typeof(SpendingRequestEntity))
                timeline.AddSpendingItem(item as SpendingRequestEntity);
        }

        /// <summary>
        /// Добавить элемент таймлайна с информацией о прибыли
        /// </summary>
        /// <param name="timeline">Сущность таймлайна</param>
        /// <param name="income">Сущность прибыли</param>
        /// <returns>Возвращает сущность таймлайна</returns>
        private static List<TimelineItem> AddIncomeItem(this List<TimelineItem> timeline, IncomeRequestEntity income)
        {
            var resource = GetResourceBodyItem(income);
            var description = GetDescriptionBodyItem(income);

            var itemToAdd = new TimelineItem()
            {
                TimelineColor = TimelineColor.green,
                TimelineFloat = TimelineFloat.left,
                TimelineTime = income.DateAndTime,
                header = CommonHelper.GenerateLink(
                    Constants.FakeLinkUrl,
                    "+" + income.Summ.ToString() + Resources.Strings.Rubles,
                    income.Id.ToString()),
                body = new[] { resource, description }
            };

            timeline.Add(itemToAdd);
            return timeline;
        }

        /// <summary>
        /// Добавить элемент таймлайна с информацией о расходе 
        /// </summary>
        /// <param name="timeline">Сущность таймлайна</param>
        /// <param name="spending">Сущность расхода</param>
        /// <returns>Возвращает сущность таймлайна</returns>
        private static List<TimelineItem> AddSpendingItem(this List<TimelineItem> timeline, SpendingRequestEntity spending)
        {
            var resource = GetResourceBodyItem(spending);
            var description = GetDescriptionBodyItem(spending);

            var itemToAdd = new TimelineItem()
            {
                TimelineColor = TimelineColor.red,
                TimelineFloat = TimelineFloat.right,
                TimelineTime = spending.DateAndTime,
                header = CommonHelper.GenerateLink(
                    Constants.FakeLinkUrl,
                    "-" + spending.Summ.ToString() + Resources.Strings.Rubles,
                    spending.Id.ToString()),
                body = new[] { resource, description }
            };

            timeline.Add(itemToAdd);
            return timeline;
        }

        /// <summary>
        /// Генерирует HTML тег для показа информации о ресурсе на панелях на таймлайне
        /// </summary>
        /// <param name="entity">Сущность прибыли или убытка</param>
        /// <returns>Возвращает HTML тег с информацией о ресурсе</returns>
        private static TimelineBodyItem GetResourceBodyItem(GenericMoneyTurnoverEntity entity)
        {
            return new TimelineBodyItem()
            {
                tag = "h3",
                content = EntityNames.ResourceName + ": " + entity.Resource.Name
            };
        }

        /// <summary>
        /// Генерирует HTML тег для показа информации об описании на панелях на таймлайне
        /// </summary>
        /// <param name="entity">Сущность прибыли или убытка</param>
        /// <returns>Возвращает HTML тег с информацией описания</returns>
        private static TimelineBodyItem GetDescriptionBodyItem(GenericMoneyTurnoverEntity entity)
        {
            return new TimelineBodyItem()
            {
                tag = "h4",
                content = entity.Description
            };
        }

        /// <summary>
        /// Сортирует чередованием прибыль и убыток для таймлайна
        /// </summary>
        /// <param name="incomes">Прибыль для таймлайна</param>
        /// <param name="spendings">Убытки для таймлайна</param>
        /// <returns>Возвращает отсортированный чередованием лист прибыли и убытков</returns>
        private static List<GenericMoneyTurnoverEntity> SortIncomesAndSpendingsForTimeline(
            IEnumerable<IncomeRequestEntity> incomes,
            IEnumerable<SpendingRequestEntity> spendings)
        {
            var incomesAndSpendings = new List<GenericMoneyTurnoverEntity>();
            for (int i = 0; i < Math.Max(incomes.Count(), spendings.Count()); i++)
            {
                if (incomes.Count() > i)
                    incomesAndSpendings.Add(incomes.ElementAt(i));
                if (spendings.Count() > i)
                    incomesAndSpendings.Add(spendings.ElementAt(i));
            }

            return incomesAndSpendings;
        }
    }
}