﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Аттрибут, который проверяет, находится ли дата в заданном диапазоне
    /// </summary>
    public class DateRangeAttribute : ValidationAttribute
    {
        /// <summary>
        /// Минимальная допустимая дата
        /// </summary>
        protected DateTime MinDate { get; set; }

        /// <summary>
        /// Максимальная допустимая дата
        /// </summary>
        protected DateTime MaxDate { get; set; }

        /// <summary>
        /// Тип объекта, в котором есть свойство с данным атрибутом
        /// </summary>
        private Type entityType;

        /// <summary>
        /// Имя свойства с данным атрибутом
        /// </summary>
        private string datePropertyName;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        /// <param name="entityType">Тип объекта, в котором есть свойство с данным атрибутом</param>
        /// <param name="datePropertyName">Имя свойства с данным атрибутом</param>
        /// <param name="minDate">Минимальная допустимая дата</param>
        /// <param name="maxDate">Максимальная допустимая дата</param>
        public DateRangeAttribute(Type entityType, string datePropertyName, DateTime minDate, DateTime maxDate)
        {
            this.entityType = entityType;
            this.datePropertyName = datePropertyName;
            this.MinDate = minDate;
            this.MaxDate = maxDate;
        }

        /// <summary>
        /// Метод проверки валидации атрибута
        /// </summary>
        /// <param name="value">Сравниваемый объект</param>
        /// <param name="validationContext">Контекст валидации</param>
        /// <returns>Возвращает результат валидации даты в адекватном диапазоне</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var date = (DateTime)value;
            if (date <= MaxDate && date >= MinDate)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(string.Format(
                    Resources.Errors.Range,
                    DataLayer.Infrastructure.AttributeHelpers.GetDisplayName(entityType, datePropertyName),
                    MinDate.ToString(Constants.DisplayDateFormat),
                    MaxDate.ToString(Constants.DisplayDateFormat)));
            }
        }
    }
}