﻿using System;
using System.ComponentModel.DataAnnotations;
using DataLayer;
using System.Reflection;

namespace BusinessLayer.Infrastructure
{
    /// <summary>
    /// Аттрибут, который проверяет, находится ли дата, вводимая в поле даты окончания кредита в адекватном диапазоне
    /// </summary>
    public class CreditEndDateAttribute : DateRangeAttribute
    {
        /// <summary>
        /// Имя поля даты начала кредита
        /// </summary>
        private string creditStartDateFieldName;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        /// <param name="entityType">Тип объекта, в котором есть свойство с данным атрибутом</param>
        /// <param name="datePropertyName">Имя свойства с данным атрибутом</param>
        /// <param name="creditStartDateFieldName">Имя поля даты начала кредита</param>
        public CreditEndDateAttribute(Type entityType, string datePropertyName, string creditStartDateFieldName)
            : base(entityType, datePropertyName, Constants.MinDate, Constants.MaxDate)
        {
            this.creditStartDateFieldName = creditStartDateFieldName;
        }

        /// <summary>
        /// Метод проверки валидации атрибута
        /// </summary>
        /// <param name="value">Сравниваемый объект</param>
        /// <param name="validationContext">Контекст валидации</param>
        /// <returns>Возвращает результат валидации даты в адекватном диапазоне</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime startDate = (DateTime)validationContext.ObjectInstance.GetType()
                    .GetProperty(creditStartDateFieldName, BindingFlags.Public | BindingFlags.Instance)
                    .GetValue(validationContext.ObjectInstance, null);
            this.MinDate = startDate.AddDays(1);

            return base.IsValid(value, validationContext);
        }
    }
}