﻿using System.Collections.Generic;
using DataLayer.Models;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис для работы с логами
    /// </summary>
    public interface ILoggingService
    {
        /// <summary>
        /// Зафиксировать изменения полей объекта в базе данных
        /// </summary>
        /// <param name="from">Исходный объект</param>
        /// <param name="to">Измененный объект</param>
        /// <param name="userName">Имя инициатора лога</param>
        void TrackObjectChanges(object from, object to, string userName);

        /// <summary>
        /// Зафиксировать создание нового объекта в базе даныых
        /// </summary>
        /// <param name="obj">Создаваемый объект</param>
        /// <param name="userName">Имя инициатора лога</param>
        void TrackCreatingObject(object obj, string userName);

        /// <summary>
        /// Зафиксировать удаление объекта из базы даныых
        /// </summary>
        /// <param name="obj">Удаленный объект</param>
        /// <param name="userName">Имя инициатора лога</param>
        void TrackRemovingObject(object obj, string userName);

        /// <summary>
        /// Получить "страницу" логов пользователя
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        IEnumerable<Log> GetPageOfLogsOfUser(string userName, int page);
    }
}