﻿using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис управления песочницами подключаемых модулей
    /// </summary>
    public interface ISandboxService
    {
        /// <summary>
        /// Выполнить метод внутри песочницы
        /// </summary>
        /// <param name="modulePath">Путь к подключаемому модулю</param>
        /// <param name="typeName">Тип объекта, в котором надо выполнить метод</param>
        /// <param name="method">Имя метода для выполнения</param>
        /// <param name="parameters">Параметры, передаваемые методу (по ссылке - ref)</param>
        /// <returns>Возвращает результат выполнения метода внутри песочницы</returns>
        object SandboxExecute(string modulePath, string typeName, string method, params object[] parameters);

        /// <summary>
        /// Создает новый объект песочницы в пуле песочниц
        /// </summary>
        /// <param name="modulePath">Путь к подключаемому модулю</param>
        /// <returns>Возвращает объект созданной песочницы</returns>
        SandboxEntity CreateSandbox(string modulePath);

        /// <summary>
        /// Удаляет объект песочницы из пула песочниц
        /// </summary>
        /// <param name="sandbox">Объект песочницы для удаления</param>
        void DisposeSandbox(SandboxEntity sandbox);

        /// <summary>
        /// Удаляет объект песочниц из пула песочниц по имени сборки подключаемого модуля
        /// </summary>
        /// <param name="assemblyName">Имя сборки подключаемого модуля</param>
        void DisposeSandboxes(string assemblyName);
    }
}