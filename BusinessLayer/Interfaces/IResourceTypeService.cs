﻿using DataLayer.Models;
using BusinessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис управления типами ресурсов
    /// </summary>
    public interface IResourceTypeService : IBaseEntityService<ResourceType, ResourceTypeEntity>
    {
        /// <summary>
        /// Получить коллекцию типов ресурсов для группы пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        /// <returns>Возвращает коллекцию типов ресурсов для группы пользователей</returns>
        IEnumerable<ResourceTypeEntity> GetForUserGroup(long userGroupId);

        /// <summary>
        /// Получает коллекцию типов ресурсов, привязанных к текущей группе пользователей
        /// </summary>
        /// <param name="userGroupId">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию типов ресурсов, привязанных к текущей группе пользователей</returns>
        IEnumerable<ResourceTypeEntity> GetForCurrentUserGroup(string userName);
    }
}