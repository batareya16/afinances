﻿using System.Collections.Generic;
using DataLayer.Models;
using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface IMandatoryCashOffService : ICashFlowService<MandatoryCashOff, MandatoryCashOffEntity>
    {
        /// <summary>
        /// Достает страницу обязательных платежей, учитывая идентификатор ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает страницу значений запланированных платежей ресурспака</returns>
        IEnumerable<MandatoryCashOff> GetPageByResourcePackId(long resourcePackId, int page);
    }
}