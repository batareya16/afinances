﻿using System;
using System.Collections.Generic;
using DataLayer.Models;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис полнотекстового поиска
    /// </summary>
    public interface ISearchService
    {
        /// <summary>
        /// Ищет объекты согласно конфигурации в поисковом индексе
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <returns>Возвращает объект результатов поиска</returns>
        SearchResult Search(SearchConfiguration configuration);

        /// <summary>
        /// Ищет объекты исключительно Fuzzy-поиском согласно конфигурации в поисковом индексе
        /// </summary>
        /// <param name="configuration">Конфигурация поиска</param>
        /// <returns>Возвращает объект результатов поиска</returns>
        SearchResult SearchFuzzyOnly(SearchConfiguration configuration);

        /// <summary>
        /// Добавляет объект в индекс полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность добавляемого объекта</param>
        void AddToIndex(BaseModel entity);

        /// <summary>
        /// Удаляет объект из индекса полнотекстового поиска
        /// </summary>
        /// <param name="entity">Сущность удаляемого объекта</param>
        void RemoveFromIndex(BaseModel entity);

        /// <summary>
        /// Обновляет объект в индексе полнотекстового поиска
        /// </summary>
        /// <param name="oldentity">Старая сущность обновляемого объекта</param>
        /// <param name="newEntity">Новая сущность обновляемого объекта</param>
        void UpdateInIndex(BaseModel oldEntity, BaseModel newEntity);

        /// <summary>
        /// Получает все типы моделей, которые могут использоваться в поиске
        /// </summary>
        /// <returns>Возвращает коллекцию типов для поиска</returns>
        IEnumerable<Type> GetSearchableModelsTypes();
    }
}