﻿using BusinessLayer.Entities.Scheduler;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис планировщика работ модулей
    /// </summary>
    public interface ISchedulerService
    {
        /// <summary>
        /// Создает таймер работы модуля и добавляет в пул таймеров
        /// </summary>
        /// <param name="job">Сущность работы таймера</param>
        void CreateTimerJob(TimerJob job);

        /// <summary>
        /// Удаляет таймер работы модуля из пула таймеров
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        void RemoveTimerJob(long moduleResourceId);

        /// <summary>
        /// Получает остаток времени до начала работы таймера
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает остаток времени в миллисекундах до начала работы таймера</returns>
        long GetTimerJobRemain(long moduleResourceId);
    }
}