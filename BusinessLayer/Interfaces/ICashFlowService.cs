﻿using System;
using System.Collections.Generic;
using DataLayer.Models;
using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Общий сервис денежного оборота
    /// </summary>
    /// <typeparam name="T">Тип модели денежного оборота</typeparam>
    /// <typeparam name="TE">Тип бизнес-модели денежного оборота</typeparam>
    public interface ICashFlowService<T,TE> : IBaseEntityService<T,TE>
        where T: BaseModel
        where TE : GenericEntity
    {
        /// <summary>
        /// Получает значения на определенное количество дней
        /// </summary>
        /// <param name="toDate">Конечная дата получения значений</param>
        /// <param name="dayCount">Количество дней, на которые надо получить значения</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="resourceId">Идентификатор ресурса (оптимально)</param>
        /// <returns>Возвращает значения на определенное количество дней</returns>
        IEnumerable<T> GetDaily(DateTime toDate, int dayCount, long resourcePackId, long? resourceId = null);

        /// <summary>
        /// Добавить сущность в базу данных
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="preventUpdatingBudget">Предотвратить обновление бюджета</param>
        long Add(TE entity, string userName, bool preventUpdatingBudget);
    }
}