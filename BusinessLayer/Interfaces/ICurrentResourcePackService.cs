﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface ICurrentResourcePackService
    {
        /// <summary>
        /// Сохранить дефолтный ресурспак пользователя
        /// </summary>
        /// <param name="currentResourcePack">Сущность дефолтного ресурспака</param>
        void SaveCurrentResourcePack(CurrentResourcePackEntity currentResourcePack);

        /// <summary>
        /// Получить дефолтный ресурспак из базы данных
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Возвращает объект привязанного текущего ресурспака</returns>
        CurrentResourcePackEntity GetCurrentResourcePack(string userName);
    }
}