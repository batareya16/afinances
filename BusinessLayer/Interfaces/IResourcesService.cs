﻿using System.Collections.Generic;
using DataLayer.Models;
using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface IResourcesService : IBaseEntityService<Resource, ResourceEntity>
    {
        /// <summary>
        /// Получает все ресурсы для данного ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает список ресурсов</returns>
        IEnumerable<Resource> GetResourcePackResources(long resourcePackId);
    }
}