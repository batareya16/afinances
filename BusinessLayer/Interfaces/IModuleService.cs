﻿using System.Collections.Generic;
using System.Web;
using DataLayer.Models;
using BusinessLayer.Entities;
using BusinessLayer.Entities.Module;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис для работы с модулями
    /// </summary>
    public interface IModuleService
    {
        /// <summary>
        /// Инициализирует модуль, привязанный к ресурсу
        /// </summary>
        /// <param name="module">Объект модуля, привязанного к ресурсу</param>
        void InitializeResourceModule(ModuleResource module);

        /// <summary>
        /// Добавляет модуль из файла .DLL в папку модулей и создает запись в БД
        /// </summary>
        /// <param name="file">Отправленный файл через POST-запрос</param>
        /// <returns>Возвращает сущность созданного модуля</returns>
        Module AddModuleFromFile(HttpPostedFileBase file);

        /// <summary>
        /// Получает определенный модуль по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор модуля</param>
        /// <returns>Возвращает объект модуля</returns>
        Module GetModule(long id);

        /// <summary>
        /// Обновляет модуль, привязанный к ресурсу
        /// </summary>
        /// <param name="config">Объект конфигурации модуля</param>
        /// <returns>Возвращает обновленный объект конфигурации модуля</returns>
        ModuleConfiguration UpdateResourceModule(ModuleConfiguration config);

        /// <summary>
        /// Привязывает модуль к списку ресурсов
        /// </summary>
        /// <param name="resourcesIds">Список идентификаторов ресурсов, к которым требуется привязать модуль</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="moduleId">Идентификатор модуля</param>
        void LinkModuleToResources(IEnumerable<long> resourcesIds, string userName, long moduleId);

        /// <summary>
        /// Получает страницу списка установленных модулей
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает коллекцию установленных модулей</returns>
        IEnumerable<Module> GetModules(int page);

        /// <summary>
        /// Удаляет запись модуля и зависимые сущности из БД, удаляет файл модуля, а также останавливает все работы
        /// </summary>
        /// <param name="id">Идентификатор модуля</param>
        void RemoveModule(long id);

        /// <summary>
        /// Получает модуль, привязанный к ресурсу из БД по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает объект модуля, привязанного к ресурсу</returns>
        ModuleResource GetModuleResource(long id);

        /// <summary>
        /// Получает модуль, привязанный к ресурсу из БД по идентификатору модуля и ресурса
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="resourceId">Идентификатор ресурса</param>
        /// <returns>Возвращает объект модуля, привязанного к ресурсу</returns>
        ModuleResource GetModuleResource(long moduleId, long resourceId);

        /// <summary>
        /// Получает коллекцию ресурспаков, к которым привязан определенный модуль
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <returns>Возвращает коллекцию ресурспаков, к которым привязан определенный модуль</returns>
        IEnumerable<ResourcePackEntity> GetModuleLinkedResourcePacks(long moduleId);

        /// <summary>
        /// Получает список модулей, привязанных к ресурсу по идентификатору модуля в рамках группы пользователей
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <param name="includeTracking">Флаг, который показывает надо ли трекать объекты при получении.
        /// Например, для последующей их модификации</param>
        /// <returns>Возвращает коллекцию модулей, привязанных к ресурсу</returns>
        IEnumerable<ModuleResource> GetModuleModuleResources(long moduleId, string userName, bool includeTracking = false);

        /// <summary>
        /// Получает остаток времени до начала работы модуля, привязанного к ресурсу
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает количество миллисекунд до начала работы модуля, привязанного к ресурсу</returns>
        long GetModuleResourceTimerJobRemain(long moduleResourceId);

        /// <summary>
        /// Немедленно запускает работу модуля, привязанного к ресурсу
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает false, если планировщик на нашел задачу,
        /// или true - если планировщик пересоздал задачу успешно</returns>
        bool ImmediatelyRunJob(long moduleResourceId);

        /// <summary>
        /// Запуск активированных модулей
        /// </summary>
        void ActivateModules();
    }
}