﻿using System.Collections.Generic;
using DataLayer.Models;
using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface ICreditService : ICashFlowService<Credit, CreditRequestEntity>
    {
        /// <summary>
        /// Достает страницу кредитов, учитывая идентификатор ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает страницу значений кредитов ресурспака</returns>
        IEnumerable<Credit> GetPageByResourcePackId(long resourcePackId, int page);

        /// <summary>
        /// Ставит флаг 'IsPayed' кредиту по идентификатору
        /// </summary>
        /// <param name="creditId">Идентификатор кредита</param>
        /// <param name="userName">Имя текущего пользователя</param>
        void CloseCredit(long creditId, string userName);
    }
}