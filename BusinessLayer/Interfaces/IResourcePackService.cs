﻿using DataLayer.Models;
using BusinessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Репозиторий ресурспаков
    /// </summary>
    public interface IResourcePackService : IBaseEntityService<ResourcePack, ResourcePackEntity>
    {
        /// <summary>
        /// Находит ресурспак в базе данных по по идентификатору ресурса
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает объект ресурспака</returns>
        ResourcePack GetByResource(long resourceId);

        /// <summary>
        /// Находит ресурспак в базе данных по идентификатору ресурса с зависимыми полями
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурса</param>
        /// <returns>Возвращает объект ресурспака</returns>
        ResourcePack GetByResourceWithRelatedData(long resourceId);

        /// <summary>
        /// Получает страницу данных ресурспаков из базы данных
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="userName">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию ресурспаков</returns>
        IEnumerable<ResourcePack> GetPage(int page, string userName);

        /// <summary>
        /// Получает коллекцию ресурспаков, привязанных к определенной группе пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        /// <remarks>
        /// Аккуратно использовать данный метод!
        /// При вычетке не учитывается наличие пользователя в данной группе пользователей.
        /// При отдаче данных пользователю предусмотреть это!
        /// </remarks>
        /// <returns>Возвращает коллекцию ресурспаков, привязанных к определенной группе пользователей</returns>
        IEnumerable<ResourcePackEntity> GetUserGroupResourcePacks(long userGroupId);

        /// <summary>
        /// Получает коллекцию ресурспаков, привязанных к текущей группе пользователей
        /// </summary>
        /// <param name="userGroupId">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию ресурспаков, привязанных к текущей группе пользователей</returns>
        IEnumerable<ResourcePackEntity> GetForCurrentUserGroup(string userName);

        /// <summary>
        /// Получает коллекцию ресурсов, привязанных к текущей группе пользователей
        /// </summary>
        /// <param name="userGroupId">Имя текущего пользователя</param>
        /// <returns>Возвращает коллекцию ресурсов, привязанных к текущей группе пользователей</returns>
        IEnumerable<ResourceEntity> GetResourcesForCurrentUserGroup(string userName);
    }
}