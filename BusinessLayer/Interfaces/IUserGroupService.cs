﻿using BusinessLayer.Entities.User;
using DataLayer.Models;
using System.Collections.Generic;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис для работы с группами пользователей
    /// </summary>
    public interface IUserGroupService : IBaseEntityService<UserGroup, UserGroupEntity>
    {
        /// <summary>
        /// Получает группу пользователей из базы данных с зависимыми сущностями пользователей
        /// </summary>
        /// <param name="id">Идентификатор группы пользователей</param>
        /// <returns>Возвращает сущность группы пользователей</returns>
        UserGroupEntity FindByIdWithRelatedUsers(long id);

        /// <summary>
        /// Получает страницу групп пользователей, привязанных к конкретному пользователю
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="userName">Имя пользователя, для которого надо получить группы</param>
        /// <returns>Возвращает страницу коллекции групп пользователей</returns>
        IEnumerable<UserGroupEntity> GetPageForUser(int page, string userName);

        /// <summary>
        /// Получает значение, которое показывает, входит ли данный пользователь в группу пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Возвращает значение, которое показывает, входит ли данный пользователь в группу пользователей</returns>
        bool ContainsUserGroupUser(long userGroupId, string userName);
    }
}