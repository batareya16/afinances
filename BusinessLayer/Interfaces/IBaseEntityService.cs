﻿using System.Collections.Generic;
using BusinessLayer.Entities;
using DataLayer.Models;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Базовый сервис для сущностей
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    /// <typeparam name="TE">Тип бизнес-модели сущности</typeparam>
    public interface IBaseEntityService<T,TE>
        where T: BaseModel
        where TE: GenericEntity
    {
        /// <summary>
        /// Добавить сущность в базу данных
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        long Add(TE entity, string userName);

        /// <summary>
        /// Находит сущность по уникальному идентификатору
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <returns>Возвращает сущность</returns>
        T FindById(long id);

        /// <summary>
        /// Находит сущность по уникальному идентификатору c зависимыми полями
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <returns>Возвращает сущность</returns>
        T FindByIdWithRelatedData(long id);

        /// <summary>
        /// Обновляет сущность в базе данных
        /// </summary>
        /// <param name="income">Сущность</param>
        /// <param name="userName">Имя текущего пользователя</param>
        void Update(TE entity, string userName);

        /// <summary>
        /// Удаляет сущность из базы данных
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <param name="userName">Имя текущего пользователя</param>
        void Remove(long id, string userName);

        /// <summary>
        /// Находит список сущностей в базе данных согласно списку идентификаторов сущностей
        /// </summary>
        /// <param name="ids">Список идентификаторов сущностей</param>
        /// <returns>Возвращает коллекцию сущностей</returns>
        IEnumerable<T> GetByIds(IEnumerable<long> ids);
    }
}
