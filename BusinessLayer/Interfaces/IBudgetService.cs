﻿using System;
using System.Collections.Generic;
using BusinessLayer.Entities;
using DataLayer.Models;

namespace BusinessLayer.Interfaces
{
    /// <summary>
    /// Сервис для работы с бюджетами
    /// </summary>
    /// <typeparam name="TE">Тип сущности денежного оборота</typeparam>
    public interface IBudgetService<TE>
        where TE: GenericEntity
    {
        /// <summary>
        /// Получает актуальные на введенную дату (оптимально) значения бюджета сгруппированные по Ресурспакам
        /// </summary>
        /// <param name="maxDate">Дата для получения для нее бюджетов ресурспаков</param>
        /// <returns>Возвращает актуальные на введенную дату (оптимально) значения бюджета сгруппированные по Ресурспакам</returns>
        IEnumerable<BudgetResourcePackEntity> GetBudgetResourcePacks(DateTime? maxDate = null);

        /// <summary>
        /// Получает значения бюджета ресурспака на определенное количество дней
        /// </summary>
        /// <param name="toDate">Конечная дата получения значений бюджета</param>
        /// <param name="dayCount">Количество дней, на которые надо получить значения бюджета ресурспака</param>
        /// <param name="ResourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает значения бюджета ресурспака на определенное количество дней</returns>
        List<TimelyBudgetEntity> GetDailyBudgets(DateTime fromDate, int dayCount, long ResourcePackId);

        /// <summary>
        /// Получает значения бюджета ресурспака на определенное количество месяцев (Макс. допустимое кол-во - 24)
        /// </summary>
        /// <param name="fromMonth">Конечный месяц получения значений бюджета</param>
        /// <param name="monthCount">Количество месяцев, на которые надо получить значения бюджета ресурспака</param>
        /// <param name="ResourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает значения бюджета ресурспака на определенное количество месяцев</returns>
        List<TimelyBudgetEntity> GetMonthlyBudgets(DateTime fromMonth, int monthCount, long ResourcePackId);

        /// <summary>
        /// Обновляет значение бюджета при обновлении прибыли/убытка
        /// </summary>
        /// <param name="moneyTurnover">Объект прибыли/убытка</param>
        /// <param name="prevSumm">Сумма прибыли/затраты до изменения</param>
        void UpdateMoneyTurnoverTrackBudget(TE moneyTurnover, double prevSumm);

        /// <summary>
        /// Обновляет значение бюджета при добавлении списка прибылей/убытков
        /// </summary>
        /// <param name="entities">Лист сущностей прибыли/убытков</param>
        void AddMoneyTurnoverTrackBudgets(List<GenericMoneyTurnoverEntity> entities);

        /// <summary>
        /// Получает из базы данных лист кредитов ресурспака и рассчитывает актуальные значения суммы
        /// </summary>
        IEnumerable<BudgetCreditEntity> GetActuatCreditsForResourcePackId(DateTime actualDate, long resourcePackId);
    }
}