﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using StructureMap;
using DataLayer;
using DataLayer.Interfaces;
using DataLayer.Models;
using BusinessLayer.Interfaces;
using BusinessLayer.Services;
using AutoMapper;
using BusinessLayer.Mappings;
using BusinessLayer.Entities;
using AntonenkoFamilyServerFinances.Mappings;

namespace AntonenkoFamilyServerFinances
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Web",
                "Web/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new[] { "AntonenkoFamilyServerFinances.Controllers.Web" }
            );

            routes.MapRoute(
                "Common",
                "",
                new { controller = "Home", action = "Index" },
                new[] { "AntonenkoFamilyServerFinances.Controllers.Common" }
            );

            routes.MapRoute(
                "Api",
                "Api/{controller}/{action}/{id}",
                new { controller = "Data", action = "Ping", id = UrlParameter.Optional },
                new[] { "AntonenkoFamilyServerFinances.Controllers.Api" }
            );
        }

        protected void Application_Start()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Server.MapPath("~/Databases/"));
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            RegisterDependencyResolver();
            RegisterAutoMapper();
            ActivateModules();
        }

        /// <summary>
        /// Инициализация контейнера внедрения зависимостей
        /// </summary>
        private void RegisterDependencyResolver()
        {
            ObjectFactory.Initialize(x =>
            {
                x.For<IDatabaseContext>().Use<DatabaseContext>();
                x.For<SearchIndexSet>().Singleton();
                x.For<ISearchProvider>().Use<SearchProvider>();
                x.For(typeof(IGenericRepository<>)).Use(typeof(GenericRepository<>));
                x.For(typeof(IBudgetService<>)).Use(typeof(BudgetService<>));
                x.For(typeof(IBaseEntityService<,>)).Use(typeof(BaseEntityService<,>));

                x.For<ICashFlowService<Income, IncomeRequestEntity>>().Use<IncomeService>();
                x.For<ICashFlowService<Spending, SpendingRequestEntity>>().Use<SpendingService>();
                x.For<ICreditService>().Use<CreditService>();
                x.For<IResourcePackService>().Use<ResourcePackService>();
                x.For<IUserGroupService>().Use<UserGroupService>();
                x.For<IResourcesService>().Use<ResourcesService>();
                x.For<IMandatoryCashOffService>().Use<MandatoryCashOffService>();
                x.For<ILoggingService>().Use<LoggingService>();
                x.For<ISearchService>().Use<SearchService>();
                x.For<IResourceTypeService>().Use<ResourceTypeService>();
                x.For<ISchedulerService>().Use<SchedulerService>();
                x.For<IModuleService>().Use<ModuleService>();
                x.For<ISandboxService>().Use<SandboxService>();
                x.For<ICurrentResourcePackService>().Use<CurrentResourcePackService>();
            });
        }

        /// <summary>
        /// Инициализация конфигурации AutoMapper'а
        /// </summary>
        private void RegisterAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new BudgetProfile());
                cfg.AddProfile(new IncomeProfile());
                cfg.AddProfile(new SpendingProfile());
                cfg.AddProfile(new CreditProfile());
                cfg.AddProfile(new ResourcesProfile());
                cfg.AddProfile(new MembershipProfile());
                cfg.AddProfile(new MandatoryCashOffProfile());
                cfg.AddProfile(new ModuleProfile());
                cfg.AddProfile(new UserProfile());
            });
        }

        /// <summary>
        /// Запуск активированных модулей
        /// </summary>
        private void ActivateModules()
        {
            ObjectFactory.GetInstance<IModuleService>().ActivateModules();
        }
    }
}