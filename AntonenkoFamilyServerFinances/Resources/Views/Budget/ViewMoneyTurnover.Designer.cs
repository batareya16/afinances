﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.233
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AFinances.PresentationLayer.Resources.Views.Budget {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ViewMoneyTurnover {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ViewMoneyTurnover() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AFinances.PresentationLayer.Resources.Views.Budget.ViewMoneyTurnover", typeof(ViewMoneyTurnover).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Действие.
        /// </summary>
        public static string Action {
            get {
                return ResourceManager.GetString("Action", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Добавить прибыль.
        /// </summary>
        public static string AddIncome {
            get {
                return ResourceManager.GetString("AddIncome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Добавить убыток.
        /// </summary>
        public static string AddSpending {
            get {
                return ResourceManager.GetString("AddSpending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Кол-во дней.
        /// </summary>
        public static string ElementsCount {
            get {
                return ResourceManager.GetString("ElementsCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to До даты.
        /// </summary>
        public static string ToDate {
            get {
                return ResourceManager.GetString("ToDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Просмотр прибыли и убытков на период.
        /// </summary>
        public static string ViewMoneyTurnoverOnPeriod {
            get {
                return ResourceManager.GetString("ViewMoneyTurnoverOnPeriod", resourceCulture);
            }
        }
    }
}
