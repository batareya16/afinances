﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.233
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AFinances.PresentationLayer.Resources.Views.Budget {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Index {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Index() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AFinances.PresentationLayer.Resources.Views.Budget.Index", typeof(Index).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Действие.
        /// </summary>
        public static string Action {
            get {
                return ResourceManager.GetString("Action", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Диаграмма бюджета.
        /// </summary>
        public static string BudgetChart {
            get {
                return ResourceManager.GetString("BudgetChart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to История бюджета.
        /// </summary>
        public static string BudgetHistory {
            get {
                return ResourceManager.GetString("BudgetHistory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Закрыт.
        /// </summary>
        public static string Closed {
            get {
                return ResourceManager.GetString("Closed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Кредиты.
        /// </summary>
        public static string Credits {
            get {
                return ResourceManager.GetString("Credits", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сумма кредитов.
        /// </summary>
        public static string CreditsAmount {
            get {
                return ResourceManager.GetString("CreditsAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Текущий бюджет.
        /// </summary>
        public static string CurrentBudget {
            get {
                return ResourceManager.GetString("CurrentBudget", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поденный.
        /// </summary>
        public static string Daily {
            get {
                return ResourceManager.GetString("Daily", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Кол-во элементов.
        /// </summary>
        public static string ElementsCount {
            get {
                return ResourceManager.GetString("ElementsCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Итоговая сумма.
        /// </summary>
        public static string FullAmount {
            get {
                return ResourceManager.GetString("FullAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ср.месячный.
        /// </summary>
        public static string Monthly {
            get {
                return ResourceManager.GetString("Monthly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Открыт.
        /// </summary>
        public static string Opened {
            get {
                return ResourceManager.GetString("Opened", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Режим вывода.
        /// </summary>
        public static string OutputMode {
            get {
                return ResourceManager.GetString("OutputMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Статус.
        /// </summary>
        public static string State {
            get {
                return ResourceManager.GetString("State", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to До даты.
        /// </summary>
        public static string ToDate {
            get {
                return ResourceManager.GetString("ToDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Общая сумма.
        /// </summary>
        public static string TotalAmount {
            get {
                return ResourceManager.GetString("TotalAmount", resourceCulture);
            }
        }
    }
}
