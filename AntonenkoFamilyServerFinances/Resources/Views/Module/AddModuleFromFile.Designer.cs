﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.233
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AFinances.PresentationLayer.Resources.Views.Module {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AddModuleFromFile {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AddModuleFromFile() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AFinances.PresentationLayer.Resources.Views.Module.AddModuleFromFile", typeof(AddModuleFromFile).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выбрать файл....
        /// </summary>
        public static string ChooseAFile {
            get {
                return ResourceManager.GetString("ChooseAFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выбрать.
        /// </summary>
        public static string ChooseBtn {
            get {
                return ResourceManager.GetString("ChooseBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Модуль &quot;{0}&quot; был успешно добавлен в программу. Теперь вы можете его настроить для работы..
        /// </summary>
        public static string ModuleAddedMsg {
            get {
                return ResourceManager.GetString("ModuleAddedMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Произошла ошибка при добавлении модуля..
        /// </summary>
        public static string ModuleAddFailed {
            get {
                return ResourceManager.GetString("ModuleAddFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Произошла ошибка при добавлении модуля. Проверьте, что имя файла архива соответствует имени модуля..
        /// </summary>
        public static string ModuleAddFailedMsg {
            get {
                return ResourceManager.GetString("ModuleAddFailedMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Модуль успешно добавлен.
        /// </summary>
        public static string ModuleSuccsessfullyAdded {
            get {
                return ResourceManager.GetString("ModuleSuccsessfullyAdded", resourceCulture);
            }
        }
    }
}
