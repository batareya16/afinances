﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Уровень представления")]
[assembly: AssemblyDescription("Уровень представления программы Antonenko Family Server Finances")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Batareya16")]
[assembly: AssemblyProduct("Antonenko Family Server Finances")]
[assembly: AssemblyCopyright("Copyright Batareya16©  2017")]
[assembly: AssemblyTrademark("Batareya16 Software")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("defe0058-01f5-472f-884f-638d8078d9bc")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
