﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.Remoting.Contexts;

namespace AFinances.PresentationLayer.Infrastructure
{
    public static class MobileAuthenticator
    {
        public static string GenerateEncryptedTicket(string userName)
        {
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
              1,
              userName,
              DateTime.Now,
              DateTime.Now.AddMinutes(30),
              true,
              string.Empty,
              FormsAuthentication.FormsCookiePath);
            return FormsAuthentication.Encrypt(ticket);
        }

        public static IPrincipal GetPrincipal(string encrTicket)
        {
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(encrTicket);
            IIdentity id = new FormsIdentity(authTicket);
            return new GenericPrincipal(id, new string[0]);
        }
    }
}
