﻿using StructureMap;
using BusinessLayer.Interfaces;
using System.Web.Security;
using System.Security;
using BusinessLayer.Resources;
using BusinessLayer.Entities;
using AutoMapper;
using BusinessLayer.Entities.User;

namespace AFinances.PresentationLayer.Infrastructure
{
    /// <summary>
    /// Класс методов-хелперов контроля доступа к методам контроллеров
    /// </summary>
    public static class AuthorizeHelper
    {
        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта группы пользователей по владельцу
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        public static void CheckUserGroupOwner(long userGroupId)
        {
            var userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
            if(userGroupService.FindById(userGroupId).OwnerUsername != Membership.GetUser().UserName)
                throw new SecurityException(Errors.UserGroupFailAccess);
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта группы пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        public static void AuthorizeUserGroup(long userGroupId)
        {
            AuthorizeUserGroupInner(userGroupId);
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта группы пользователей через идентификатор ресурспака
        /// </summary>
        /// <param name="userGroupId">Идентификатор ресурспака</param>
        public static void AuthorizeUserGroupByResourcePack(long resourcePackId)
        {
            var resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            AuthorizeUserGroupInner(resourcePackService.FindById(resourcePackId).UserGroupId);
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта группы пользователей через идентификатор ресурса
        /// </summary>
        /// <param name="userGroupId">Идентификатор ресурса</param>
        public static void AuthorizeUserGroupByResource(long resourceId)
        {
            var resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            AuthorizeUserGroupInner(resourcePackService.GetByResource(resourceId).UserGroupId);
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта ресурспака через идентификатор
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        public static void AuthorizeResourcePack(long resourcePackId)
        {
            var resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            var resourcePack = resourcePackService.FindByIdWithRelatedData(resourcePackId);
            AuthorizeResourcePackInner(Mapper.Map<ResourcePackEntity>(resourcePack));
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта ресурспака через идентификатор ресурса
        /// </summary>
        /// <param name="resourceId">Идентификатор ресурса</param>
        public static void AuthorizeResourcePackByResource(long resourceId)
        {
            var resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            var resourcePack = resourcePackService.GetByResourceWithRelatedData(resourceId);
            AuthorizeResourcePackInner(Mapper.Map<ResourcePackEntity>(resourcePack));
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта ресурспака через ресурс
        /// </summary>
        /// <param name="resource">Сущность ресурса</param>
        public static void AuthorizeResourcePackByResource(ResourceEntity resource)
        {
            var resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            var resourcePack = resourcePackService.FindByIdWithRelatedData(resource.ResourcePackId);
            AuthorizeResourcePackInner(Mapper.Map<ResourcePackEntity>(resourcePack));
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта ресурспака
        /// </summary>
        /// <param name="resourcePack">Сущность ресурспака</param>
        public static void AuthorizeResourcePack(ResourcePackEntity resourcePack)
        {
            var userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
            resourcePack.UserGroup =
                Mapper.Map<UserGroupEntity>(userGroupService.FindById(resourcePack.UserGroupId));
            AuthorizeResourcePackInner(resourcePack);
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта типа ресурса
        /// </summary>
        /// <param name="resourcePack">Идентификатор типа ресурса</param>
        public static void AuthorizeResourceType(long resourceTypeId)
        {
            var resourceTypeService = ObjectFactory.GetInstance<IResourceTypeService>();
            AuthorizeUserGroup(resourceTypeService.FindById(resourceTypeId).UserGroupId);
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта типа ресурса
        /// </summary>
        /// <param name="resourcePack">Сущность типа ресурса</param>
        public static void AuthorizeResourceType(ResourceTypeEntity resourceType)
        {
            var userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
            AuthorizeResourcePackInner(new ResourcePackEntity()
            {
                UserGroupId = resourceType.UserGroupId,
                UserGroup = Mapper.Map<UserGroupEntity>(userGroupService.FindById(resourceType.UserGroupId))
            });
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта ресурспака
        /// </summary>
        /// <param name="resourcePackId">Сущность ресурспака</param>
        private static void AuthorizeResourcePackInner(ResourcePackEntity resourcePack)
        {
            if (resourcePack.UserGroup.Private)
            {
                if (resourcePack.UserGroup.OwnerUsername != Membership.GetUser().UserName)
                    throw new SecurityException(Errors.UserGroupFailAccess);
            }
            else
            {
                AuthorizeUserGroupInner(resourcePack.UserGroupId);
            }
        }

        /// <summary>
        /// Метод, проверяющий доступ к редактированию объекта группы пользователей
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        private static void AuthorizeUserGroupInner(long userGroupId)
        {
            var userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
            if (!userGroupService.ContainsUserGroupUser(userGroupId, Membership.GetUser().UserName))
            {
                throw new SecurityException(Errors.UserGroupFailAccess);
            }
        }
    }
}
