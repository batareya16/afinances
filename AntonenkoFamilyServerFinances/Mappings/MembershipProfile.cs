﻿using AutoMapper;
using System.Web.Security;
using BusinessLayer.Entities;

namespace AntonenkoFamilyServerFinances.Mappings
{
    /// <summary>
    /// Маппинг для сущностей Membership Provider
    /// </summary>
    public class MembershipProfile : Profile
    {
        public MembershipProfile()
        {
            Mapper.CreateMap<MembershipUser, ViewAccountEntity>();
            Mapper.CreateMap<MembershipUser, BaseUserEntity>();
        }
    }
}