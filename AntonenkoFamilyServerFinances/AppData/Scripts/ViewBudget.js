﻿function ViewBudget(
    dateFormat,
    firstSelectedResourcePackId,
    urlDateFormat,
    timeString,
    budgetString,
    monthString,
    currencyString,
    showAllString,
    hideAllString) {

    /* ---------- private поля ---------- */

    this.__selectors = {
        chartLoaderSpinner: "#chartLoader",
        listLoaderSpinner: "#listLoader",
        budgetListHistory: "#BudgetList",
        showAllCredits: "#ToggleAllCurrentCredits",
        chartCanvas: "#negative-positive-graph",
        currentBudgetForm: "#CurrentBudgetForm",
        currentBudgetCreditPanelsParts: ".panel-body, .panel-footer",
        currentBudgetFormCreditPanelsHeader: "#CurrentBudgetForm .panel-heading",
        currentBudgetFormCreditPanelsParts: "#CurrentBudgetForm .panel-body, #CurrentBudgetForm .panel-footer",
        resourcePackDropdowns: "[name=resourcepackPanel]"
    }

    this.__actions = ["GetDailyBudgets", "GetMonthlyBudgets"];
    this.__defaultItemsCount = 7;
    this.__parseIntRegex = "[0-9]+";
    this.__chartCanvasWidth = 960;
    this.__chartCanvasHeight = 400;
    this.__controllerNamespace = "/Web/";
    this.__controllerURL = this.__controllerNamespace + "Data/";
    this.__viewDateBudgetAction = "Budget/ViewDateBudget";
    this.__timeString = timeString;
    this.__budgetString = budgetString;
    this.__monthString = monthString;
    this.__defaultResourcePackId = getCurrentResourcePackId();

    /* ---------- public поля ---------- */

    /// <summary>
    /// Формат даты, который будет выводится на страницу
    /// </summary>
    this.dateFormat = dateFormat;

    /// <summary>
    /// Строка валюты, выводимая на страницу
    /// </summary>
    this.currencyString = currencyString;

    /// <summary>
    /// Формат даты, который нужен для передачи даты через URL
    /// </summary>
    this.urlDateFormat = urlDateFormat;

    /// <summary>
    /// Параметры данных в диаграмме
    /// </summary>
    this.chartConfig = {
        date: new Date(),
        count: this.__defaultItemsCount,
        resourcePackId: firstSelectedResourcePackId,
        action: this.__actions[0],
        selector: "#chartFilter"
    };

    /// <summary>
    /// Параметры данных в листе истории
    /// </summary>
    this.listConfig = {
        date: new Date(),
        count: this.__defaultItemsCount,
        resourcePackId: firstSelectedResourcePackId,
        action: this.__actions[0],
        selector: "#listFilter"
    };

    /// <summary>
    /// Функции для управления кнопкой раскрытия всех панелей кредитов
    /// </summary>
    this.toggleAllFunctions = new ToggleAllPanels(false, showAllString, hideAllString, this.__selectors.showAllCredits);

    /* ---------- public методы ---------- */

    /// <summary>
    /// Загружает данные с сервера в диграмму согласно параметрам
    /// </summary>
    this.UpdateChart = function () {
        var self = this;
        $(self.__selectors.chartLoaderSpinner).addClass("is-active");
        self.DisableChartPanel(true);
        this.__getDataFromPanelRequest(
            this.chartConfig,
            this.__reloadChartDataOnPageWhenDataCame);
    }

    /// <summary>
    /// Загружает данные с сервера в лист истории согласно параметрам
    /// </summary>
    this.UpdateList = function () {
        $(this.__selectors.listLoaderSpinner).addClass("is-active");
        this.DisableListPanel(true);
        this.__getDataFromPanelRequest(
            this.listConfig,
            this.__reloadListDataOnPageWhenDataCame);
    }

    /// <summary>
    /// Меняет режим просмотра данных (напр. поденный или помесячный) на странице
    /// </summary>
    this.SelectOutputMode = function (obj) {
        $(obj).parent().children().removeClass("btn-primary");
        $(obj).addClass("btn-primary");
    }

    /// <summary>
    /// Показать или скрыть текущую панель кредита
    /// </summary>
    this.toggleCurrentCreditPanel = function (event) {
        var panel = $(event.currentTarget).closest('.panel').find(this.__selectors.currentBudgetCreditPanelsParts);
        if (!$(panel).is(':hidden')) return;
        this.toggleAllFunctions.showAllPanels(false, this.__selectors.currentBudgetFormCreditPanelsParts);
        $(panel).slideDown('fast');
    }

    /// <summary>
    /// Вычитывает конфигурацию диаграммы со страницы в объект конфигурации и загружает данные диаграммы
    /// </summary>
    this.LoadChartData = function () {
        this.__loadPanelData(this.chartConfig);
        this.UpdateChart();
    }

    /// <summary>
    /// Вычитывает конфигурацию листа истории со страницы в объект конфигурации и загружает данные листа истории
    /// </summary>
    this.LoadListData = function () {
        this.__loadPanelData(this.listConfig);
        this.UpdateList();
    }

    /// <summary>
    /// Вычитывает предыдущую или следующую страницу данных диаграммы с сервера
    /// </summary>
    this.PrevNextChart = function (isNext) {
        this.__prevNextPanel(this.chartConfig, isNext);
        this.UpdateChart();
    }

    /// <summary>
    /// Вычитывает предыдущую или следующую страницу данных листа истории с сервера
    /// </summary>
    this.PrevNextList = function (isNext) {
        this.__prevNextPanel(this.listConfig, isNext);
        this.UpdateList();
    }

    /// <summary>
    /// Делает недоступной (или наоборот) панель выбора данных для диаграммы
    /// </summary>
    this.DisableChartPanel = function (disabled) {
        this.__disablePanel(disabled, this.chartConfig.selector);
    }

    /// <summary>
    /// Делает недоступной (или наоборот) панель выбора данных для листа истории
    /// </summary>
    this.DisableListPanel = function (disabled) {
        this.__disablePanel(disabled, this.listConfig.selector);
    }

    /// <summary>
    /// Обновляет значение дефолтного ресурспака и выбирает текущий ресурспак в панелях, если он изменился
    /// </summary>
    this.UpdateResourcePackPanels = function () {
        var currentResourcePackId = getCurrentResourcePackId();
        if (this.__defaultResourcePackId != currentResourcePackId) {
            this.DropdownsSelectResourcePack(currentResourcePackId);
            this.__defaultResourcePackId = currentResourcePackId;
            this.listConfig.resourcePackId = currentResourcePackId;
            this.chartConfig.resourcePackId = currentResourcePackId;
        }
    }

    /// <summary>
    /// Выбирает передаваемый идентификатор ресурспака в выпадающих меню панелей
    /// </summary>
    this.DropdownsSelectResourcePack = function (resPackId) {
        $(this.__selectors.resourcePackDropdowns).val(resPackId);
    }

    /// <summary>
    /// Назначает события поведения для панелей текущего бюджета
    /// </summary>
    this.BindCurrentBudgetPanelEvents = function () {
        this.toggleAllFunctions.showAllPanels(false, this.__selectors.currentBudgetFormCreditPanelsParts);
        var self = this;
        $(this.__selectors.currentBudgetFormCreditPanelsHeader).mouseenter(function (e) { self.toggleCurrentCreditPanel(e); });
        $(this.__selectors.showAllCredits).unbind();
        $(this.__selectors.showAllCredits).click(function () { self.toggleAllFunctions.toggleAllPanels(self.__selectors.currentBudgetFormCreditPanelsParts); });
    }

    /* ---------- private методы ---------- */

    this.__getDataFromPanelRequest = function (panelConfig, successFunction) {
        var self = this;
        $.ajax({
            url: self.__controllerURL + panelConfig.action,
            data: {
                fromDate: moment(panelConfig.date).format(self.urlDateFormat),
                itemsCount: panelConfig.count,
                resourcePackId: panelConfig.resourcePackId
            }
        })
        .done(function (responce) {
            successFunction(responce, self);
            modalPanelFunctions.bindModalRedirectButtons();
        });
    }

    this.__reloadChartDataOnPageWhenDataCame = function (responce, self) {
        var chartData = {
            xLabel: self.__timeString,
            yLabel: self.__budgetString,
            groups: [{
                label: self.__monthString,
                values: responce.map(
                            function (a) {
                                return {
                                    label: moment(parseInt(a.DateTime.match(self.__parseIntRegex))).format(self.dateFormat),
                                    value: a.Balance - a.SummaryCreditSum
                                };
                            })
            }]
        };

        var canvas = $(self.__selectors.chartCanvas)[0];
        canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
        $(self.__selectors.chartCanvas).graphly({
            'data': chartData,
            'width': self.__chartCanvasWidth,
            'height': self.__chartCanvasHeight
        });
        $(self.__selectors.chartLoaderSpinner).removeClass("is-active");
        self.DisableChartPanel(false);
    }

    this.__reloadListDataOnPageWhenDataCame = function (responce, self) {
            $(self.__selectors.budgetListHistory).find('tbody').html(
                self.__createHtmlTableRowsFromBudgetsListResponse(responce));
            $(self.__selectors.listLoaderSpinner).removeClass("is-active");
            self.DisableListPanel(false);
    }

    this.__loadPanelData = function (panelConfig) {
        panelConfig.count = $(panelConfig.selector).find('[name=elementCountPanel]').val();
        panelConfig.date = moment($(panelConfig.selector).find("[name=todatePanel]").val(), this.dateFormat).toDate();
        panelConfig.resourcePackId = $(panelConfig.selector).find('[name=resourcepackPanel]').val();
        panelConfig.action = $(panelConfig.selector).find('[name=outputmodebtnsPanel] > .btn-primary').attr('action');
    }

    this.__prevNextPanel = function (panelConfig, isNext) {
        var itemsToAdd = isNext ? panelConfig.count : -panelConfig.count;
        if (panelConfig.action == this.__actions[0])
            panelConfig.date = panelConfig.date.addDaysToDate(itemsToAdd);
        if (panelConfig.action == this.__actions[1])
            panelConfig.date = panelConfig.date.addMonthsToDate(itemsToAdd);
    }

    this.__disablePanel = function (disabled, selector) {
        $(selector).find("button").attr("disabled", disabled);
    }

    this.__getCreditTemplate = function (creditItem, self) {
        var creditTemplate = bt("id:credit-row").createInstance();
        creditTemplate.set('id', creditItem.Id);
        creditTemplate.set('name', creditItem.Name);
        creditTemplate.set('from', moment(parseInt(creditItem.StartDate.match(self.__parseIntRegex))).format(self.dateFormat));
        creditTemplate.set('to', moment(parseInt(creditItem.EndDate.match(self.__parseIntRegex))).format(self.dateFormat));
        creditTemplate.set('summ', roundMoneyVal(creditItem.Summ) + self.currencyString);
        return creditTemplate.element.outerHTML;
    }

    this.__getBudgetTemplate = function (element, self) {
        var budgetTemplate = bt("id:budget-row").createInstance();
        var budgetDate = moment(parseInt(element.DateTime.match(self.__parseIntRegex)));
        budgetTemplate.set('url',
            self.__controllerNamespace + self.__viewDateBudgetAction
            + '?ResourcePackId=' + self.listConfig.resourcePackId
            + '&Date=' + budgetDate.format(self.urlDateFormat));
        budgetTemplate.set('date', budgetDate.format(self.dateFormat));
        budgetTemplate.set('balance', roundMoneyVal(element.Balance) + self.currencyString);
        budgetTemplate.set('creditSum', roundMoneyVal(element.SummaryCreditSum) + self.currencyString);
        budgetTemplate.set('total', roundMoneyVal(element.Balance - element.SummaryCreditSum) + self.currencyString);
        var creditsString = "";
        $(element.Credits).each(function () { creditsString += self.__getCreditTemplate(this, self); });
        $(budgetTemplate.element).find('.credits').html(creditsString);
        return budgetTemplate.element.outerHTML;
    }

    this.__createHtmlTableRowsFromBudgetsListResponse = function (responce) {
        var tableInnerHtml = '';
        var self = this;
        $(responce).each(function () { tableInnerHtml += self.__getBudgetTemplate(this, self); })
        return tableInnerHtml;
    }
}