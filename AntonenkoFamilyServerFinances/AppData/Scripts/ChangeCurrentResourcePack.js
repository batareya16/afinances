﻿function ChangeCurrentResourcePack(currResPackId) {

    /* ---------- private поля ---------- */

    this.__currentResourcePackId = currResPackId;

    this.__urls = {
        getUserGroupResourcePacks: "/Web/Data/GetUserGroupResourcePacks"
    };
    
    this.__selectors = {
        resourcePacksDropdown: "[name=CurrentResourcePackId]"
    };

    this.__templates = {
        resourcePackOption: "<option value='{id}' {attr}>{name}</option>"
    };
    
    this.__proto__ = new UserGroupSearchDropDown();

    /* ---------- public поля ---------- */

    /// <summary>
    /// Селектор скрытого поля идентификатора группы пользователей
    /// </summary>
    this.selectors.userGroupId = "#CurrentUserGroupId";

    /* ---------- private методы ---------- */

    this.__getUserGroupResourcePacks = function (userGroupId) {
        var self = this;
        $.ajax({
            type: "POST",
            url: self.__urls.getUserGroupResourcePacks,
            data: {
                id: $(self.selectors.userGroupId).val()
            },
            success: function (data) {
                self.__displayResourcePackResults(data, self);
            }
        });
    };

    this.__displayResourcePackResults = function (resoucePacks, self) {
        var innerHtml = "";
        $(resoucePacks).each(function () {
            var template = bt(self.__templates.resourcePackOption).createInstance();
            template.set('name', this.Name);
            template.set('id', this.Id);
            if (self.__currentResourcePackId === this.Id) {
                template.set('attr', "selected='selected'");
            }

            innerHtml += template.element.wholeText;
        });

        $(self.__selectors.resourcePacksDropdown).html(innerHtml);
    };

    /* ---------- public методы ---------- */

    /// <summary>
    /// Метод получения возможных ресурспаков на основе текущей группы пользователей
    /// </summary>
    this.getCurrentResourcePacks = function() {
        var currentUserGroup = $(this.selectors.userGroupId).val();
        this.__getUserGroupResourcePacks(currentUserGroup);
    };

    /// <summary>
    /// Метод отмены выбора группы пользователей
    /// </summary>
    this.onDeselectedUserGroup = function (self) {
        $(self.__selectors.resourcePacksDropdown).html("");
    };

    /// <summary>
    /// Метод выбора текущей группы пользователей
    /// </summary>
    this.onUserGroupSelected = function (e, self) {
        $(self.__selectors.resourcePacksDropdown).html("");
        this.__getUserGroupResourcePacks(e.target.value);
    }
}