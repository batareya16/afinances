﻿function ToggleAllPanels(
    showedAll,
    showAllString,
    hideAllString,
    showAllBtnSelector) {

    /* ---------- private поля ---------- */

    this.__strings = {
        showAllValues: {
            false: showAllString, 
            true: hideAllString
            }}

    this.__showedAll = showedAll;
    this.__showAllBtnSelector = showAllBtnSelector;

    /* ---------- public методы ---------- */

    /// <summary>
    /// Показать или скрыть все панели
    /// </summary>
    this.toggleAllPanels = function (selector) {
        this.showAllPanels(!this.__showedAll, selector);
    }

    /// <summary>
    /// Показать или скрыть все панели в зависимости от параметра
    /// </summary>
    this.showAllPanels = function (isShow, selector) {
        var self = this;
        var panels = $(selector);
        if (isShow) panels.slideDown('fast')
        else panels.slideUp('fast');
        self.__showedAll = isShow;
        $(self.__showAllBtnSelector).text(self.__strings.showAllValues[isShow]);
    }
}