﻿function ManageUserGroup(searchUsersUrl) {

    /* ---------- private поля ---------- */

    this.__templates = {
        dropdownTemplate: "<a href='javascript:void(0)' class='user-search-result'>{username}</a>",
        selectedUserTemplate: "id:selected-user-template"
    }
    this.__searchUsersUrl = searchUsersUrl;
    this.__selectors = {
        userSearchDropdown: ".user-search-results",
        selectedUsersInUserGroup: "#seletedUsersTable tr td.selected-user",
        selectedUsersTableBody: "#seletedUsersTable tbody",
        userSearchResult: ".user-search-results > a",
        removeUserBtn: ".remove-found-user",
        userSearchField: "#UserSearch",
        usersJsonInput: "#UsersJson"
    }
    
    /* ---------- public методы ---------- */

    /// <summary>
    /// Метод поиска пользователей и отображение результатов в выпадающем меню
    /// </summary>
    this.findUsers = function (str) {
        var self = this;
        $.ajax({
            type: "POST",
            url: self.__searchUsersUrl,
            data: {
                query: $(self.__selectors.userSearchField).val(),
                selectedUsersJson: JSON.stringify(self.__getSelectedUsers(self).toArray())
            },
            success: function (data) {
                self.__displaySearchResults(data, self);
                self.__bindSelectUserFunctions(self);
            }
        });
    }

    this.applySelectedUsers = function () {
        var self = this;
        $(this.__selectors.usersJsonInput).val(JSON.stringify(this.__getSelectedUsers(self).toArray()));
    }

    this.bindRemoveUserButton = function (self) {
        $(self.__selectors.removeUserBtn).on("click", function (e) {
            self.__removeUser(e);
        });
    }

    /* ---------- private методы ---------- */

    this.__bindSelectUserFunctions = function (self) {
        $(self.__selectors.userSearchResult).on("click", function (e) {
            self.__selectUser(e, self);
        });
    }

    this.__removeUser = function (e) {
        $(e.target).closest('tr').remove();
    }

    this.__selectUser = function (e, self) {
        var userItem = bt(this.__templates.selectedUserTemplate).createInstance();
        userItem.set('username', e.target.text);
        var $table = $(this.__selectors.selectedUsersTableBody);
        $table.append(userItem.element.outerHTML);
        $(this.__selectors.userSearchDropdown).html('');
        $(this.__selectors.userSearchDropdown).hide();
        this.bindRemoveUserButton(self);
    }

    this.__getSelectedUsers = function (self) {
        return $(self.__selectors.selectedUsersInUserGroup).map(function (x, y) { return $(y).text().trim() });
    }

    this.__displaySearchResults = function (users, self) {
        var innerHtml = "";
        if (users.length <= 0) {
            $(self.__selectors.userSearchDropdown).hide();
            return;
        }

        $(users).each(function () {
            var dropdownTemplate = bt(self.__templates.dropdownTemplate).createInstance();
            dropdownTemplate.set('username', this.UserName);
            innerHtml += dropdownTemplate.element.wholeText;
        });

        $(self.__selectors.userSearchDropdown).html(innerHtml);
        $(self.__selectors.userSearchDropdown).show();
    }
}