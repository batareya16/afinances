﻿function ModuleConfiguration(moduleId, modulePasswordRegex, YesString, NoString) {

    /* ---------- private поля ---------- */

    this.__moduleId = moduleId;

    this.__modulePasswordRegex = modulePasswordRegex;

    this.__selectors = {
        resourcePack: "#ResourcePackId",
        resources: "[name=ResourceId]",
        hiddenResource: "#HiddenResourceId",
        moduleConfigInnerForm: "#ModuleConfigurationInnerForm form",
        resourcesLoader: "#ResourcesLoader",
        moduleResourceId: "#ModuleConfigurationInnerForm [name=Id]",
        additionalConfigurationHiddenJson: "#AdditionalConfiguration",
        additionalConfigurationInputsContainer: "#AdditionalConfigContainer",
        AdditionalConfigurationId: "additionalcfg-item"
    }

    this.__urls = {
        controllerURL: "/Web/Data/",
        getResources: "GetModuleResourcePackResources",
        getModuleResourcePackConfig: "/Web/Module/ModuleConfigurationInner",
        updateModuleResourcePackConfig: "/Web/Module/UpdateModuleConfigurationInner",
        immediatelyRunJob: "/Web/Module/ImmediatelyRunJob",
        getModuleResourceTimerJobRemain: "/Web/Module/GetModuleResourceTimerJobRemain"
    }

    this.__listPanelFunctions = new ListPanel(
        this.__urls.controllerURL,
        null,
        this.resourcePackId);

    this.__templates = {
        resourceDropDownItemTemplate: "<option value='{id}'>{name}</option>"
    }

    this.__togglesStrings = {
        yes: YesString,
        no: NoString
    }

    /* ---------- public методы ---------- */

    /// <summary>
    /// Получает список ресурсов ресурспака, выбранные для конфигурации модуля
    /// </summary>
    this.getResources = function () {
        var self = this;
        $(this.__selectors.resourcesLoader).addClass("is-active");
        this.__listPanelFunctions.resourcePackId = $(this.__selectors.resourcePack).val();
        this.__listPanelFunctions.getPanelDataFromServer(
            this.__urls.getResources,
            this.__selectors.resources,
            this.__getDropDownOptionTemplate,
            null,
            this,
            null,
            null,
            this.__moduleId)
        .then(function () { self.onSelectResource(); });
    }

    /// <summary>
    /// Получает конфигурацию модуля, привязанного к ресурсу при выборе ресурса из списка
    /// </summary>
    this.onSelectResource = function () {
        this.__duplicateResourceId();
        this.__getResourcePackConfiguration();
    }

    this.startTimer = function () {
        this.__getTimerJobRemain();
    }

    /// <summary>
    /// Отправляет запрос сохранения конфигурации модуля, привязанного к ресурсу
    /// </summary>
    this.submitModuleResourcePackChanges = function () {
        var form = $(this.__selectors.moduleConfigInnerForm);
        form.attr("action", this.__urls.updateModuleResourcePackConfig);
        this.__duplicateResourceId();
        form.submit();
    }

    /// <summary>
    /// Рендерит дополнительные поля конфигурации модуля, которые хранятся в строке JSON
    /// </summary>
    this.displayModuleConfigurationFields = function () {
        var self = this;
        var obj = JSON.parse($(this.__selectors.additionalConfigurationHiddenJson).val() || "{}");
        var resultHtml = "";
        var booleanFields = [];
        for (var fieldName in obj) {
            var template = bt("id:" + this.__selectors.AdditionalConfigurationId).createInstance();
            var isBool = !!fieldName.match(/\[b\]$/);
            template.set('type', isBool ? "checkbox" : "text");
            template.set('name', fieldName);
            template.set('labelname', fieldName.replace(/\[.\]$/, ""));
            template.set('value', obj[fieldName] || "");
            if (isBool) booleanFields.push(fieldName);
            resultHtml += this.checkAndRenderConfigPasswordFields(fieldName, template.element).outerHTML;
        }

        $(this.__selectors.additionalConfigurationInputsContainer).html(resultHtml);
        $(booleanFields).each(function () {
            var $element = $("[name='" + this + "']");
            $element.attr("data-toggle", "toggle");
            $element.bootstrapToggle({
                on: self.__togglesStrings.yes,
                off: self.__togglesStrings.no
            });
            $element.prop('checked', getBool(obj[this])).change();
        });
    }

    /// <summary>
    /// Проверяет, является ли дополнительное поле конфигурации модуля паролем
    /// и ставит соответствующий тип элемента ввода
    /// </summary>
    this.checkAndRenderConfigPasswordFields = function (fieldName, temlateElement) {
        var regExp = new RegExp(this.__modulePasswordRegex);
        if (regExp.test(fieldName)) {
            $(temlateElement).find(".editor-label label").html(fieldName.replace(regExp, ""));
            $(temlateElement).find(".editor-field input").attr("type", "password");
        }
        return temlateElement;
    }

    /// <summary>
    /// Отправляет запрос запуска работы модуля, привязанного к ресурсу
    /// </summary>
    this.immediatelyRunJob = function () {
        var form = $(this.__selectors.moduleConfigInnerForm);
        form.attr("action", this.__urls.immediatelyRunJob);
        this.__duplicateResourceId();
        form.submit();
    }

    /* ---------- private методы ---------- */

    this.__startJobTimer = function (remainTime) {
        var $timerSpan = $('#ResourceModuleJobTimer');
        $timerSpan.attr("data-seconds-left", remainTime / 1000);
        $timerSpan.startTimer();
    }

    this.__getTimerJobRemain = function () {
        var self = this;
        var moduleResourceId = $(this.__selectors.moduleResourceId).val();
        $.ajax({
            url: this.__urls.getModuleResourceTimerJobRemain,
            data: { moduleResourceId: moduleResourceId },
            success: function (result) {
                self.__startJobTimer(result);
            }
        });
    }

    this.__getDropDownOptionTemplate = function (item, self) {
        var template = bt(self.__templates.resourceDropDownItemTemplate).createInstance();
        template.set('id', item.Id);
        template.set('name', item.Name);
        return template.element.wholeText;
    }

    this.__duplicateResourceId = function () {
        var resourceVal = $(this.__selectors.resources).val();
        $(this.__selectors.hiddenResource).val(resourceVal);
    }

    this.__getResourcePackConfiguration = function () {
        var form = $(this.__selectors.moduleConfigInnerForm);
        form.attr("action", this.__urls.getModuleResourcePackConfig);
        form.submit();
    }
}