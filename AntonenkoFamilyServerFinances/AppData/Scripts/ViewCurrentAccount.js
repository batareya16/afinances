﻿function ViewCurrentAccount(modalPanelId) {

    /* ---------- private поля ---------- */

    this.__selectors = {
        modalPanel: '#' + modalPanelId,
        prevActionsDate: '#PrevDate',
        nextActionsDate: '#NextDate',
        actionsPage: '#page',
        actionsForm: '#ActionsForm',
        userGroupsForm: '#UserGroupsForm',
        createUsersBtn: '#CreateUser',
        addUserGroupBtn: '#AddUserGroup',
        changePasswordBtn: '#ChangePassword',
        logoutBtn: '#Logout'
    }

    this.__urls = {
        createUser: '/Web/Account/CreateUser',
        addUserGroup: '/Web/UserGroup/AddUserGroup',
        logOff: '/Web/Account/LogOff',
        changePassword: '/Web/Account/ChangePassword'
    }

    /* ---------- public методы ---------- */

    /// <summary>
    /// Инициализация обработчиков страницы
    /// </summary>
    this.initialize = function () {
        var self = this;
        $(this.__selectors.modalPanel).modal({ show: false });

        $(this.__selectors.prevActionsDate).click(function () {
            self.__decPage(self.__selectors.actionsPage);
        });

        $(this.__selectors.nextActionsDate).click(function () {
            self.__incPage(self.__selectors.actionsPage);
        });

        $(this.__selectors.createUsersBtn).click(function () {
            modalPanelFunctions.redirectModal(self.__urls.createUser);
            $(self.__selectors.modalPanel).modal('show');
        });

        $(this.__selectors.addUserGroupBtn).click(function () {
            modalPanelFunctions.redirectModal(self.__urls.addUserGroup);
            $(self.__selectors.modalPanel).modal('show');
        });

        $(this.__selectors.changePasswordBtn).click(function () {
            modalPanelFunctions.redirectModal(self.__urls.changePassword);
            $(self.__selectors.modalPanel).modal('show');
        });

        $(this.__selectors.logoutBtn).click(function () { location.href = self.__urls.logOff });

        this.__updateData();

        $(this.__selectors.modalPanel).on('hidden.bs.modal', function () {
            self.__updateData();
        });

        return self;
    }

    /* ---------- private методы ---------- */

    this.__updateData = function () {
        $(this.__selectors.actionsForm).submit();
        $(this.__selectors.userGroupsForm).submit();
    }

    this.__incPage = function (paginatorSelector) {
        $(paginatorSelector).val(+$(paginatorSelector).val() + 1);
    }

    this.__decPage = function (paginatorSelector) {
        var pageNumber = +$(paginatorSelector).val();
        if (pageNumber <= 0) return;
        $(paginatorSelector).val(pageNumber - 1);
    }
}