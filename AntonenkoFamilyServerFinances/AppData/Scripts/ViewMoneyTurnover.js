﻿function ViewMoneyTurnover(
    serverDateFormat,
    clientDateFormat,
    showAllString,
    hideAllString,
    modalSelector) {

    /* ---------- private поля ---------- */

    this.__selectors = {
        toDateInputSelector: '[name=todatePanel]',
        dayCountInputSelector: '[name=dayCountPanel]',
        resourcePackInputSelector: '[name=resourcepackPanel]',
        timeline: '#budgetTimeline',
        pageTitle: '#PageTitle',
        showAllBtn: '[name=showAll]',
        loader: '#TimelineLoader',
        modalPanelSelector: modalSelector,
        incomeTimelineLink: ".timeline-green .panel-heading a",
        spendingTimelineLink: ".timeline-red .panel-heading a",
        addIncomeBtnSelector: "#AddIncome",
        addSpendingBtnSelector: "#AddSpending"
    }

    this.__urls = {
         getTimelineDataUrl: "/Web/Data/GetTimelineData",
         viewIncomeUrl: "/Web/Budget/ViewIncome",
         viewSpendingUrl: "/Web/Budget/ViewSpending",
         addIncomeUrl: "/Web/Budget/AddIncome",
         addSpendingUrl: "/Web/Budget/AddSpending"
     }
     
     this.__defaultResourcePackId = getCurrentResourcePackId();

    /* ---------- public поля ---------- */

    /// <summary>
    /// Конфигурация таймлайна
    /// </summary>
    this.timelineConfig = {
        toDate: null,
        dayCount: null,
        resourcePackId: null
    }

    /// <summary>
    /// Формат даты для передачи серверу в виде запроса
    /// </summary>
    this.serverDateFormat = serverDateFormat;

    /// <summary>
    /// Формат даты для вывода на странице
    /// </summary>
    this.clientDateFormat = clientDateFormat;

    /// <summary>
    /// Функции для управления кнопкой раскрытия всех панелей таймлайна
    /// </summary>
    this.toggleAllFunctions = new ToggleAllPanels(false, showAllString, hideAllString, this.__selectors.showAllBtn);

    /* ---------- public методы ---------- */

    /// <summary>
    /// Инициализирует объект таймлайна, заполняет первичную конфигурацию
    /// </summary>
    this.initialize = function () {
        this.setTimelineConfig();
        var self = this;

        $(self.__selectors.addIncomeBtnSelector).click(function () {
            var url = self.__urls.addIncomeUrl + '?resourcePackId=' + self.timelineConfig.resourcePackId;
            showAndRedirectModal(url);
        });

        $(self.__selectors.addSpendingBtnSelector).click(function () {
            var url = self.__urls.addSpendingUrl + '?resourcePackId=' + self.timelineConfig.resourcePackId;
            showAndRedirectModal(url);
        });

        return this;
    }

    /// <summary>
    /// Устанавливает конфигурацию таймлайна по данным из панели конфигурации
    /// </summary>
    this.setTimelineConfig = function () {
        this.timelineConfig.toDate = moment($(this.__selectors.toDateInputSelector).val(), clientDateFormat);
        this.timelineConfig.dayCount = $(this.__selectors.dayCountInputSelector).val();
        this.timelineConfig.resourcePackId = $(this.__selectors.resourcePackInputSelector).val();
        this.reloadTimeline();
    }

    /// <summary>
    /// Загрузить данные предыдущего отрезка времени в таймлайн
    /// </summary>
    this.prevTimeline = function () {
        this.timelineConfig.toDate = this.timelineConfig.toDate.add(-this.timelineConfig.dayCount, 'days');
        this.reloadTimeline();
    }

    /// <summary>
    /// Загрузить данные следующего отрезка времени в таймлайн
    /// </summary>
    this.nextTimeline = function () {
        this.timelineConfig.toDate = this.timelineConfig.toDate.add(this.timelineConfig.dayCount, 'days');
        this.reloadTimeline();
    }

    /// <summary>
    /// Обновляет значение дефолтного ресурспака и выбирает текущий ресурспак в таймлайне, если он изменился
    /// </summary>
    this.updateResourcePackPanel = function () {
        var currentResourcePackId = getCurrentResourcePackId();
        if (this.__defaultResourcePackId != currentResourcePackId) {
            this.dropdownSelectResourcePack(currentResourcePackId);
            this.__defaultResourcePackId = currentResourcePackId;
            this.timelineConfig.resourcePackId = currentResourcePackId;
        }
    }

    /// <summary>
    /// Выбирает передаваемый идентификатор в выпадающем меню ресурспака
    /// </summary>
    this.dropdownSelectResourcePack = function (resPackId) {
        $(this.__selectors.resourcePackInputSelector).val(resPackId);
    }

    /// <summary>
    /// Показать или скрыть текущую панель на таймлайне
    /// </summary>
    this.toggleCurrentPanel = function (event) {
        var panel = $(event.currentTarget).closest('article').find('.panel-body');
        if (!$(panel).is(':hidden')) return;
        this.toggleAllFunctions.showAllPanels(false, 'article .panel-body');
        $(panel).slideDown('fast');
    }

    /// <summary>
    /// Получить данные с сервера и перерисовать таймлайн
    /// </summary>
    this.reloadTimeline = function () {
        var self = this;
        $(self.__selectors.loader).addClass("is-active");
        $.ajax({
            url: this.__urls.getTimelineDataUrl,
            data: {
                toDate: moment(this.timelineConfig.toDate).format(this.serverDateFormat),
                dayCount: this.timelineConfig.dayCount,
                resourcePackId: this.timelineConfig.resourcePackId
            }
        }).done(function (data) { self.__generateTimeline(data, self); });
    }

    /* ---------- private методы ---------- */

    this.__bindPanelEvents = function (self) {
        $('article .panel-heading').mouseenter(function (e) { self.toggleCurrentPanel(e); });
        $(self.__selectors.showAllBtn).unbind();
        $(self.__selectors.showAllBtn).click(function () { self.toggleAllFunctions.toggleAllPanels('article .panel-body'); });

        $(self.__selectors.incomeTimelineLink).click(function () {
            var url = self.__urls.viewIncomeUrl+ '?id=' + $(this).attr("id");
            showAndRedirectModal(url);
        });

        $(self.__selectors.spendingTimelineLink).click(function () {
            var url = self.__urls.viewSpendingUrl + '?id=' + $(this).attr("id");
            showAndRedirectModal(url);
        });
    }

    this.__setPageTitle = function (self) {
        var toDate = self.timelineConfig.toDate;
        var fromDate = moment(toDate).subtract(self.timelineConfig.dayCount, 'days');
        var dateFormat = self.clientDateFormat;
        $(self.__selectors.pageTitle).text(moment(fromDate).format(dateFormat) + '-' + moment(toDate).format(dateFormat));
    }

    this.__generateTimeline = function (data, self) {
        $(self.__selectors.timeline).albeTimeline(data, {
            //Анимация появления
            //Напр. 'fadeInUp', 'bounceIn', и т.д.
            effect: 'bounceIn',
            //Устанавливает видимость групп элементов
            showGroup: true,
            //Устанавливает видимость автосоздаваемого меню - якорей для групп (зависит от 'showGroup')
            showMenu: true,
            //Задает локализацию текста (i18n)
            language: 'ru-RU',
            //Задает формат вывода даты
            //'dd/MM/yyyy', 'dd MMMM yyyy HH:mm:ss' и т.д.
            formatDate: 'dd MMM',
            //Задает направление сортировки элементов
            //true: По убыванию
            //false: По возрастанию
            //null: Без сортировки
            sortDesc: null,
            resourcePackId: this.timelineConfig.resourcePackId
        });
        self.__bindPanelEvents(self);
        self.__setPageTitle(self);
        self.toggleAllFunctions.showAllPanels(false, 'article .panel-body');
        $(self.__selectors.loader).removeClass("is-active");
    }

}