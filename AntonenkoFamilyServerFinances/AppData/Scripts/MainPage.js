﻿function MainPage() {

    /* ---------- private поля ---------- */

    this.__resourcePackPage = 0;
    this.__urls = {
        getResourcePacksPage: "Web/Data/GetResourcePacksPage"
    }
    this.__selectors = {
        resourcePackList: "#resourcepack-list"
    }

    /* ---------- public методы ---------- */

    /// <summary>
    /// Загрузить данные предыдущей страницы ресурспаков
    /// </summary>
    this.prevResourcePacks = function () {
        if (this.__resourcePackPage > 0) this.__resourcePackPage--;
        this.updateResourcePacks();
    }

    /// <summary>
    /// Загрузить данные следующей страницы ресурспаков
    /// </summary>
    this.nextResourcePacks = function () {
        this.__resourcePackPage++;
        this.updateResourcePacks();
    }

    /// <summary>
    /// Загрузить данные текущей страницы ресурспаков
    /// </summary>
    this.updateResourcePacks = function () {
        var self = this;
        $.ajax({
            url: self.__urls.getResourcePacksPage,
            data: {
                page: self.__resourcePackPage
            }
        })
        .done(function (responce) {
            var resultHtml = "";
            $(responce).each(function () {
                var template = bt("id:resourcepack-row").createInstance();
                template.set('id', this.Id);
                template.set('name', this.Name);
                resultHtml += template.element.outerHTML;
            });
            $(self.__selectors.resourcePackList).html(resultHtml);
        });
    }
}