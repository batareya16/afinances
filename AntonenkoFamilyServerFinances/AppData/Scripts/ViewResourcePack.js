﻿function ViewResourcePack(
    resourcePackId,
    currencyString) {

    /* ---------- private поля ---------- */

    this.__urls = {
        datacontrollerURL: "/Web/Data/",
        getResourcesUrl: "GetResourcePackResources",
        getMandatoryCashOffUrl: "GetResourcePackMandatoryCashOffsPage",
        getCreditsUrl: "GetResourcePackCreditsPage",
        viewResource: "/Web/Resource/ViewResource",
        viewMandatoryCashOff: "/Web/MandatoryCashOff/ViewMandatoryCashOff",
        viewCredit: "/Web/Credit/ViewCredit"
    }

    this.__currencyString = currencyString;

    this.__selectors = {
        resourcesPanelSelector: "#Resources",
        creditsPanelSelector: "#Credits",
        mandatoryCashOffPanelSelector: "#MandatoryCashOffs"
    }

    this.__templates = {
        resourcesTemplate:
            "<li class='list-group-item'>" +
                "<button type='button' class='btn btn-link modal-redirect' url='{viewResourceUrl}/{id}'>{description}</button>"
            + "</li>",
        creditTemplate:
            "<li class='list-group-item'>"
                + "<button type='button' class='btn btn-link modal-redirect' url='{viewCreditUrl}/{id}'>{name}</button>"
                + ": {summ}{currency}"
            + "</li>",
        mandatoryCashOffTemplate:
            "<li class='list-group-item'>"
                + "<button type='button' class='btn btn-link modal-redirect' url='{viewMandatoryCashOffUrl}/{id}'>{description}</button>"
                + " : {summ}{currency}"
            + "</li>"
    }

    /* ---------- public поля ---------- */

    /// <summary>
    /// Идентификатор ресурспака
    /// </summary>
    this.resourcePackId = resourcePackId;

    /// <summary>
    /// Класс методов для работы с функциями для панели листа
    /// </summary>
    this.listPanelFunctions = new ListPanel(
        this.__urls.datacontrollerURL,
        null,
        this.resourcePackId);

    /// <summary>
    /// Номера страниц панелей
    /// </summary>
    this.pages = {
        credits: 0,
        mandatoryCashOffs: 0
    }

    /* ---------- public методы ---------- */

    /// <summary>
    /// Перезаполняет все панели странице данными с сервера
    /// </summary>
    this.reloadAllPanels = function () {
        this.getResources();
        this.getMandatoryCashOffs();
        this.getCredits();
    }

    /// <summary>
    /// Получить все ресурсы ресурспака и отобразить их на панели ресурсов
    /// </summary>
    this.getResources = function() {
        var self = this;
        $(this.__selectors.resourcesPanelSelector).siblings('.loader').addClass("is-active");
        self.listPanelFunctions.getPanelDataFromServer(
            self.__urls.getResourcesUrl,
            self.__selectors.resourcesPanelSelector,
            self.__resourcesTemplate,
            null,
            self,
            null);
    }

    /// <summary>
    /// Получить страницу запл.платежей ресурспака и отобразить их на панели запл.платежей
    /// </summary>
    this.getMandatoryCashOffs = function() {
        var self = this;
        $(this.__selectors.mandatoryCashOffPanelSelector).siblings('.loader').addClass("is-active");
        self.listPanelFunctions.getPanelDataFromServer(
            self.__urls.getMandatoryCashOffUrl,
            self.__selectors.mandatoryCashOffPanelSelector,
            self.__getMandatoryCashOffTemplate,
            null,
            self,
            null,
            self.pages.mandatoryCashOffs);
    }

    /// <summary>
    /// Получить страницу кредитов ресурспака и отобразить их на панели кредитов
    /// </summary>
    this.getCredits = function () {
        var self = this;
        $(this.__selectors.creditsPanelSelector).siblings('.loader').addClass("is-active");
        self.listPanelFunctions.getPanelDataFromServer(
            self.__urls.getCreditsUrl,
            self.__selectors.creditsPanelSelector,
            self.__getCreditTemplate,
            null,
            self,
            null,
            self.pages.credits);
    }

    /* ---------- private методы ---------- */

    this.__resourcesTemplate = function (resource, self) {
        var resourcesTemplate = bt(self.__templates.resourcesTemplate).createInstance();
        resourcesTemplate.set('viewResourceUrl', self.__urls.viewResource);
        resourcesTemplate.set('id', resource.Id);
        resourcesTemplate.set('description', resource.Name);
        return resourcesTemplate.element.wholeText;
    }

    this.__getCreditTemplate = function (credit, self) {
        var creditTemplate = bt(self.__templates.creditTemplate).createInstance();
        creditTemplate.set('viewCreditUrl', self.__urls.viewCredit);
        creditTemplate.set('id', credit.Id);
        creditTemplate.set('name', credit.Name);
        creditTemplate.set('summ', credit.Summ);
        creditTemplate.set('currency', self.__currencyString);
        return creditTemplate.element.wholeText;
    }

    this.__getMandatoryCashOffTemplate = function (cashOff, self) {
        var cashOffTemplate = bt(self.__templates.mandatoryCashOffTemplate).createInstance();
        cashOffTemplate.set('viewMandatoryCashOffUrl', self.__urls.viewMandatoryCashOff);
        cashOffTemplate.set('id', cashOff.Id);
        cashOffTemplate.set('description', cashOff.Description);
        cashOffTemplate.set('summ', cashOff.Summ);
        cashOffTemplate.set('currency', self.__currencyString);
        return cashOffTemplate.element.wholeText;
    }
}