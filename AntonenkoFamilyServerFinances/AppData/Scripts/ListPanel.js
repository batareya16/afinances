﻿function ListPanel(
    controllerURL,
    serverDateFormat,
    resourcePackId) {

    this.controllerURL = controllerURL;
    this.serverDateFormat = serverDateFormat;
    this.resourcePackId = resourcePackId;

    this.getPanelDataFromServer = function (
        actionURL,
        panelSelector,
        getTemplateFunc,
        resourceId,
        parent,
        currentDate,
        page,
        moduleId) {
            var self = this;
            return $.ajax({
                url: this.controllerURL + actionURL,
                data: {
                    date: moment(currentDate).format(this.serverDateFormat),
                    resourcePackId: this.resourcePackId,
                    resourceId: resourceId,
                    page: page,
                    moduleId: moduleId
                }
            })
            .done(function (result) {
                self.__fillPanelWithData(result, panelSelector, getTemplateFunc, parent);
                modalPanelFunctions.bindModalRedirectButtons();
            });
    }

    this.__fillPanelWithData = function (items, panelSelector, getTemplateFunc, self) {
        var dataHtmlString = "";
        items.forEach(function (item) {
            dataHtmlString += getTemplateFunc(item, self);
        });

        $(panelSelector).siblings('.loader').removeClass("is-active");
        $(panelSelector).html(dataHtmlString);
    }
}