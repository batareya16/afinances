﻿function ModalPanel() {

    /* ---------- public поля ---------- */

    /// <summary>
    /// Селектор модального окна
    /// </summary>
    this.modalWindowSelector = ".modal";

    /// <summary>
    /// Селектор модальной формы
    /// </summary>
    this.modalFormSelector = ".modal-dialog form";

    /* ---------- public методы ---------- */

    /// <summary>
    /// Метод инициализации модальной формы
    /// </summary>
    this.initializeModal = function () {
        $(this.modalFormSelector).validate();
        this.bindModalRedirectButtons();
        initializeDateTimePickers();
        initializeSpinner(this.modalFormSelector + " .loader");
        this.__bindJqueryValidator();
    }

    /// <summary>
    /// Отправить модальную форму POST-запросом на указанный URL
    /// </summary>
    this.submitModal = function (url) {
        this.__modalFormGotoUrl(url, true);
    }

    /// <summary>
    /// Заблокировать, или разблокировать все поля ввода в модальной форме
    /// </summary>
    this.toggleForm = function (blocked) {
        $(this.modalFormSelector + " :input").prop("disabled", blocked);
    }

    /// <summary>
    /// Отправить модальную форму GET-запросом на указанный URL
    /// </summary>
    this.redirectModal = function (url) {
        //Отключает валидацию перед переадресацией с помощью GET-запроса
        nullable(nullable($("#ModalPanel form").validate())['settings']).ignore = "*";
        this.__modalFormGotoUrl(url, false);
    }

    /// <summary>
    /// Создает событие открытия модальной панели при нажатии на определенные кнопки
    /// </summary>
    this.bindModalRedirectButtons = function () {
        var self = this;
        $(".modal-redirect").unbind("click");
        $('.modal-redirect').click(function () {
            $(self.modalWindowSelector).modal('show');
            self.redirectModal($(this).attr('url'));
        });
    }

    /* ---------- private методы ---------- */

    this.__bindJqueryValidator = function () {
        var $form = $(this.modalFormSelector);
        $form.unbind();
        $form.data("validator", null);
        $.validator.unobtrusive.parse(document);
        $form.validate($form.data("unobtrusiveValidation").options);
    }

    this.__modalFormGotoUrl = function (url, isPostRequest) {
        if (url) $(this.modalFormSelector).attr("action", url);
        var modalForm = isPostRequest ? "post" : "get";
        $(this.modalFormSelector).attr("method", modalForm);
        $(this.modalFormSelector).submit();
    }
}