﻿function ViewDateBudget(
    resourcePackId,
    dateString,
    dateFormat,
    serverDateFormat,
    viewSpendingUrl,
    viewIncomeUrl,
    viewDateBudgetUrl,
    viewCreditUrl,
    viewMandatoryCashOffUrl,
    currencyString,
    allString) {

    /* ---------- private поля ---------- */

    this.__controllerURL = "/Web/Data/";
    this.__budgetControllerUrl = "/Web/Budget/";
    this.__currencyString = currencyString;
    this.__allString = allString;
    this.__actionURLs = {
        getResources: "GetResourcePackResources",
        getIncomes: "GetResourceIncomesOnDate",
        getSpendings: "GetResourceSpendingsOnDate",
        getCredits: "GetResourcePackCreditsOnDate",
        getMandatoryCashOffs: "GetMandatoryCashOffsOnDate",
        getDateBudget: "GetDateBudget",
        viewDateBudget: "ViewDateBudget"
    };

    this.__selectors = {
        incomePanel: "#Incomes",
        spendingPanel: "#Spendings",
        creditsPanel: "#Credits",
        mandatoryCashOffPanel: "#MandatoryCashOffs",
        resourceDropDown: "#ResourceId",
        dateTextSelector: "#DateText",
        balanceSelector: "#Balance",
        summaryCreditSelector: "#SummaryCredit",
        nextPrevBtnSelector: ".next-prev-btn"
    }

    this.__viewItemURLs = {
        viewIncome: viewIncomeUrl,
        viewSpending: viewSpendingUrl,
        viewCredit: viewCreditUrl,
        viewMandatoryCashOff: viewMandatoryCashOffUrl
    }

    this.__templates = {
        incomeTemplate: 
            "<li class='list-group-item'>" +
                "<button type='button' class='btn btn-link modal-redirect' url='{viewIncomeUrl}/{id}'>{description}</button>"
                + ": {summ}{currency} ({resourceName})"
            + "</li>",
        spendingTemplate:
            "<li class='list-group-item'>" +
                "<button type='button' class='btn btn-link modal-redirect' url='{viewSpendingUrl}/{id}'>{description}</button>"
                + ": {price}{currency} ({resourceName})"
            + "</li>",
        creditTemplate:
            "<li class='list-group-item'>"
                + "<button type='button' class='btn btn-link modal-redirect' url='{viewCreditUrl}/{id}'>{name}</button>"
                + ": {summ}{currency}"
            + "</li>",
        mandatoryCashOffTemplate:
            "<li class='list-group-item'>"
                + "<button type='button' class='btn btn-link modal-redirect' url='{viewMandatoryCashOffUrl}/{id}'>{description}</button>"
                + " : {summ}{currency}"
            + "</li>",
        dropDownDefaultItemTemplate: "<option value='0'>{all}</option>",
        resourceDropDownItemTemplate: "<option value='{id}'>{name}</option>"
    }

    this.__defaultResourcePackId = getCurrentResourcePackId();

    /* ---------- public поля ---------- */

    /// <summary>
    /// Идентификатор ресурспака
    /// </summary>
    this.resourcePackId = resourcePackId;

    /// <summary>
    /// Формат даты для вывода на странице
    /// </summary>
    this.dateFormat = dateFormat;

    /// <summary>
    /// Формат даты для передачи серверу в виде запроса
    /// </summary>
    this.serverDateFormat = serverDateFormat;

    /// <summary>
    /// Адрес страницы просмотра бюджета на определенную дату
    /// </summary>
    this.viewDateBudgetUrl = viewDateBudgetUrl;

    /// <summary>
    /// Актуальная дата, для которой отображается бюджет на странице
    /// </summary>
    this.currentDate = new Date(dateString);

    /// <summary>
    /// Класс методов для работы с функциями для панели листа
    /// </summary>
    this.listPanelFunctions = new ListPanel(
        this.__controllerURL,
        this.serverDateFormat,
        this.resourcePackId);

    /* ---------- public методы ---------- */

    /// <summary>
    /// Получает список ресурсов ресурспака и заполняет выпадающий список на странице
    /// </summary>
    this.fillResourcePackResources = function () {
        var self = this;
        $.ajax({
            url: self.__controllerURL + self.__actionURLs.getResources,
            data: { resourcePackId: this.resourcePackId }
        }).done(function (resources) {
            self.__fillDropDownWithData(
                    resources,
                    self.__selectors.resourceDropDown,
                    self.__getDefaultDropDownItemTemplate(self),
                    self.__getDropDownOptionTemplate,
                    self);
        });
    }

    /// <summary>
    /// Получает прибыль на определенную дату с сервера определенного ресурса (или всего ресурспака)
    /// и заполняет лист прибыли на странице
    /// </summary>
    this.getResourceIncomes = function () {
        $(this.__selectors.incomePanel).siblings('.loader').addClass("is-active");
        var resourceId = $(this.__selectors.resourceDropDown).val();
        this.listPanelFunctions.getPanelDataFromServer(
            this.__actionURLs.getIncomes,
            this.__selectors.incomePanel,
            this.__getIncomeTemplate,
            resourceId,
            this,
            this.currentDate);
    }

    /// <summary>
    /// Получает затраты на определенную дату с сервера определенного ресурса (или всего ресурспака)
    /// и заполняет лист затрат на странице
    /// </summary>
    this.getResourceSpendings = function () {
        $(this.__selectors.spendingPanel).siblings('.loader').addClass("is-active");
        var resourceId = $(this.__selectors.resourceDropDown).val();
        this.listPanelFunctions.getPanelDataFromServer(
            this.__actionURLs.getSpendings,
            this.__selectors.spendingPanel,
            this.__getSpendingTemplate,
            resourceId,
            this,
            this.currentDate);
    }

    /// <summary>
    /// Получает кредиты на определенную дату с сервера определенного ресурспака
    /// и заполняет лист кредитов на странице
    /// </summary>
    this.getResourceCredits = function () {
        $(this.__selectors.creditsPanel).siblings('.loader').addClass("is-active");
        var resourceId = $(this.__selectors.resourceDropDown).val();
        this.listPanelFunctions.getPanelDataFromServer(
            this.__actionURLs.getCredits,
            this.__selectors.creditsPanel,
            this.__getCreditTemplate,
            resourceId,
            this,
            this.currentDate);
    }

    /// <summary>
    /// Получает обязательные платежи на определенную дату с сервера определенного ресурспака
    /// и заполняет лист обязательных платежей на странице
    /// </summary>
    this.getMandatoryCashOffs = function () {
        $(this.__selectors.mandatoryCashOffPanel).siblings('.loader').addClass("is-active");
        var resourceId = $(this.__selectors.resourceDropDown).val();
        this.listPanelFunctions.getPanelDataFromServer(
            this.__actionURLs.getMandatoryCashOffs,
            this.__selectors.mandatoryCashOffPanel,
            this.__getMandatoryCashOffTemplate,
            resourceId,
            this,
            this.currentDate);
    }

    /// <summary>
    /// Загрузить данные предыдущего дня на страницу
    /// </summary>
    this.setPreviousDate = function () {
        this.currentDate = moment(this.currentDate).subtract(1, 'days').toDate();
        this.updateModel();
    }

    /// <summary>
    /// Загрузить данные следующего дня на страницу
    /// </summary>
    this.setNextDate = function () {
        this.currentDate = moment(this.currentDate).add(1, 'days').toDate();
        this.updateModel();
    }

    /// <summary>
    /// Обновить все данные на странице
    /// </summary>
    this.updateModel = function () {
        var self = this;
        $(self.__selectors.nextPrevBtnSelector).attr("disabled", "disabled");
        $.ajax({
            url: self.__controllerURL + self.__actionURLs.getDateBudget,
            data: {
                date: moment(self.currentDate).format(self.serverDateFormat),
                resourcePackId: self.resourcePackId
            }
        }).done(function (budget) {
            $(self.__selectors.dateTextSelector).html(moment(self.currentDate).format(self.dateFormat));
            $(self.__selectors.balanceSelector).html(roundMoneyVal(budget.Balance - budget.SummaryCredit));
            $(self.__selectors.summaryCreditSelector).html(roundMoneyVal(budget.SummaryCredit));
            history.pushState(null, null, self.__getActualUrl(self));
            self.reloadData();
            $(self.__selectors.nextPrevBtnSelector).removeAttr("disabled");
        });
    }

    /// <summary>
    /// Перезаполняет данные прибыли, затрат, кредитов на странице
    /// </summary>
    this.reloadData = function () {
        this.getResourceIncomes();
        this.getResourceSpendings();
        this.getResourceCredits();
        this.getMandatoryCashOffs();
    }

    /// <summary>
    /// Обновляет значение ресурспака и перезагружает страницу с другим ресурспаком, если он изменился
    /// </summary>
    this.updateCurrentResourcePack = function () {
        var currentResourcePackId = getCurrentResourcePackId();
        if (this.__defaultResourcePackId != currentResourcePackId) {
            this.resourcePackId = currentResourcePackId;
            window.location.href = this.__getActualUrl(this);
        }
    }

    /* ---------- private методы ---------- */

    this.__getActualUrl = function(self) {
        return self.viewDateBudgetUrl + "?ResourcePackId=" + self.resourcePackId + "&Date=" + moment(self.currentDate).format(self.serverDateFormat);
    }

    this.__getIncomeTemplate = function (income, self) {
        var incomeTemplate = bt(self.__templates.incomeTemplate).createInstance();
        incomeTemplate.set('viewIncomeUrl', self.__viewItemURLs.viewIncome);
        incomeTemplate.set('id', income.Id);
        incomeTemplate.set('description', income.Description);
        incomeTemplate.set('summ', roundMoneyVal(income.Summ));
        incomeTemplate.set('currency', self.__currencyString);
        incomeTemplate.set('resourceName', income.Resource.Name);
        return incomeTemplate.element.wholeText;
    }

    this.__getSpendingTemplate = function (spending, self) {
        var spendingTemplate = bt(self.__templates.spendingTemplate).createInstance();
        spendingTemplate.set('viewSpendingUrl', self.__viewItemURLs.viewSpending);
        spendingTemplate.set('id', spending.Id);
        spendingTemplate.set('description', spending.Description);
        spendingTemplate.set('price', roundMoneyVal(spending.Price));
        spendingTemplate.set('currency', self.__currencyString);
        spendingTemplate.set('resourceName', spending.Resource.Name);
        return spendingTemplate.element.wholeText;
    }

    this.__getCreditTemplate = function (credit, self) {
        var creditTemplate = bt(self.__templates.creditTemplate).createInstance();
        creditTemplate.set('viewCreditUrl', self.__viewItemURLs.viewCredit);
        creditTemplate.set('id', credit.Id);
        creditTemplate.set('name', credit.Name);
        creditTemplate.set('summ', roundMoneyVal(credit.Summ));
        creditTemplate.set('currency', self.__currencyString);
        return creditTemplate.element.wholeText;
    }

    this.__getMandatoryCashOffTemplate = function (cashOff, self) {
        var cashOffTemplate = bt(self.__templates.mandatoryCashOffTemplate).createInstance();
        cashOffTemplate.set('viewMandatoryCashOffUrl', self.__viewItemURLs.viewMandatoryCashOff);
        cashOffTemplate.set('id', cashOff.Id);
        cashOffTemplate.set('description', cashOff.Description);
        cashOffTemplate.set('summ', cashOff.Summ);
        cashOffTemplate.set('currency', self.__currencyString);
        return cashOffTemplate.element.wholeText;
    }

    this.__getDefaultDropDownItemTemplate = function (self) {
        var template = bt(self.__templates.dropDownDefaultItemTemplate).createInstance();
        template.set('all', self.__allString);
        return template.element.wholeText;
    }

    this.__getDropDownOptionTemplate = function (item, self) {
        var dropdownTemplate = bt(self.__templates.resourceDropDownItemTemplate).createInstance();
        dropdownTemplate.set('id', item.Id);
        dropdownTemplate.set('name', item.Name);
        return dropdownTemplate.element.wholeText;
    }

    this.__fillDropDownWithData = function (items, dropDownSelector, defaultItemString, getTemplateFunc, self) {
        var dataHtmlString = defaultItemString;
        $(items).each(function () {
            dataHtmlString += getTemplateFunc(this, self);
        });

        $(dropDownSelector).html(dataHtmlString);
    }
}