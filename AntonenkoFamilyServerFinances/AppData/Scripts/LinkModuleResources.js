﻿function LinkModuleResources() {

    /* ---------- private поля ---------- */

    this.__urls = {
        controllerURL: "/Web/Data/",
        getResources: "GetResourcePackResources"
    }

    this.__listPanelFunctions = new ListPanel(
        this.__urls.controllerURL,
        null,
        this.resourcePackId);

    this.__selectors = {
        resourcePack: "#ResourcePackId",
        resources: "#ResourceId",
        selectedResources: "#SelectedResources",
        loader: "#Loader",
        listItemsSelector: "#SelectedResources .list-group-item",
        listItemsJsonSelector: "#SelectedResourcesIdsJson"
    }

     this.__templates = {
        resourceDropDownItemTemplate: "<option value='{id}'>{name}</option>",
        resourceListItemTemplate: "<li class='list-group-item' value='{id}'>{name}</li>"
    }

    /* ---------- public методы ---------- */

    /// <summary>
    /// Получает список ресурсов ресурспака, исключенные выбранные для модуля
    /// </summary>
    this.getResources = function() {
        $(this.__selectors.loader).addClass("is-active");
        this.__listPanelFunctions.resourcePackId = $(this.__selectors.resourcePack).val();
        this.__listPanelFunctions.getPanelDataFromServer(
            this.__urls.getResources,
            this.__selectors.resources,
            this.__getDropDownOptionTemplate,
            null,
            this);
    }

    /// <summary>
    /// Выбирает ресурс в списке ресурсов модуля
    /// </summary>
    this.selectResource = function (element) {
        $(this.__selectors.listItemsSelector).removeClass("active");
        $(element).addClass("active");
    }

    /// <summary>
    /// Привязывает ресурс к модулю
    /// </summary>
    this.addResource = function () {
        modalPanelFunctions.toggleForm(true);
        var item = $(this.__selectors.resources + " :selected");
        var listItemHTML = this.__getListItemTemplate({ Id: item.val(), Name: item.text() }, this);
        var selectedResources = $(this.__selectors.selectedResources);
        if (item.val() != null) selectedResources.html(selectedResources.html() + listItemHTML);
        this.getResources();
        this.bindSelectedResources();
    }

    /// <summary>
    /// Удаляет ресурс из выбранных для модуля
    /// </summary>
    this.removeResource = function () {
        modalPanelFunctions.toggleForm(true);
        $(this.__selectors.listItemsSelector + ".active").remove();
        this.getResources();
    }

    /// <summary>
    /// Создает необходимые события для списка выбранных ресурсов для модуля
    /// </summary>
    this.bindSelectedResources = function () {
        var self = this;
        var $selectedItems = $(this.__selectors.listItemsSelector);
        $selectedItems.unbind();
        $selectedItems.click(function () { self.selectResource(this); });
    }

    /// <summary>
    /// Сохраняет список выбранных ресурсов в виде JSON строки в скрытый input
    /// </summary>
    this.saveSelectedResourcesNamesJson = function () {
        var selectedIds = JSON.stringify($(this.__selectors.listItemsSelector).map(function () { return this.value }).get());
        $(this.__selectors.listItemsJsonSelector).val(selectedIds);
    }

    /* ---------- private методы ---------- */

    this.__getDropDownOptionTemplate = function (item, self) {
        var template = bt(self.__templates.resourceDropDownItemTemplate).createInstance();
        template.set('id', item.Id);
        template.set('name', item.Name);
        return template.element.wholeText;
    }

    this.__getListItemTemplate = function (item, self) {
        var template = bt(self.__templates.resourceListItemTemplate).createInstance();
        template.set('id', item.Id);
        template.set('name', item.Name);
        return template.element.wholeText;
    }

    this.__listPanelFunctions.__fillPanelWithData = function (items, panelSelector, getTemplateFunc, self) {
        var dataHtmlString = "";
        items.forEach(function (item) {
            if (!self.__isExistResource(item.Id) && item.Id != null) dataHtmlString += getTemplateFunc(item, self);
        });

        $(self.__selectors.loader).removeClass("is-active");
        modalPanelFunctions.toggleForm(false);
        $(panelSelector).html(dataHtmlString);
    }

    this.__isExistResource = function (id) {
        return !!$(this.__selectors.selectedResources).find("[value={0}]".format(id)).length;
    }
}