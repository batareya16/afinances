var dateTimePickerFormat;

Date.prototype.addMonthsToDate = function (num) {
    var dateObj = moment(new Date(this.valueOf()));
    dateObj = dateObj.add(num, 'months');
    return new Date(dateObj.format("MM/DD/YYYY"));
}

Date.prototype.addDaysToDate = function (days) {
    var dateObj = moment(new Date(this.valueOf()));
    dateObj = dateObj.add(days, 'days');
    return new Date(dateObj.format("MM/DD/YYYY"));
}

String.prototype.format = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

function fixToggleValue($toggle) {
    if (!$toggle.closest('.toggle').hasClass('off')) {
        $toggle.attr("checked", "checked");
        $toggle.val("True");
    }
    else {
        $toggle.removeAttr("checked");
        $toggle.val("False");
    }
}

function roundMoneyVal(val) {
    if (isNaN(val)) return false;
    return Math.round(val * 100) / 100;
}

function setDateTimePickerFormat(format) {
    dateTimePickerFormat = format;
}

function initializeDateTimePickers() {
    $('.datepicker').datepicker({ dateFormat: dateTimePickerFormat });
}

function initializeSpinner(spinnerSelector) {
    var modalSpinner = new Spinner().spin();
    $(spinnerSelector).append(modalSpinner.el);
}

function fixJQueryValidator() {
    $.validator.methods.range = function (value, element, param) {
        var globalizedValue = value.replace(",", ".");
        return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
    }

    $.validator.methods.number = function (value, element) {
        return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
    }
}

function fixFloatingDot(fieldSelector) {
    // Сделано в связи с тем, что сервер не принимает точку как разделитель, а клиентская часть - наоборот.
    $(fieldSelector).val($(fieldSelector).val().replace('.', ','));
}

function getBool(val) {
    var num = +val;
    return !isNaN(num) ? !!num : !!String(val).toLowerCase().replace(!!0, '');
}

function positive(val) {
    return Math.max(0, val);
}

function nullable(obj) {
    return obj || '';
}

function initializeFileInputs() {
    $(".input-file").before(
		function () {
		    if (!$(this).prev().hasClass('input-ghost')) {
		        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
		        element.attr("name", $(this).attr("name"));
		        element.change(function () {
		            element.next(element).find('input').val((element.val()).split('\\').pop());
		        });
		        $(this).find("button.btn-choose").click(function () {
		            element.click();
		        });
		        $(this).find("button.btn-reset").click(function () {
		            element.val(null);
		            $(this).parents(".input-file").find('input').val('');
		        });
		        $(this).find('input').css("cursor", "pointer");
		        $(this).find('input').mousedown(function () {
		            $(this).parents('.input-file').prev().click();
		            return false;
		        });
		        return element;
		    }
		}
	);
}

function fixAjaxFormFileUploading() {
    window.addEventListener("submit", function (e) {
        var form = e.target;
        if (form.getAttribute("enctype") === "multipart/form-data") {
            if (form.dataset.ajax) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var xhr = new XMLHttpRequest();
                xhr.open(form.method, form.action);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        if (form.dataset.ajaxUpdate) {
                            var updateTarget = document.querySelector(form.dataset.ajaxUpdate);
                            if (updateTarget) {
                                updateTarget.innerHTML = xhr.responseText;
                            }
                        }
                    }
                };
                xhr.send(new FormData(form));
            }
        }
    }, true);
}

function serializeForm(formSelector) {
    var serialized = $(formSelector).serializeArray();
    $(formSelector + " input:checkbox").each(function () {
        serialized.push({ name: this.name, value: getBool(this.checked) });
    });
    return JSON.stringify(serialized.reduce(function (mem, x) { mem[x.name] = x.value; return mem; }, {}));
}

function getCurrentResourcePack() {
    var cookie = $.cookie('ResourcePack');
    return cookie != null ? JSON.parse(cookie) : null;
}

function getCurrentResourcePackId() {
    return nullable(nullable(getCurrentResourcePack())['ResourcePack'])['Id'];
}

function getCurrentUserGroupId() {
    return nullable(nullable(getCurrentResourcePack())['UserGroup'])['Id'];
}

function setupCurrentResourcePack(emptyString) {
    var resPack = getCurrentResourcePack();
    $("[name=currentResourcePackName]").html(
        resPack != null && resPack['ResourcePack'] != null
            ? resPack.ResourcePack['Name']
            : emptyString);
    $("[name=currentUserGroupName]").html(
        resPack != null && resPack['UserGroup'] != null
            ? resPack.UserGroup['Name']
            : emptyString);
}