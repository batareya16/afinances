﻿function UserGroupSearchDropDown () {

    /* ---------- private поля ---------- */

    this.__dropdownUrls = {
        searchUserGroups: "/Web/Data/SearchUserGroupsFuzzy"
    };

    this.__dropdownTemplates = {
        dropdownTemplate: "<a href='javascript:void(0)' class='usergroup-search-result' usergroup-id='{id}'>{name}</a>"
    };
    
    /* ---------- public поля ---------- */

    this.selectors = {
        userGroupSearchField: "#UserGroupSearch",
        deselectUserGroupBtn: "#DeselectUserGroup",
        userGroupPanel: "#SelectedUserGroupPanel",
        userGroupName: "#UserGroupName",
        userGroupId: "#UserGroupId",
        userGroupsDropDown: ".user-search-results",
        userGroupSearchResult: ".usergroup-search-result"
    };

    /* ---------- public методы ---------- */

    /// <summary>
    /// Метод инициализации обработчиков на странице
    /// </summary>
    this.initialize = function () {
        var self = this;
        $(this.selectors.deselectUserGroupBtn).click(function (e) {
            $(self.selectors.userGroupPanel).hide();
            $(self.selectors.userGroupName).html('');
            $(self.selectors.userGroupId).val('');
            self.onDeselectedUserGroup(self);
        });

        return self;
    };

    /// <summary>
    /// Метод поиска групп пользователей и отображение результатов в выпадающем меню
    /// </summary>
    this.findUserGroups = function (str, checkOwnership) {
        var self = this;
        $.ajax({
            type: "POST",
            url: self.__dropdownUrls.searchUserGroups,
            data: {
                query: $(self.selectors.userGroupSearchField).val(),
                selectedUserGroupId: $(self.selectors.userGroupId).val(),
                checkOwnership: checkOwnership
            },
            success: function (data) {
                self.__displaySearchResults(data, self);
                self.__bindSelectUserGroupFunctions(self);
            }
        });
    };

    /// <summary>
    /// Метод-заглушка, который вызывается после выбора группы пользователей
    /// </summary>
    this.onUserGroupSelected = function (e, self) {};

    /// <summary>
    /// Метод-заглушка, который вызывается после отмены выбора группы пользователей
    /// </summary>
    this.onDeselectedUserGroup = function (self) {};

    /* ---------- private методы ---------- */

    this.__selectUserGroup = function (e, self) {
        $(self.selectors.userGroupName).html(e.target.text);
        $(self.selectors.userGroupId).val($(e.target).attr('usergroup-id'));
        $(self.selectors.userGroupPanel).show();
        $(self.selectors.userGroupsDropDown).html('');
        $(self.selectors.userGroupSearchField).val('');
        $(self.selectors.userGroupsDropDown).hide();
        this.onUserGroupSelected(e, self);
    };

    this.__bindSelectUserGroupFunctions = function (self) {
        $(self.selectors.userGroupSearchResult).on("click", function (e) {
            self.__selectUserGroup(e, self);
        });
    };

    this.__displaySearchResults = function (userGroups, self) {
        var innerHtml = "";
        if (userGroups.length <= 0) {
            $(self.selectors.userGroupsDropDown).hide();
            return;
        }

        $(userGroups).each(function () {
            var dropdownTemplate = bt(self.__dropdownTemplates.dropdownTemplate).createInstance();
            dropdownTemplate.set('name', this.Name);
            dropdownTemplate.set('id', this.Id);
            innerHtml += dropdownTemplate.element.wholeText;
        });

        $(self.selectors.userGroupsDropDown).html(innerHtml);
        $(self.selectors.userGroupsDropDown).show();
    }
}