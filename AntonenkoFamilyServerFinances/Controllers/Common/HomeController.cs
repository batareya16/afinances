using System.Web.Mvc;

namespace AntonenkoFamilyServerFinances.Controllers.Common
{
    /// <summary>
    /// Контроллер, который нужен только чтобы перенаправлять на главную страницу /Web без самого префикса
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Перенаправляет на главную страницу программы
        /// </summary>
        /// <remarks>
        /// Нужен, чтобы без префиксов перейти на /Web/Home
        /// </remarks>
        /// <returns>Перенаправляет на главную страницу программы</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return RedirectPermanent("/Web/Home/Index");
        }
    }
}