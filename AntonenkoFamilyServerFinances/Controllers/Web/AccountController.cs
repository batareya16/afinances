﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using AFinances.PresentationLayer.Resources;
using AFinances.PresentationLayer.Resources.Views;
using AutoMapper;
using BusinessLayer.Entities;
using BusinessLayer.Infrastructure;
using BusinessLayer.Interfaces;
using BusinessLayer.Resources;
using DataLayer;
using DataLayer.Models;
using Newtonsoft.Json;
using StructureMap;
using System.Web;
using AFinances.PresentationLayer.Infrastructure;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер управления пользователями системы
    /// </summary>
    public class AccountController : Controller
    {
        /// <summary>
        /// Сервис логгирования
        /// </summary>
        private ILoggingService loggingService;

        /// <summary>
        /// Сервис полнотекстового поиска
        /// </summary>
        private ISearchService searchService;

        /// <summary>
        /// Сервис ресурспаков
        /// </summary>
        private IResourcePackService resourcePackService;

        /// <summary>
        /// Сервис текущего используемого ресурспака
        /// </summary>
        private ICurrentResourcePackService currentResPackService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public AccountController()
        {
            loggingService = ObjectFactory.GetInstance<ILoggingService>();
            searchService = ObjectFactory.GetInstance<ISearchService>();
            resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            currentResPackService = ObjectFactory.GetInstance<ICurrentResourcePackService>();
        }

        /// <summary>
        /// Страница аутентификации в систему
        /// </summary>
        /// <returns>Возвращает представление страницы аутентификации в систему</returns>
        [HttpGet]
        public ActionResult LogOn()
        {
            return View();
        }

        /// <summary>
        /// Аутентифицирует пользователя в программе
        /// </summary>
        /// <param name="model">Модель аутентификации пользователя</param>
        /// <param name="returnUrl">Предыдущая страница, на которую необходимо вернуться</param>
        /// <returns>Переадресовывает на предыдущую страницу</returns>
        [HttpPost]
        public ActionResult LogOn(LogOnEntity model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    SetCurrentResourcePackInCookies(model.UserName);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return View(model);
        }

        /// <summary>
        /// Вылогинивает пользователя из системы
        /// </summary>
        /// <returns>Переадресовывает на главную страницу в программе</returns>
        [HttpGet]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Страница создания пользователя
        /// </summary>
        /// <returns>Возвращает частичное представление страницы создания пользователя</returns>
        [Authorize]
        public ActionResult CreateUser()
        {
            return PartialView();
        }

        /// <summary>
        /// Создает пользователя в системе
        /// </summary>
        /// <param name="model">Сущность создания пользователя</param>
        /// <returns>Возвращает частичное представление успешного создания пользователя</returns>
        [Authorize]
        [HttpPost]
        public ActionResult CreateUser (CreateUserEntity model)
        {
            if (ModelState.IsValid)
            {
                MembershipCreateStatus createStatus;
                var createdUser = Membership.CreateUser(model.UserName, model.Password, model.UserName, null, null, true, null, out createStatus);
                if (createStatus == MembershipCreateStatus.Success)
                {
                    loggingService.TrackCreatingObject(Mapper.Map<BaseUserEntity>(createdUser), Membership.GetUser().UserName);
                    searchService.AddToIndex(new User() { UserName = model.UserName });
                    return PartialView("MessageModalForm", new KeyValuePair<string, string>(
                            Titles.CreateUser,
                            string.Format(
                                AFinances.PresentationLayer.Resources.Views.Common.UserCreatedSuccsessfully,
                                CommonHelper.GenerateLink(
                                    Url.Action(
                                        "ViewAccount",
                                        new { name = createdUser.UserName }),
                                        AFinances.PresentationLayer.Resources.Views.Common.User))));
                }
                else
                {
                    ModelState.AddModelError("", MembershipErrors.ResourceManager.GetString(createStatus.ToString()));
                }
            }
            return PartialView(model);
        }

        /// <summary>
        /// Страница изменения пароля
        /// </summary>
        /// <returns>Возвращает частичное представление страницы изменения пароля</returns>
        [Authorize]
        public ActionResult ChangePassword()
        {
            return PartialView();
        }

        /// <summary>
        /// Изменяет пароль текущего пользователя
        /// </summary>
        /// <param name="model">Сущность изменения пароля</param>
        /// <returns>Возвращает частичное представление страницы успешного изменения пароля</returns>
        [Authorize]
        [HttpPost]
        public PartialViewResult ChangePassword(ChangePasswordEntity model)
        {
            if (ModelState.IsValid)
            {
                bool changePasswordSucceeded = false;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                    return PartialView("MessageModalForm", new KeyValuePair<string, string>(
                            Titles.ChangePassword,
                            AFinances.PresentationLayer.Resources.Views.Common.PasswordChangedSuccsessfully));
                else
                    ModelState.AddModelError(string.Empty, Errors.ChangePasswordFail);
            }
            return PartialView(model);
        }

        [Authorize]
        [HttpGet]
        public PartialViewResult ChangeCurrentResourcePack()
        {
            var cookie = Request.Cookies.Get(Constants.CurrentResourcePackCookieName);
            return cookie != null && cookie.Value != string.Empty
                ? PartialView(JsonConvert.DeserializeObject<CurrentResourcePackEntity>(cookie.Value))
                : PartialView(new CurrentResourcePackEntity());
        }

        [Authorize]
        [HttpPost]
        public PartialViewResult ChangeCurrentResourcePack(CurrentResourcePackEntity currentResourcePack)
        {
            currentResourcePack.UserName = Membership.GetUser().UserName;
            if (currentResourcePack.CurrentResourcePackId.HasValue)
            {
                AuthorizeHelper.AuthorizeUserGroupByResourcePack(currentResourcePack.CurrentResourcePackId.Value);
                var userGroupIdFromDb = resourcePackService.FindById(currentResourcePack.CurrentResourcePackId.Value).UserGroupId;
                if (userGroupIdFromDb != currentResourcePack.CurrentUserGroupId.Value)
                {
                    throw new InvalidOperationException();
                }
            }

            currentResPackService.SaveCurrentResourcePack(currentResourcePack);
            SetCurrentResourcePackInCookies(currentResourcePack.UserName);
            return PartialView("UpdateCurrentResPack");
        }

        /// <summary>
        /// Страница просмотра действий пользователя
        /// </summary>
        /// <param name="page">Номер страницы элементов действий пользователя</param>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Возвращает частичное представление страницы просмотра действий пользователя</returns>
        [Authorize]
        [HttpPost]
        public PartialViewResult GetUserActions(int page)
        {
            var actions = loggingService.GetPageOfLogsOfUser(Membership.GetUser().UserName, page);
            return PartialView(actions);
        }

        /// <summary>
        /// Страница просмотра текущего пользователя
        /// </summary>
        /// <returns>Возвращает страницу просмотра текущего пользователя</returns>
        [Authorize]
        [HttpGet]
        public ActionResult ViewCurrentAccount()
        {
            var user = Mapper.Map<ViewAccountEntity>(Membership.GetUser());
            return View(user);
        }

        /// <summary>
        /// Страница просмотра пользователя
        /// </summary>
        /// <param name="name">Имя пользователя</param>
        /// <returns>Возвращает страницу просмотра пользователя</returns>
        [Authorize]
        [HttpGet]
        public ActionResult ViewAccount(string name)
        {
            var user = Mapper.Map<ViewAccountEntity>(Membership.GetUser(name));
            return View(user);
        }

        /// <summary>
        /// Добавляет объект текущего используемого ресурспака в куки
        /// </summary>
        /// <param name="currResPack">Имя текущего пользователя</param>
        private void SetCurrentResourcePackInCookies(string userName)
        {
            var currResPack = currentResPackService.GetCurrentResourcePack(userName);

            if (currResPack != null)
            {
                currResPack.UserName = null;
                Response.Cookies.Add(new HttpCookie(Constants.CurrentResourcePackCookieName, JsonConvert.SerializeObject(currResPack)));
            }
            else
            {
                Response.Cookies.Remove(Constants.CurrentResourcePackCookieName);
            }
        }
    }
}
