﻿using System.Web.Mvc;
using BusinessLayer.Interfaces;
using StructureMap;
using BusinessLayer.Entities;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер домашней страницы и базовых страниц программы
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// Сервис бюджета
        /// </summary>
        private IBudgetService<IncomeRequestEntity> budgetService;

        /// <summary>
        /// Базовый контроллер
        /// </summary>
        public HomeController()
        {
            budgetService = ObjectFactory.GetInstance <IBudgetService<IncomeRequestEntity>>();
        }

        /// <summary>
        /// Главная страница программы
        /// </summary>
        /// <returns>Возвращает представление главной страницы программы</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View(budgetService.GetBudgetResourcePacks());
        }

        /// <summary>
        /// Страница "О программе"
        /// </summary>
        /// <returns>Возвращает представление страницы "О программе"</returns>
        [HttpGet]
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Страница пожертвований
        /// </summary>
        /// <returns>Возвращает представление страницы пожертвований</returns>
        [HttpGet]
        public ActionResult Donate()
        {
            return PartialView();
        }
    }
}
