﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BusinessLayer.Interfaces;
using StructureMap;
using BusinessLayer.Entities;
using AutoMapper;
using System.Web.Security;
using BusinessLayer.Resources;
using AFinances.PresentationLayer.Infrastructure;

namespace DataLayer.Controllers.Web
{
    /// <summary>
    /// Контроллер страниц управления кредитами
    /// </summary>
    [Authorize]
    public class CreditController : Controller
    {
        /// <summary>
        /// Сервис кредитов
        /// </summary>
        private ICreditService creditService;

        /// <summary>
        /// Сервис ресурспаков
        /// </summary>
        private IResourcePackService resourcePackService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public CreditController()
        {
            creditService = ObjectFactory.GetInstance<ICreditService>();
            resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
        }

        /// <summary>
        /// Страница просмотра кредита
        /// </summary>
        /// <param name="id">Идентификатор кредита</param>
        /// <returns>Перенаправляет на страницу просмотра кредита</returns>
        [HttpGet]
        public ActionResult ViewCredit(long id)
        {
            var credit = Mapper.Map<CreditRequestEntity>(creditService.FindByIdWithRelatedData(id));
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(credit.ResourcePackId);
            return PartialView(credit);
        }

        /// <summary>
        /// Страница добавления кредита
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Перенаправляет на страницу добавления кредита</returns>
        [HttpGet]
        public ActionResult AddCredit(long resourcePackId)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var credit = new CreditRequestEntity() { ResourcePackId = resourcePackId, StartDate = DateTime.Today, EndDate = DateTime.Today.AddMonths(1) };
            credit.ResourcePacks = resourcePackService
                .GetUserGroupResourcePacks(resourcePackService.FindById(resourcePackId).UserGroupId);
            return PartialView(credit);
        }

        /// <summary>
        /// Добавляет кредит в базу данных
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <returns>Перенаправляет на страницу просмотра кредита</returns>
        [HttpPost]
        public ActionResult AddCredit(CreditRequestEntity credit)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(credit.ResourcePackId);
            if (ModelState.IsValid)
            {
                var id = creditService.Add(credit, Membership.GetUser().UserName);
                return RedirectToAction("ViewCredit", new { id });
            }

            credit.ResourcePacks = resourcePackService
                .GetUserGroupResourcePacks(resourcePackService.FindById(credit.ResourcePackId).UserGroupId);
            return PartialView(credit);
        }

        /// <summary>
        /// Страница изменения кредита
        /// </summary>
        /// <param name="id">Идентификатор кредита</param>
        /// <returns>Перенаправляет на страницу изменения кредита</returns>
        [HttpGet]
        public ActionResult EditCredit(long id)
        {
            var credit = Mapper.Map<CreditRequestEntity>(creditService.FindByIdWithRelatedData(id));
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(credit.ResourcePackId);
            credit.ResourcePacks = resourcePackService
                .GetUserGroupResourcePacks(credit.ResourcePack.UserGroupId);
            return PartialView(credit);
        }

        /// <summary>
        /// Изменяет кредит и обновляет его в базе данных
        /// </summary>
        /// <param name="credit">Сущность кредита</param>
        /// <returns>Перенаправляет на страницу просмотра кредита</returns>
        [HttpPost]
        public ActionResult EditCredit(CreditRequestEntity credit)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(credit.ResourcePackId);
            var dbCredit = creditService.FindByIdWithRelatedData(credit.Id);
            var resourcePack = resourcePackService.FindById(credit.ResourcePackId);
            if (resourcePack.UserGroupId != dbCredit.ResourcePack.UserGroupId)
            {
                ModelState.AddModelError(string.Empty, Errors.CantChangeUserGroup);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    creditService.Update(credit, Membership.GetUser().UserName);
                    return RedirectToAction("ViewCredit", new { id = credit.Id });
                }
            }

            credit.ResourcePacks = resourcePackService.GetUserGroupResourcePacks(dbCredit.ResourcePack.UserGroupId);
            return PartialView(credit);
        }

        /// <summary>
        /// Удаляет кредит из базы данных
        /// </summary>
        /// <param name="id">Идентификатор кредита</param>
        /// <returns>Перенаправляет на пустую страницу модального окна</returns>
        [HttpGet]
        public ActionResult RemoveCredit(long id)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(creditService.FindById(id).ResourcePackId);
            creditService.Remove(id, Membership.GetUser().UserName);
            return PartialView("_BlankModalForm");
        }

        /// <summary>
        /// Выставляет флаг 'IsPayed' кредиту и меняет дату окончания на текущую
        /// </summary>
        /// <param name="id">Идентификатор кредита</param>
        /// <returns>Перенаправляет на страницу просмотра кредита</returns>
        [HttpGet]
        public ActionResult CloseCredit(long id)
        {
            var credit = Mapper.Map<CreditRequestEntity>(creditService.FindByIdWithRelatedData(id));
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(credit.ResourcePackId);
            if (credit.StartDate >= DateTime.Today.AddDays(1))
            {
                ModelState.AddModelError(string.Empty, Errors.CloseCreditStartDateMoreThanNow);
                return PartialView("ViewCredit", credit);
            }
            else
            {
                creditService.CloseCredit(id, Membership.GetUser().UserName);
                return RedirectToAction("ViewCredit", new { id });
            }
        }
    }
}
