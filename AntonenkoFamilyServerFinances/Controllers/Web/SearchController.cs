﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StructureMap;
using DataLayer.Models;
using BusinessLayer.Interfaces;
using DataLayer;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер страниц полнотекстового поиска
    /// </summary>
    [Authorize]
    public class SearchController : Controller
    {
        /// <summary>
        /// Сервис полнотекстового поиска
        /// </summary>
        private ISearchService searchService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public SearchController()
        {
            searchService = ObjectFactory.GetInstance<ISearchService>();
        }

        /// <summary>
        /// Страница поиска
        /// </summary>
        /// <returns>Возвращает представление страницы поиска</returns>
        [HttpGet]
        public ActionResult Index()
        {
            var model = new Dictionary<Type, bool>();
            var searchableTypes = searchService.GetSearchableModelsTypes();
            foreach (var type in searchableTypes)
            {
                model.Add(type, Constants.DefaultSearchTypes.Contains(type));
            }

            return View(model);
        }

        /// <summary>
        /// Страница результатов поиска
        /// </summary>
        /// <param name="config">Конфигурация поиска</param>
        /// <returns>Возвращает представление страницы результатов поиска</returns>
        [HttpPost]
        public ActionResult Search(SearchConfiguration config)
        {
            var result = searchService.Search(config);
            return PartialView("Index_Search", result);
        }
    }
}
