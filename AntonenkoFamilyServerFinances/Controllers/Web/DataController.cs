﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BusinessLayer.Interfaces;
using BusinessLayer.Entities;
using StructureMap;
using DataLayer.Models;
using BusinessLayer.Infrastructure;
using AutoMapper;
using DataLayer;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Web.Security;
using AFinances.PresentationLayer.Infrastructure;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер для подгрузки данных на страницы с помощью JSON-объектов
    /// </summary>
    [Authorize]
    public class DataController : Controller
    {
        /// <summary>
        /// Сервис бюджета
        /// </summary>
        private IBudgetService<IncomeRequestEntity> budgetService;

        /// <summary>
        /// Сервис прибыли
        /// </summary>
        private ICashFlowService<Income, IncomeRequestEntity> incomeService;

        /// <summary>
        /// Сервис затрат
        /// </summary>
        private ICashFlowService<Spending, SpendingRequestEntity> spendingService;

        /// <summary>
        /// Сервис кредитов
        /// </summary>
        private ICreditService creditService;

        /// <summary>
        /// Сервис ресурсов
        /// </summary>
        private IResourcesService resourceService;

        /// <summary>
        /// Сервис ресурспаков
        /// </summary>
        private IResourcePackService resourcePackService;
        
        /// <summary>
        /// Сервис запланированных платежей
        /// </summary>
        private IMandatoryCashOffService mandatoryCashOffService;

        /// <summary>
        /// Сервис групп пользователей
        /// </summary>
        private IUserGroupService userGroupService;

        /// <summary>
        /// Сервис модулей
        /// </summary>
        private IModuleService moduleService;

        /// <summary>
        /// Сервис полнотекстового поиска
        /// </summary>
        private ISearchService searchService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public DataController()
        {
            this.budgetService = ObjectFactory.GetInstance<IBudgetService<IncomeRequestEntity>>();
            this.incomeService = ObjectFactory.GetInstance<ICashFlowService<Income, IncomeRequestEntity>>();
            this.spendingService = ObjectFactory.GetInstance<ICashFlowService<Spending, SpendingRequestEntity>>();
            this.creditService = ObjectFactory.GetInstance<ICreditService>();
            this.resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            this.mandatoryCashOffService = ObjectFactory.GetInstance<IMandatoryCashOffService>();
            this.resourceService = ObjectFactory.GetInstance<IResourcesService>();
            this.moduleService = ObjectFactory.GetInstance<IModuleService>();
            this.searchService = ObjectFactory.GetInstance<ISearchService>();
            this.userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
        }

        /// <summary>
        /// Получить список бюджетов до определенной даты определенного количества
        /// </summary>
        /// <param name="fromDate">Дата, до которой надо получить спиок бюджетов</param>
        /// <param name="itemsCount">Количество элементов возвращаемого списка</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает список бюджетов</returns>
        public JsonResult GetDailyBudgets(DateTime fromDate, int itemsCount, int resourcePackId)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            return Json(budgetService.GetDailyBudgets(fromDate, itemsCount, resourcePackId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получить список бюджетов до определенного месяца определенного количества (список сгруппирован по месяцам)
        /// </summary>
        /// <param name="fromDate">Дата, до которой надо получить спиок бюджетов</param>
        /// <param name="itemsCount">Количество элементов возвращаемого списка</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает список бюджетов</returns>
        public JsonResult GetMonthlyBudgets(DateTime fromDate, int itemsCount, int resourcePackId)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            return Json(budgetService.GetMonthlyBudgets(fromDate, itemsCount, resourcePackId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получить список прибыли на определенную дату
        /// </summary>
        /// <param name="date">Дата, на которую надо получить прибыль</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="resourceId">Идентификатор ресурса (опитимально)</param>
        /// <returns>Возвращает список прибыли на определенную дату</returns>
        public JsonResult GetResourceIncomesOnDate(DateTime date, int resourcePackId, int? resourceId = null)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            return Json(incomeService.GetDaily(date, 1, resourcePackId, resourceId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получить список затрат на определенную дату
        /// </summary>
        /// <param name="date">Дата, на которую надо получить затраты</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="resourceId">Идентификатор ресурса (опитимально)</param>
        /// <returns>Возвращает список затрат на определенную дату</returns>
        public JsonResult GetResourceSpendingsOnDate(DateTime date, int resourcePackId, int? resourceId = null)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            return Json(spendingService.GetDaily(date, 1, resourcePackId, resourceId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получить список кредитов на определенную дату
        /// </summary>
        /// <param name="date">Дата, на которую надо получить кредиты</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает список кредитов на определенную дату</returns>
        public JsonResult GetResourcePackCreditsOnDate(DateTime date, int resourcePackId)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            return Json(budgetService.GetActuatCreditsForResourcePackId(date, resourcePackId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получить бюджет ресурспака на определенную дату
        /// <param name="date">Дата, на которую надо получить бюджет</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// </summary>
        /// <returns>Возвращает бюджет ресурспака на определенную дату</returns>
        public JsonResult GetDateBudget(int resourcePackId, DateTime date)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var budget = budgetService.GetDailyBudgets(date, 1, resourcePackId).FirstOrDefault();
            var resourcePack = resourcePackService.FindById(resourcePackId);
            return Json(budget != null
                ? new BudgetResourceEntity()
                {
                    Date = date,
                    ResourcePackId = resourcePackId,
                    Balance = budget.Balance,
                    ResourcePackName = resourcePack.Name,
                    SummaryCredit = budget.SummaryCreditSum
                }
                : new BudgetResourceEntity()
            , JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получить список запланированных платежей на определенную дату
        /// </summary>
        /// <param name="date">Дата, на которую надо получить запл. платежи</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает список запланированных платежей на определенную дату</returns>
        public JsonResult GetMandatoryCashOffsOnDate(int resourcePackId, DateTime date)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            return Json(
                mandatoryCashOffService.GetDaily(date, 1, resourcePackId),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получает список ресурсов для данного ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает Json представление списка ресурсов</returns>
        public JsonResult GetResourcePackResources(long resourcePackId)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            return Json(resourceService.GetResourcePackResources(resourcePackId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получает список выбранных ресурсов для данного модуля конкретного ресурспака
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает Json представление списка ресурсов</returns>
        [HttpGet]
        public JsonResult GetModuleResourcePackResources(long moduleId, long resourcePackId)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var moduleResourcesIds = moduleService
                .GetModuleModuleResources(moduleId, Membership.GetUser().UserName)
                .Select(x => x.ResourceId);
            var resources = resourceService.GetResourcePackResources(resourcePackId).Where(x => moduleResourcesIds.Contains(x.Id));
            return Json(resources, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Достает страницу кредитов, учитывая идентификатор ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает JSON представление страницы значений кредитов ресурспака</returns>
        public JsonResult GetResourcePackCreditsPage(long resourcePackId, int page)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var credits = creditService.GetPageByResourcePackId(resourcePackId, page);
            return Json(Mapper.Map<IEnumerable<CreditRequestEntity>>(credits), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Достает страницу обязательных платежей, учитывая идентификатор ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Возвращает JSON представление страницы значений запланированных платежей ресурспака</returns>
        public JsonResult GetResourcePackMandatoryCashOffsPage(long resourcePackId, int page)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var mandatoryCashOffs = mandatoryCashOffService.GetPageByResourcePackId(resourcePackId, page);
            return Json(Mapper.Map<IEnumerable<MandatoryCashOffEntity>>(mandatoryCashOffs), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Генерирует данные для таймлайна прибыли/убытков
        /// </summary>
        /// <param name="toDate">Дата, до которой надо получить прибыль/убытки</param>
        /// <param name="dayCount">Количество дней, на которые надо получить данные</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает Json представление данных для таймлайна</returns>
        public JsonResult GetTimelineData(DateTime toDate, int dayCount, long resourcePackId)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var incomes = Mapper.Map<IEnumerable<IncomeRequestEntity>>(incomeService.GetDaily(toDate, dayCount, resourcePackId));
            var spendings = Mapper.Map<IEnumerable<SpendingRequestEntity>>(spendingService.GetDaily(toDate, dayCount, resourcePackId));
            var timeline = TimelineHelper.CreateTimeline(incomes, spendings);
            return Json(timeline, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получает страницу из коллекции всех ресурспаков
        /// </summary>
        /// <returns>Возвращает страницу из коллекции всех ресурспаков</returns>
        public JsonResult GetResourcePacksPage(int page)
        {
            return Json(
                Mapper.Map<IEnumerable<ResourcePackEntity>>(resourcePackService.GetPage(page, Membership.GetUser().UserName)),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Получает коллекцию ресурспаков, привязанных к определенной группе пользователей
        /// </summary>
        /// <param name="id">Идентификатор группы пользователей</param>
        /// <returns>Возвращает коллекцию ресурспаков, привязанных к определенной группе пользователей</returns>
        public JsonResult GetUserGroupResourcePacks(long id)
        {
            AuthorizeHelper.AuthorizeUserGroup(id);
            return Json(resourcePackService.GetUserGroupResourcePacks(id));
        }

        /// <summary>
        /// Ищет пользователей в системе по имени пользователя согласно запросу
        /// </summary>
        /// <param name="query">Запрос для поиска пользователей в системе по имени пользователя</param>
        /// <param name="selectedUsersJson">Коллекция уже выбранных пользователей в JSON формате</param>
        /// <returns>Возвращает найденных в системе пользователей</returns>
        public JsonResult SearchUsersFuzzy(string query, string selectedUsersJson)
        {
            var regex = new Regex(Constants.SearchUnAllowedSymbolsRegex);
            var excludeUsers = new JavaScriptSerializer()
                    .Deserialize<IEnumerable<string>>(selectedUsersJson != null ? selectedUsersJson : "[]")
                    .Select(x => regex.Replace(x, string.Empty))
                    .Where(x => x != Membership.GetUser().UserName);
            return SearchFuzzyForDropDown<User>(query, excludeUsers);
        }

        /// <summary>
        /// Ищет группы пользователей в системе по имени пользователя согласно запросу, учитывая условие, что текущий пользователь - владелец
        /// </summary>
        /// <param name="query">Запрос для поиска группы пользователей по имени</param>
        /// <param name="selectedUserGroupId">Идентификатор группы пользователей, которую следует исключить из поиска</param>
        /// <param name="checkOwnership">Флаг, показывающий будет ли проверяться владение группой пользователей или просто участие в ней</param>
        /// <returns>Возвращает найденных в системе группы пользователей</returns>
        public JsonResult SearchUserGroupsFuzzy(string query, long? selectedUserGroupId, bool checkOwnership)
        {  
            var conditions = new Dictionary<string, string>();

            //Искать только те, текущий пользователь которых - владелец или участник группы (по флагу)
            conditions.Add(
                checkOwnership
                    ? CommonHelper.nameof(() => new UserGroup().OwnerUsername)
                    : CommonHelper.nameof(() => new UserGroup().User_UserGroupsString),
                Membership.GetUser().UserName);

            if (selectedUserGroupId.HasValue && selectedUserGroupId != 0)
            {
                var selectedUserGroup = userGroupService.FindById(selectedUserGroupId.Value);
                return SearchFuzzyForDropDown<UserGroup>(query, new[] { selectedUserGroup.Name }, conditions);
            }

            return SearchFuzzyForDropDown<UserGroup>(query, Enumerable.Empty<string>(), conditions);
        }

        /// <summary>
        /// Ищет объекты в системе по запросу с возможсностью исключения элементов и дополнительных условий
        /// </summary>
        /// <typeparam name="T">Тип сущности для поиска</typeparam>
        /// <param name="query">Запрос для поиска</param>
        /// <param name="excludeResults">Элементы, которые следует исключить из поиска</param>
        /// <param name="fieldsConditions">Дополнительные условия для поиска элементов</param>
        /// <returns>Возвращает найденные в системе объекты</returns>
        private JsonResult SearchFuzzyForDropDown<T>(
            string query,
            IEnumerable<string> excludeResults,
            Dictionary<string, string> fieldsConditions = null)
            where T: BaseModel
        {
            var regex = new Regex(Constants.SearchUnAllowedSymbolsRegex);
            var config = new SearchConfiguration()
            {
                Page = 0,
                Query = query,
                Take = Constants.DropdownSearchTakeValue,
                TypeNamesJson = JsonConvert.SerializeObject(new[] { typeof(T).FullName }),
                ExcludeResults = excludeResults,
                FieldsConditions = fieldsConditions
            };

            return string.IsNullOrEmpty(regex.Replace(query, string.Empty))
                ? Json(Enumerable.Empty<T>())
                : Json(this.searchService.SearchFuzzyOnly(config).Results.First(x => x.Key == typeof(T)).Value);
        }
    }
}
