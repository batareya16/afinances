﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BusinessLayer.Interfaces;
using StructureMap;
using AutoMapper;
using BusinessLayer.Entities;
using System.Web.Security;
using AFinances.PresentationLayer.Infrastructure;
using BusinessLayer.Resources;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер страниц управления запланированными обязательными платежами
    /// </summary>
    [Authorize]
    public class MandatoryCashOffController : Controller
    {
        /// <summary>
        /// Сервис обязательных платежей
        /// </summary>
        private IMandatoryCashOffService mandatoryCashOffService;

        /// <summary>
        /// Сервис ресурспаков
        /// </summary>
        private IResourcePackService resourcePackService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public MandatoryCashOffController()
        {
            mandatoryCashOffService = ObjectFactory.GetInstance<IMandatoryCashOffService>();
            resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
        }

        /// <summary>
        /// Страница просмотра обязательного платежа
        /// </summary>
        /// <param name="id">Идентификатор обязательного платежа</param>
        /// <returns>Перенаправляет на страницу просмотра обязательного платежа</returns>
        [HttpGet]
        public ActionResult ViewMandatoryCashOff(long id)
        {
            var mandatoryCashOff = Mapper.Map<MandatoryCashOffEntity>(mandatoryCashOffService.FindByIdWithRelatedData(id));
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(mandatoryCashOff.ResourcePackId);
            return PartialView(mandatoryCashOff);
        }

        /// <summary>
        /// Страница добавления обязательного платежа
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <param name="date">Дата обязательного платежа (оптимально)</param>
        /// <returns>Перенаправляет на страницу добавления обязательного платежа</returns>
        [HttpGet]
        public ActionResult AddMandatoryCashOff(long resourcePackId, DateTime? date = null)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var mandatoryCashOff = new MandatoryCashOffEntity() { ResourcePackId = resourcePackId };
            mandatoryCashOff.DateAndTime = date ?? DateTime.Today;
            mandatoryCashOff.ResourcePacks = resourcePackService
                .GetUserGroupResourcePacks(resourcePackService.FindById(resourcePackId).UserGroupId);
            return PartialView(mandatoryCashOff);
        }

        /// <summary>
        /// Добавляет обязательный платеж в базу данных
        /// </summary>
        /// <param name="credit">Сущность обязательного платежа</param>
        /// <returns>Перенаправляет на страницу просмотра обязательного платежа</returns>
        [HttpPost]
        public ActionResult AddMandatoryCashOff(MandatoryCashOffEntity mandatoryCashOff)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(mandatoryCashOff.ResourcePackId);
            if (ModelState.IsValid)
            {
                var id = mandatoryCashOffService.Add(mandatoryCashOff, Membership.GetUser().UserName);
                return RedirectToAction("ViewMandatoryCashOff", new { id });
            }

            mandatoryCashOff.ResourcePacks = resourcePackService
                .GetUserGroupResourcePacks(resourcePackService.FindById(mandatoryCashOff.ResourcePackId).UserGroupId);
            return PartialView(mandatoryCashOff);
        }

        /// <summary>
        /// Страница изменения обязательного платежа
        /// </summary>
        /// <param name="id">Идентификатор обязательного платежа</param>
        /// <returns>Перенаправляет на страницу изменения обязательного платежа</returns>
        [HttpGet]
        public ActionResult EditMandatoryCashOff(long id)
        {
            var mandatoryCashOff = Mapper.Map<MandatoryCashOffEntity>(mandatoryCashOffService.FindByIdWithRelatedData(id));
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(mandatoryCashOff.ResourcePackId);
            mandatoryCashOff.ResourcePacks = resourcePackService
                .GetUserGroupResourcePacks(mandatoryCashOff.ResourcePack.UserGroupId);
            return PartialView(mandatoryCashOff);
        }

        /// <summary>
        /// Изменяет обязательный платеж и обновляет его в базе данных
        /// </summary>
        /// <param name="credit">Сущность обязательного платежа</param>
        /// <returns>Перенаправляет на страницу просмотра обязательного платежа</returns>
        [HttpPost]
        public ActionResult EditMandatoryCashOff(MandatoryCashOffEntity mandatoryCashOff)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(mandatoryCashOff.ResourcePackId);
            var dbMandatoryCashOff = mandatoryCashOffService.FindByIdWithRelatedData(mandatoryCashOff.Id);
            var resourcePack = resourcePackService.FindById(mandatoryCashOff.ResourcePackId);

            if (resourcePack.UserGroupId != dbMandatoryCashOff.ResourcePack.UserGroupId)
            {
                ModelState.AddModelError(string.Empty, Errors.CantChangeUserGroup);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    mandatoryCashOffService.Update(mandatoryCashOff, Membership.GetUser().UserName);
                    return RedirectToAction("ViewMandatoryCashOff", new { id = mandatoryCashOff.Id });
                }
            }

            mandatoryCashOff.ResourcePacks = resourcePackService
                .GetUserGroupResourcePacks(dbMandatoryCashOff.ResourcePack.UserGroupId);
            return PartialView(mandatoryCashOff);
        }

        /// <summary>
        /// Удаляет обязательный платеж из базы данных
        /// </summary>
        /// <param name="id">Идентификатор обязательного платежа</param>
        /// <returns>Перенаправляет на пустую страницу модального окна</returns>
        [HttpGet]
        public ActionResult RemoveMandatoryCashOff(long id)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(mandatoryCashOffService.FindById(id).ResourcePackId);
            mandatoryCashOffService.Remove(id, Membership.GetUser().UserName);
            return PartialView("_BlankModalForm");
        }
    }
}
