﻿using System.Web.Mvc;
using System.Linq;
using BusinessLayer.Entities.User;
using BusinessLayer.Interfaces;
using StructureMap;
using System.Web.Security;
using BusinessLayer.Infrastructure;
using BusinessLayer.Entities;
using BusinessLayer.Resources;
using AFinances.PresentationLayer.Infrastructure;

namespace DataLayer.Controllers.Web
{
    /// <summary>
    /// Контроллер страниц управления группами пользователей
    /// </summary>
    [Authorize]
    public class UserGroupController : Controller
    {
        /// <summary>
        /// Сервис группы пользователей
        /// </summary>
        private IUserGroupService userGroupService;

        /// <summary>
        /// Стандартный конструктор
        /// </summary>
        public UserGroupController()
        {
            userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
        }

        /// <summary>
        /// Страница добавления группы пользователей
        /// </summary>
        /// <returns>Перенаправляет на страницу добавления группы пользователей</returns>
        [HttpGet]
        public ActionResult AddUserGroup()
        {
            var userGroup = new UserGroupEntity();
            return PartialView(userGroup);
        }

        /// <summary>
        /// Создает группу пользователей
        /// </summary>
        /// <param name="userGroup">Сущность группы пользователей</param>
        /// <returns>Перенаправляет на страницу просмотра группы пользователей</returns>
        [HttpPost]
        public ActionResult AddUserGroup(UserGroupEntity userGroup)
        {
            if (ModelState.IsValid)
            {
                if (userGroup.Users.All(x => ValidationHelper.IsValid<BaseUserEntity>(x)))
                {
                    var id = userGroupService.Add(userGroup, Membership.GetUser().UserName);
                    return RedirectToAction("ViewUserGroup", new { id });
                }
                else
                {
                    ModelState.AddModelError(
                        CommonHelper.nameof(() => userGroup.Users),
                        string.Format(Errors.IncorrectFormat, Resources.Models.Account.UserName));
                }
            }

            return PartialView(userGroup);
        }

        /// <summary>
        /// Страница просмотра группы пользователей
        /// </summary>
        /// <param name="id">Идентификатор группы пользователей</param>
        /// <returns>Возвращает страницу просмотра группы пользователей</returns>
        [HttpGet]
        public ActionResult ViewUserGroup(long id)
        {
            AuthorizeHelper.AuthorizeUserGroup(id);
            var userGroup = userGroupService.FindByIdWithRelatedUsers(id);
            return PartialView(userGroup);
        }

        [HttpGet]
        public ActionResult EditUserGroup(long id)
        {
            AuthorizeHelper.CheckUserGroupOwner(id);
            var userGroup = userGroupService.FindByIdWithRelatedUsers(id);
            return PartialView(userGroup);
        }

        /// <summary>
        /// Удаляет группу пользователей из базы данных
        /// </summary>
        /// <param name="id">Идентификатор группы пользователя</param>
        /// <returns>Перенаправляет на пустую страницу модального окна</returns>
        [HttpGet]
        public ActionResult RemoveUserGroup(long id)
        {
            AuthorizeHelper.CheckUserGroupOwner(id);
            userGroupService.Remove(id, Membership.GetUser().UserName);
            return PartialView("_BlankModalForm");
        }

        /// <summary>
        /// Изменяет группу пользователей
        /// </summary>
        /// <param name="userGroup">Сущность группы пользователей</param>
        /// <returns>Перенаправляет на страницу просмотра группы пользователей</returns>
        [HttpPost]
        public ActionResult EditUserGroup(UserGroupEntity userGroup)
        {
            if (ModelState.IsValid)
            {
                AuthorizeHelper.CheckUserGroupOwner(userGroup.Id);
                if (userGroup.Users.All(x => ValidationHelper.IsValid<BaseUserEntity>(x)))
                {
                    userGroupService.Update(userGroup, Membership.GetUser().UserName);
                    return RedirectToAction("ViewUserGroup", new { id = userGroup.Id });
                }
                else
                {
                    ModelState.AddModelError(
                        CommonHelper.nameof(() => userGroup.Users),
                        string.Format(Errors.IncorrectFormat, Resources.Models.Account.UserName));
                }
            }

            return PartialView(userGroup);
        }

        [HttpPost]
        public PartialViewResult GetUserGroups(int userGroupsPage)
        {
            return PartialView(userGroupService.GetPageForUser(userGroupsPage, Membership.GetUser().UserName));
        }
    }
}
