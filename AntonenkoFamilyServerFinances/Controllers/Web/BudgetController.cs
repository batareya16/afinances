﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataLayer.Models;
using BusinessLayer.Interfaces;
using StructureMap;
using BusinessLayer.Entities;
using AutoMapper;
using System.Web.Security;
using AFinances.PresentationLayer.Infrastructure;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер страниц управления бюджетом
    /// </summary>
    [Authorize]
    public class BudgetController : Controller
    {
        /// <summary>
        /// Сервис управления ресурспаками
        /// </summary>
        private IResourcePackService resourcePackService;

        /// <summary>
        /// Сервис управления бюджетом
        /// </summary>
        private IBudgetService<IncomeRequestEntity> budgetService;

        /// <summary>
        /// Сервис управления ресурсами
        /// </summary>
        private IResourcesService resourcesService;

        /// <summary>
        /// Сервис управления группами пользователей
        /// </summary>
        private IUserGroupService userGroupService;

        /// <summary>
        /// Сервис управления прибылью
        /// </summary>
        private ICashFlowService<Income, IncomeRequestEntity> incomeService;

        /// <summary>
        /// Сервис управления затратами
        /// </summary>
        private ICashFlowService<Spending, SpendingRequestEntity> spendingService;

        /// <summary>
        /// Сервис типов ресурса
        /// </summary>
        private IBaseEntityService<ResourceType, ResourceTypeEntity> resourceTypeService;

        /// <summary>
        /// Базовый контроллер
        /// </summary>
        public BudgetController()
        {
            budgetService = ObjectFactory.GetInstance<IBudgetService<IncomeRequestEntity>>();
            resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            resourcesService = ObjectFactory.GetInstance<IResourcesService>();
            userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
            incomeService = ObjectFactory.GetInstance<ICashFlowService<Income, IncomeRequestEntity>>();
            spendingService = ObjectFactory.GetInstance<ICashFlowService<Spending, SpendingRequestEntity>>();
            resourceTypeService = ObjectFactory.GetInstance<IBaseEntityService<ResourceType, ResourceTypeEntity>>();
        }

        /// <summary>
        /// Страница просмотра бюджета
        /// </summary>
        /// <returns>Возвращает представление страницы просмотра бюджета</returns>
        [HttpGet]
        public ActionResult Index(){
            return View(Mapper.Map<IEnumerable<ResourcePackEntity>>(
                resourcePackService.GetForCurrentUserGroup(Membership.GetUser().UserName)));
        }

        /// <summary>
        /// Страница текущего бюджета ресурспаков
        /// </summary>
        /// <returns>Возвращает представление страницы текущего бюджета ресурспаков</returns>
        [HttpPost]
        public ActionResult GetCurrentBudget()
        {
            var currentBudget = budgetService.GetBudgetResourcePacks();
            return PartialView("Index_CurrentBudget", currentBudget);
        }

        /// <summary>
        /// Страница просмотра бюджета ресурспака на определенную дату
        /// </summary>
        /// <param name="date">Дата просмотра бюджета ресурспака</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает представление страницы просмотра бюджета ресурспака на определенную дату</returns>
        [HttpGet]
        public ActionResult ViewDateBudget(long resourcePackId, DateTime date)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var budget = budgetService.GetDailyBudgets(date, 1, resourcePackId).FirstOrDefault();
            var resourcePack = resourcePackService.FindById(resourcePackId);
            return View(new BudgetResourceEntity() {
                Date = date,
                ResourcePackId = resourcePackId,
                Balance = budget.Balance,
                ResourcePackName = resourcePack.Name,
                SummaryCredit = budget.SummaryCreditSum
            });
        }

        /// <summary>
        /// Страница добавления прибыли на определенную дату
        /// </summary>
        /// <param name="date">Дата добавления (оптимально)</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает представление страницы добавления прибыли на определенную дату</returns>
        [HttpGet]
        public ActionResult AddIncome(long resourcePackId, DateTime? date = null)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var resources = resourcesService.GetResourcePackResources(resourcePackId);
            var income = new IncomeRequestEntity() { Resources = resources };
            income.DateAndTime = date ?? DateTime.Today;
            return PartialView(income);
        }

        /// <summary>
        /// Добавляет прибыль в базу данных
        /// </summary>
        /// <param name="income">Сущность прибыли</param>
        /// <returns>Перенаправляет на страницу просмотра прибыли</returns>
        [HttpPost]
        public ActionResult AddIncome(IncomeRequestEntity income)
        {
            if (ModelState.IsValid)
            {
                AuthorizeHelper.AuthorizeUserGroupByResource(income.ResourceId);
                var id = incomeService.Add(income, Membership.GetUser().UserName);
                return RedirectToAction( "ViewIncome", new { id });
            }

            income.Resources = resourcesService.GetResourcePackResources(income.ResourcePackId);
            return PartialView(income);
        }

        /// <summary>
        /// Страница изменения определенной прибыли
        /// </summary>
        /// <param name="id">Идентификатор прибыли</param>
        /// <returns>Возвращает представление страницы изменения определенной прибыли</returns>
        [HttpGet]
        public ActionResult EditIncome(long id)
        {
            var income = Mapper.Map<IncomeRequestEntity>(incomeService.FindById(id));
            AuthorizeHelper.AuthorizeUserGroupByResource(income.ResourceId);
            var resource = resourcesService.FindById(income.ResourceId);
            income.Resources = resourcesService.GetResourcePackResources(resource.ResourcePackId);
            income.ResourcePackId = resource.ResourcePackId;
            return PartialView(Mapper.Map<IncomeRequestEntity>(income));
        }

        /// <summary>
        /// Изменяет определенный объект прибыли
        /// </summary>
        /// <param name="income">Сущность прибыли</param>
        /// <returns>Перенаправляет на страницу просмотра прибыли</returns>
        [HttpPost]
        public ActionResult EditIncome(IncomeRequestEntity income)
        {
            if (ModelState.IsValid)
            {
                AuthorizeHelper.AuthorizeUserGroupByResource(income.ResourceId);
                incomeService.Update(income, Membership.GetUser().UserName);
                return RedirectToAction(
                    "ViewIncome",
                    new { id = income.Id });
            }

            income.Resources = resourcesService.GetResourcePackResources(income.ResourcePackId);
            return PartialView(income);
        }

        /// <summary>
        /// Страница изменения определенного убытка
        /// </summary>
        /// <param name="id">Идентификатор расхода</param>
        /// <returns>Возвращает представление страницы изменения определенного убытка</returns>
        [HttpGet]
        public ActionResult EditSpending(long id)
        {
            var spending = Mapper.Map<SpendingRequestEntity>(spendingService.FindById(id));
            AuthorizeHelper.AuthorizeUserGroupByResource(spending.ResourceId);
            var resource = resourcesService.FindById(spending.ResourceId);
            spending.Resources = resourcesService.GetResourcePackResources(resource.ResourcePackId);
            spending.ResourcePackId = resource.ResourcePackId;
            return PartialView(Mapper.Map<SpendingRequestEntity>(spending));
        }

        /// <summary>
        /// Изменяет определенный объект убытка
        /// </summary>
        /// <param name="spending">Сущность расхода</param>
        /// <returns>Перенаправляет на страницу просмотра убытка</returns>
        [HttpPost]
        public ActionResult EditSpending(SpendingRequestEntity spending)
        {
            if (ModelState.IsValid)
            {
                AuthorizeHelper.AuthorizeUserGroupByResource(spending.ResourceId);
                spendingService.Update(spending, Membership.GetUser().UserName);
                return RedirectToAction(
                    "ViewSpending",
                    new { id = spending.Id });
            }

            spending.Resources = resourcesService.GetResourcePackResources(spending.ResourcePackId);
            return PartialView(spending);
        }

        /// <summary>
        /// Страница добавления расхода на определенную дату 
        /// </summary>
        /// <param name="date">Дата добавления (оптимально)</param>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Перенаправляет на страницу просмотра бюджета на определенную дату</returns>
        [HttpGet]
        public ActionResult AddSpending(long resourcePackId, DateTime? date = null)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePackId);
            var resources = resourcesService.GetResourcePackResources(resourcePackId);
            var spending = new SpendingRequestEntity() { Resources = resources };
            spending.DateAndTime = date ?? DateTime.Today;
            return PartialView(spending);
        }

        /// <summary>
        /// Добавляет расход в базу данных
        /// </summary>
        /// <param name="spending">Сущность расхода</param>
        /// <returns>Перенаправляет на страницу просмотра убытка</returns>
        [HttpPost]
        public ActionResult AddSpending(SpendingRequestEntity spending)
        {
            if (ModelState.IsValid)
            {
                AuthorizeHelper.AuthorizeUserGroupByResource(spending.ResourceId);
                var id = spendingService.Add(spending, Membership.GetUser().UserName);
                return RedirectToAction("ViewSpending", new { id });
            }

            spending.Resources = resourcesService.GetResourcePackResources(spending.ResourcePackId);
            return PartialView(spending);
        }

        /// <summary>
        /// Страница просмотра определенной прибыли
        /// </summary>
        /// <param name="id">Идентификатор прибыли</param>
        /// <returns>Возвращает представление страницы определенной прибыли</returns>
        [HttpGet]
        public ActionResult ViewIncome(long id)
        {
            var income = Mapper.Map<IncomeRequestEntity>(incomeService.FindByIdWithRelatedData(id));
            AuthorizeHelper.AuthorizeUserGroupByResource(income.ResourceId);
            return PartialView(income);
        }

        /// <summary>
        /// Страница просмотра определенного расхода
        /// </summary>
        /// <param name="id">Идентификатор расхода</param>
        /// <returns>Возвращает представление страницы определенного расхода</returns>
        [HttpGet]
        public ActionResult ViewSpending(long id)
        {
            var spending = Mapper.Map<SpendingRequestEntity>(spendingService.FindByIdWithRelatedData(id));
            AuthorizeHelper.AuthorizeUserGroupByResource(spending.ResourceId);
            return PartialView(spending);
        }

        /// <summary>
        /// Удаляет прибыль из базы данных
        /// </summary>
        /// <param name="id">Идентификатор прибыли</param>
        /// <returns>Перенаправляет на пустую страницу модального окна</returns>
        [HttpGet]
        public ActionResult RemoveIncome(long id)
        {
            var income = incomeService.FindById(id);
            AuthorizeHelper.AuthorizeUserGroupByResource(income.ResourceId);
            incomeService.Remove(id, Membership.GetUser().UserName);
            return PartialView("_BlankModalForm");
        }

        /// <summary>
        /// Удаляет расход из базы данных
        /// </summary>
        /// <param name="id">Идентификатор расхода</param>
        /// <returns>Перенаправляет на страницу просмотра бюджета на определенную дату</returns>
        [HttpGet]
        public ActionResult RemoveSpending(long id)
        {
            var spending = spendingService.FindById(id);
            AuthorizeHelper.AuthorizeUserGroupByResource(spending.ResourceId);
            var resourcePackId = resourcePackService.GetByResource(spending.ResourceId).Id;
            spendingService.Remove(id, Membership.GetUser().UserName);
            return PartialView("_BlankModalForm");
        }

        /// <summary>
        /// Генерирует фейковые данные для теста на время разработки
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GenerateFakeTestData()
        {
            var ugId = userGroupService.Add(
                new BusinessLayer.Entities.User.UserGroupEntity()
                {
                    Name = "My family",
                    OwnerUsername = Membership.GetUser().UserName
                },
                Membership.GetUser().UserName);
            var typeId = resourceTypeService.Add(
                new ResourceTypeEntity()
                {
                    Name = "Nalichnye",
                    UserGroupId = ugId
                },
                Membership.GetUser().UserName);
            var rtId = resourcePackService.Add(
                new ResourcePackEntity()
                {
                    Name = "A",
                    Limit = 10,
                    UserGroupId = ugId
                },
                Membership.GetUser().UserName);
            var rId = resourcesService.Add(
                new ResourceEntity()
                {
                    Name = "Nalichnie",
                    ResourcePackId = rtId,
                    ResourceTypeId = typeId
                },
                Membership.GetUser().UserName);
            incomeService.Add(
                new IncomeRequestEntity()
                {
                    DateAndTime = DateTime.Today,
                    Description = "test",
                    ResourceId = rId,
                    Summ = 100
                },
                Membership.GetUser().UserName);
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Страница просмотра прибыли и убытков с помощью таймлайна
        /// </summary>
        /// <returns>Возвращает представление страницы просмотра прибыли и убытков</returns>
        [HttpGet]
        public ActionResult ViewMoneyTurnover()
        {
            var resourcePacks = Mapper.Map<IEnumerable<ResourcePackEntity>>(
                resourcePackService.GetForCurrentUserGroup(Membership.GetUser().UserName));
            return View(resourcePacks);
        }
    }
}
