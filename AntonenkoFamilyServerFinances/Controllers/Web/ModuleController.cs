﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using DataLayer.Models;
using BusinessLayer.Interfaces;
using StructureMap;
using BusinessLayer.Entities.Module;
using BusinessLayer.Entities;
using BusinessLayer.Infrastructure;
using AutoMapper;
using AddModuleFromFileResource = AFinances.PresentationLayer.Resources.Views.Module.AddModuleFromFile;
using LinkModuleResourcesResource = AFinances.PresentationLayer.Resources.Views.Module.LinkModuleResources;
using ModuleConfigurationResource = AFinances.PresentationLayer.Resources.Views.Module.ModuleConfiguration;
using System.Web.Security;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер управления модулями парсинга данных
    /// </summary>
    [Authorize]
    public class ModuleController : Controller
    {
        /// <summary>
        /// Сервис подключаемых модулей парсинга данных
        /// </summary>
        private IModuleService moduleService;

        /// <summary>
        /// Сервис ресурспаков
        /// </summary>
        private IResourcePackService resourcePackService;

        /// <summary>
        /// Сервис ресурсов
        /// </summary>
        private IResourcesService resourcesService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public ModuleController()
        {
            moduleService = ObjectFactory.GetInstance<IModuleService>();
            resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            resourcesService = ObjectFactory.GetInstance<IResourcesService>();
        }

        /// <summary>
        /// Главная страница просмотра списка модулей
        /// </summary>
        /// <returns>Возвращает представление главной страницы просмотра списка модулей</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Страница списка модулей
        /// </summary>
        /// <param name="page">Номер страницы списка модулей</param>
        /// <returns>Возвращает частичное представление страницы списка модулей</returns>
        public PartialViewResult GetModules(byte page)
        {
            var modules = moduleService.GetModules(page);
            return PartialView("Index_ModulesList", modules);
        }

        /// <summary>
        /// Страница привязки модулей к ресурсам
        /// </summary>
        /// <param name="id">Идентификатор модуля</param>
        /// <returns>Возвращает частичное представление страницы привязки модулей к ресурсам</returns>
        [HttpGet]
        public PartialViewResult LinkModuleResources(long id)
        {
            var module = moduleService.GetModule(id);
            var linkedModuleResources = new LinkModuleResourcesEntity()
            {
                ModuleId = module.Id,
                DisplayModuleName = module.DisplayName,
                ResourcePacks = Mapper.Map<IEnumerable<ResourcePackEntity>>(
                    resourcePackService.GetForCurrentUserGroup(Membership.GetUser().UserName)),
                SelectedResources = Mapper.Map<IEnumerable<ResourceEntity>>(
                    resourcesService.GetByIds(moduleService
                        .GetModuleModuleResources(id, Membership.GetUser().UserName)
                        .Select(x => x.ResourceId)))
            };
            return PartialView(linkedModuleResources);
        }

        /// <summary>
        /// Привязывает модуль к списку ресурсов
        /// </summary>
        /// <param name="linkedModuleResources">Сущность привязки определенного модуля к списку ресурсов</param>
        /// <returns>Возвращает частичное представление страницы об успешной привязки модуля к ресурсам</returns>
        [HttpPost]
        public PartialViewResult LinkModuleResources(LinkModuleResourcesEntity linkedModuleResources)
        {
            moduleService.LinkModuleToResources(
                linkedModuleResources.SelectedResourcesIds,
                Membership.GetUser().UserName,
                linkedModuleResources.ModuleId);
            return PartialView("MessageModalForm", new KeyValuePair<string, string>(
                LinkModuleResourcesResource.SelectedResourcesSuccsessfullyUpdated,
                string.Format(LinkModuleResourcesResource.SelectedResourcesUpdatedMsg, linkedModuleResources.DisplayModuleName)));
        }

        /// <summary>
        /// Страница конфигурации модуля, привязанного к ресурсам
        /// </summary>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <returns>Возвращает частичное представление страницы конфигурации модуля, привязанного к ресурсам</returns>
        [HttpGet]
        public PartialViewResult ModuleConfiguration(long moduleId)
        {
            var moduleCfg = new ModuleConfiguration()
            {
                ModuleId = moduleId,
                ResourcePacks = moduleService.GetModuleLinkedResourcePacks(moduleId)
            };
            return PartialView("Index_ModuleConfiguration", moduleCfg);
        }

        /// <summary>
        /// Часть страницы конфигурации модуля, которая отвечает за конкретный модуль, привязанный к конкретному ресурсу
        /// </summary>
        /// <param name="resourceId">Идентификатор ресурса</param>
        /// <param name="moduleId">Идентификатор модуля</param>
        /// <returns>Возвращает частичное представление страницы конфигурации модуля,
        /// которая отвечает за конкретный модуль, привязанный к конкретному ресурсу</returns>
        public PartialViewResult ModuleConfigurationInner(long resourceId, long moduleId)
        {
            var moduleCfg = Mapper.Map<ModuleConfiguration>(moduleService.GetModuleResource(moduleId, resourceId));
            var module = moduleService.GetModule(moduleId);
            var moduleResourceId = moduleCfg.Id;
            Mapper.Map(module, moduleCfg, typeof(Module), typeof(ModuleConfiguration));
            moduleCfg.Id = moduleResourceId;
            return PartialView("Index_ModuleConfiguration_Config", moduleCfg);
        }

        /// <summary>
        /// Получает остаток миллисекунд до следующего запуска работы модуля
        /// </summary>
        /// <param name="moduleResourceId">Идентификатор модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает остаток миллисекунд до следующего запуска работы модуля</returns>
        [HttpGet]
        public long GetModuleResourceTimerJobRemain(long moduleResourceId)
        {
            return moduleService.GetModuleResourceTimerJobRemain(moduleResourceId);
        }

        /// <summary>
        /// Обновляет конфигурацию модуля, привязанного к ресурсу
        /// </summary>
        /// <param name="config">Сущность конфигурации модуля, привязанного к ресурсу</param>
        /// <returns>Возвращает частичное представление страницы конфигурации модуля,
        /// которая отвечает за конкретный модуль, привязанный к конкретному ресурсу</returns>
        [HttpPost]
        public PartialViewResult UpdateModuleConfigurationInner(ModuleConfiguration config)
        {
            var modelErrorMsg = ModuleConfigurationResource.UpdateModuleConfigurationError;
            var result = config;
            if (ModelState.IsValid && config.IsModuleConfigTimeValid())
            {
                result = moduleService.UpdateResourceModule(config);
                modelErrorMsg = ModuleConfigurationResource.UpdateModuleConfigurationSuccess;
            }
            else result.AdditionalConfiguration =
                Serializer.GetCustomConfigurationNodesJson(result.AdditionalConfiguration);

            result.AdditionalConfiguration =
                ModuleAdditConfigHelper.ProcessConfigToDisplay(result.AdditionalConfiguration);
            ModelState.AddModelError(string.Empty, modelErrorMsg);
            return PartialView("Index_ModuleConfiguration_Config", result);
        }

        /// <summary>
        /// Запускает работу модуля, привязанного к ресурсу
        /// </summary>
        /// <param name="config">Сущность конфигурации модуля</param>
        /// <returns>Возвращает частичное представление страницы конфигурации модуля,
        /// которая отвечает за конкретный модуль, привязанный к конкретному ресурсу</returns>
        [HttpPost]
        public PartialViewResult ImmediatelyRunJob(ModuleConfiguration config)
        {
            var modelErrorMsg = ModuleConfigurationResource.RunJobError;
            if (moduleService.ImmediatelyRunJob(config.Id))
                modelErrorMsg = ModuleConfigurationResource.RunJobSuccess;
            config.AdditionalConfiguration = ModuleAdditConfigHelper.ProcessConfigToDisplay(
                Serializer.GetCustomConfigurationNodesJson(config.AdditionalConfiguration));
            ModelState.AddModelError(string.Empty, modelErrorMsg);
            return PartialView("Index_ModuleConfiguration_Config", config);
        }

        /// <summary>
        /// Удаляет модуль
        /// </summary>
        /// <param name="id">Идентификатор модуля</param>
        /// <param name="page">Номер текущей страницы на странице списка модулей</param>
        /// <returns>Перенаправляет на страницу списка модулей</returns>
        [HttpPost]
        public ActionResult RemoveModule(long id, byte page)
        {
            moduleService.RemoveModule(id);
            return RedirectToAction("GetModules", new { page });
        }

        /// <summary>
        /// Страница добавления модуля из файла библиотеки
        /// </summary>
        /// <returns>Возвращает представление страницы добавления модуля из файла</returns>
        [HttpGet]
        public ActionResult AddModuleFromFile()
        {
            return View();
        }

        /// <summary>
        /// Добавляет модуль парсинга данных из файла библиотеки
        /// </summary>
        /// <param name="moduleFile">Отправленный файл через POST-запрос</param>
        /// <returns>Перенаправляет на страницу сообщения об успешном добавлении модуля</returns>
        [HttpPost]
        public ActionResult AddModuleFromFile(HttpPostedFileBase moduleFile)
        {
            var module = moduleService.AddModuleFromFile(moduleFile);
            return module != null
                ? PartialView("MessageModalForm", new KeyValuePair<string, string>(
                    AddModuleFromFileResource.ModuleSuccsessfullyAdded,
                    string.Format(AddModuleFromFileResource.ModuleAddedMsg, module.DisplayName)))
                : PartialView("MessageModalForm", new KeyValuePair<string, string>(
                    AddModuleFromFileResource.ModuleAddFailed,
                    AddModuleFromFileResource.ModuleAddFailedMsg));
        }
    }
}
