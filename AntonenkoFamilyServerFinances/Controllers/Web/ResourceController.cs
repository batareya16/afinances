﻿using System.Collections.Generic;
using System.Web.Mvc;
using BusinessLayer.Interfaces;
using BusinessLayer.Infrastructure;
using StructureMap;
using AutoMapper;
using BusinessLayer.Entities;
using DataLayer.Models;
using System.Web.Security;
using AFinances.PresentationLayer.Resources.Views;
using CommonResource = AFinances.PresentationLayer.Resources.Views.Common;
using DataLayer;
using AFinances.PresentationLayer.Infrastructure;
using BusinessLayer.Entities.User;

namespace AntonenkoFamilyServerFinances.Controllers.Web
{
    /// <summary>
    /// Контроллер страниц управления ресурсами
    /// </summary>
    [Authorize]
    public class ResourceController : Controller
    {
        /// <summary>
        /// Сервис управления ресурспаками
        /// </summary>
        private IResourcePackService resourcePackService;

        /// <summary>
        /// Сервис управления группами пользователей
        /// </summary>
        private IUserGroupService userGroupService;

        /// <summary>
        /// Сервис управления ресурсами
        /// </summary>
        private IResourcesService resourcesService;

        /// <summary>
        /// Сервис управления типами ресурсов
        /// </summary>
        private IResourceTypeService resourceTypeService;

        /// <summary>
        /// Базовый конструктор
        /// </summary>
        public ResourceController()
        {
            resourcePackService = ObjectFactory.GetInstance<IResourcePackService>();
            resourcesService = ObjectFactory.GetInstance<IResourcesService>();
            userGroupService = ObjectFactory.GetInstance<IUserGroupService>();
            resourceTypeService = ObjectFactory.GetInstance<IResourceTypeService>();
        }

        /// <summary>
        /// Страница для просмотра деталей ресурспака
        /// </summary>
        /// <param name="id">Идентификатор ресурспака</param>
        /// <returns>Возвращает представление страницы просмотра деталей ресурспака</returns>
        [HttpGet]
        public ActionResult ViewResourcePack(long id)
        {
            return View(id);
        }

        /// <summary>
        /// Страница информации о ресурспаке
        /// </summary>
        /// <param name="resourcePack">Сущность ресурспака</param>
        /// <returns>Возвращает представление страницы информации о ресурспаке</returns>
        [HttpPost]
        public ActionResult GetResourcePackInfo(ResourcePackEntity resourcePack)
        {
            AuthorizeHelper.AuthorizeUserGroupByResourcePack(resourcePack.Id);
            var resourcePackEntity = Mapper.Map<ResourcePackEntity>(resourcePackService.FindByIdWithRelatedData(resourcePack.Id));
            return PartialView("ViewResourcePack_ResourcePack", resourcePackEntity);
        }
        
        /// <summary>
        /// Страница просмотра деталей ресурса
        /// </summary>
        /// <param name="id">Идентификатор ресурса</param>
        /// <returns>Возвращает представление страницы просмотра деталей ресурса</returns>
        [HttpGet]
        public ActionResult ViewResource(long id)
        {
            AuthorizeHelper.AuthorizeUserGroupByResource(id);
            var resource = Mapper.Map<ResourceEntity>(resourcesService.FindByIdWithRelatedData(id));
            return PartialView(resource);
        }

        /// <summary>
        /// Страница изменения ресурса
        /// </summary>
        /// <param name="resourceId">Сущность ресурса</param>
        /// <returns>Перенаправляет на страницу просмотра ресурса</returns>
        [HttpGet]
        public ActionResult EditResource(long resourceId)
        {
            AuthorizeHelper.AuthorizeResourcePackByResource(resourceId);
            var resource = Mapper.Map<ResourceEntity>(resourcesService.FindByIdWithRelatedData(resourceId));
            resource.ResourceTypes = resourceTypeService.GetForUserGroup(resource.ResourcePack.UserGroupId);
            return PartialView(resource);
        }

        /// <summary>
        /// Изменяет определенный ресурс в базе данных
        /// </summary>
        /// <param name="resource">Сущность ресурса</param>
        /// <returns>Перенаправляет на страницу просмотра ресурса</returns>
        [HttpPost]
        public ActionResult EditResource(ResourceEntity resource)
        {
            AuthorizeHelper.AuthorizeResourcePackByResource(resource.Id);
            AuthorizeHelper.AuthorizeResourceType(resource.ResourceTypeId);
            if (ModelState.IsValid)
            {
                resourcesService.Update(resource, Membership.GetUser().UserName);
                return RedirectToAction("ViewResource", new { resource.Id });
            }

            var dbResource = resourcesService.FindByIdWithRelatedData(resource.Id);
            resource.ResourceTypes = resourceTypeService.GetForUserGroup(dbResource.ResourcePack.UserGroupId);
            return PartialView(resource);
        }

        /// <summary>
        /// Страница добавления ресурса
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возврашает представление страницы добавления ресурса</returns>
        [HttpGet]
        public ActionResult AddResource(long? resourcePackId)
        {
            var resource = new ResourceEntity();
            if (resourcePackId.HasValue)
            {
                AuthorizeHelper.AuthorizeResourcePack(resourcePackId.Value);
                resource.ResourcePack = Mapper.Map<ResourcePackEntity>(resourcePackService.FindById(resourcePackId.Value));
                resource.ResourcePackId = resource.ResourcePack.Id;
                resource.ResourceTypes = resourceTypeService.GetForUserGroup(resource.ResourcePack.UserGroupId);
            }
            else
            {
                var userName = Membership.GetUser().UserName;
                resource.ResourcePacks =
                    Mapper.Map<IEnumerable<ResourcePackEntity>>(resourcePackService.GetForCurrentUserGroup(userName));
                resource.ResourceTypes = resourceTypeService.GetForCurrentUserGroup(userName);
            }
            return PartialView(resource);
        }

        /// <summary>
        /// Добавляет ресурс в базу данных
        /// </summary>
        /// <param name="resource">Сущность ресурса</param>
        /// <returns>Перенаправляет на страницу просмотра ресурса</returns>
        [HttpPost]
        public ActionResult AddResource(ResourceEntity resource)
        {
            AuthorizeHelper.AuthorizeResourcePackByResource(resource);
            AuthorizeHelper.AuthorizeResourceType(resource.ResourceTypeId);
            if (ModelState.IsValid)
            {
                var id = resourcesService.Add(resource, Membership.GetUser().UserName);
                return RedirectToAction("ViewResource", new { id });
            }

            resource.ResourcePack =
                Mapper.Map<ResourcePackEntity>(resourcePackService.FindById(resource.ResourcePackId));
            resource.ResourceTypes = resourceTypeService.GetForUserGroup(resource.ResourcePack.UserGroupId);
            return PartialView(resource);
        }

        /// <summary>
        /// Удаляет ресурс из базы данных
        /// </summary>
        /// <param name="id">Идентификатор ресурса</param>
        /// <returns>Перенаправляет на пустую страницу модального окна</returns>
        [HttpGet]
        public ActionResult RemoveResource(long id)
        {
            AuthorizeHelper.AuthorizeResourcePackByResource(id);
            resourcesService.Remove(id, Membership.GetUser().UserName);
            return PartialView("_BlankModalForm");
        }

        /// <summary>
        /// Страница добавления ресурспака
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей (опционально)</param>
        /// <returns>Возвращает представление страницы добавления ресурспака</returns>
        [HttpGet]
        public ActionResult AddResourcePack(long? userGroupId)
        {
            var resourcePackEntity = new ResourcePackEntity();
            if (userGroupId.HasValue)
            {
                AuthorizeHelper.AuthorizeUserGroup(userGroupId.Value);
                resourcePackEntity.UserGroupId = userGroupId.Value;
                resourcePackEntity.UserGroup =
                    Mapper.Map<UserGroupEntity>(userGroupService.FindById(userGroupId.Value));
            }

            return PartialView(resourcePackEntity);
        }

        /// <summary>
        /// Добавляет ресурспак в базу данных
        /// </summary>
        /// <param name="resourcePack">Сущность ресурспака</param>
        /// <returns>Перенаправляет на страницу сообщения об успешном создании ресурспака</returns>
        [HttpPost]
        public ActionResult AddResourcePack(ResourcePackEntity resourcePack)
        {
            if (ModelState.IsValid)
            {
                AuthorizeHelper.AuthorizeResourcePack(resourcePack);
                var id = resourcePackService.Add(resourcePack, Membership.GetUser().UserName);
                return PartialView("MessageModalForm", new KeyValuePair<string, string>(
                            Titles.AddResourcePack,
                            string.Format(
                                CommonResource.ResourcePackCreatedSuccsessfully,
                                CommonHelper.GenerateLink(Url.Action("ViewResourcePack", new { id }), CommonResource.ResourcePack))));
            }

            return PartialView(resourcePack);
        }

        /// <summary>
        /// Страница изменения ресурспака
        /// </summary>
        /// <param name="resourcePackId">Идентификатор ресурспака</param>
        /// <returns>Возвращает представление страницы изменения ресурспака</returns>
        [HttpGet]
        public ActionResult EditResourcePack(long resourcePackId)
        {
            AuthorizeHelper.AuthorizeResourcePack(resourcePackId);
            var resourcePack = Mapper.Map<ResourcePackEntity>(resourcePackService.FindByIdWithRelatedData(resourcePackId));
            return PartialView(resourcePack);
        }

        /// <summary>
        /// Изменяет определенный объект ресурспака
        /// </summary>
        /// <param name="resourcePack">Сущность ресурспака</param>
        /// <returns>Перенаправляет на страницу сообщения об успешном изменении ресурспака</returns>
        [HttpPost]
        public ActionResult EditResourcePack(ResourcePackEntity resourcePack)
        {
            AuthorizeHelper.AuthorizeResourcePack(resourcePack.Id);
            if (ModelState.IsValid)
            {
                resourcePackService.Update(resourcePack, Membership.GetUser().UserName);
                return PartialView("MessageModalForm", new KeyValuePair<string, string>(
                            Titles.EditResourcePack,
                            string.Format(
                                CommonResource.ResourcePackEditedSuccsessfully,
                                CommonHelper.GenerateLink(Url.Action("ViewResourcePack", new { resourcePack.Id }), CommonResource.ResourcePack))));
            }

            return PartialView(resourcePack);
        }

        /// <summary>
        /// Удаляет ресурспак из базы данных
        /// </summary>
        /// <param name="id">Идентификатор ресурспака</param>
        /// <returns>Перенаправляет на пустую страницу модального окна</returns>
        [HttpGet]
        public ActionResult RemoveResourcePack(long id)
        {
            AuthorizeHelper.AuthorizeResourcePack(id);
            resourcePackService.Remove(id, Membership.GetUser().UserName);
            return PartialView("MessageModalForm", new KeyValuePair<string, string>(
                            Titles.AddResourcePack,
                            string.Format(
                                CommonResource.ResourcePackRemovedSuccsessfully,
                                CommonHelper.GenerateLink(Url.Action("Index", "Home"), CommonResource.Main))));
        }

        /// <summary>
        /// Страница просмотра всех типов ресурсов в рамках группы пользователей
        /// </summary>
        /// <param name="id">Идентификатор группы пользователей</param>
        /// <returns>Перенаправляет на страницу просмотра типов ресурсов</returns>
        [HttpGet]
        public ActionResult ViewResourceTypes(long id)
        {
            AuthorizeHelper.AuthorizeUserGroup(id);
            return PartialView(resourceTypeService.GetForUserGroup(id));
        }

        /// <summary>
        /// Удаляет тип ресурса из базы данных
        /// </summary>
        /// <param name="resourceTypeId">Идентификатор типа ресурсов</param>
        /// <returns>Перенаправляет на страницу просмотра типов ресурсов</returns>
        [HttpGet]
        public ActionResult RemoveResourceType(long resourceTypeId)
        {
            AuthorizeHelper.AuthorizeResourceType(resourceTypeId);
            var userGroupId = resourceTypeService.FindById(resourceTypeId).UserGroupId;
            resourceTypeService.Remove(resourceTypeId, Membership.GetUser().UserName);
            return RedirectToAction("ViewResourceTypes", new { id = userGroupId });
        }

        /// <summary>
        /// Добавляет тип ресурса в базу данных
        /// </summary>
        /// <param name="userGroupId">Идентификатор группы пользователей</param>
        /// <param name="name">Название группы ресурса</param>
        /// <returns>Перенаправляет на страницу просмотра типов ресурсов</returns>
        [HttpGet]
        public ActionResult AddResourceType(string name, long userGroupId)
        {
            var resourceType = new ResourceTypeEntity() { Name = name, UserGroupId = userGroupId };
            AuthorizeHelper.AuthorizeResourceType(resourceType);
            if (resourceType.IsValid<ResourceTypeEntity>())
                resourceTypeService.Add(resourceType, Membership.GetUser().UserName);

            return RedirectToAction("ViewResourceTypes", new { id = userGroupId });
        }
    }
}
