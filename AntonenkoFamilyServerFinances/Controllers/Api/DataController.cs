﻿using System.Web.Mvc;
using BusinessLayer.Interfaces;
using BusinessLayer.Entities;
using StructureMap;
using AFinances.PresentationLayer.Infrastructure;

namespace AntonenkoFamilyServerFinances.Controllers.Api
{
    /// <summary>
    /// API Контроллер для внешних приложений
    /// </summary>
    public class DataController : MobileController
    {
        /// <summary>
        /// Сервис бюджета
        /// </summary>
        private IBudgetService<IncomeRequestEntity> budgetService;

        /// <summary>
        /// Базовый контроллер
        /// </summary>
        public DataController()
        {
            budgetService = ObjectFactory.GetInstance<IBudgetService<IncomeRequestEntity>>();
        }

        /// <summary>
        /// Получает текущий бюджет ресурспаков в виде JSON
        /// </summary>
        /// <returns>Возвращает текущий бюджет ресурспаков в виде JSON</returns>
        public JsonResult GetCurrentBudgetJson(string encrTicket)
        {
            var principal = MobileAuthenticator.GetPrincipal(encrTicket);
            return AuthenticateRequest(encrTicket)
                ? Json(string.Empty)
                : Json(budgetService.GetBudgetResourcePacks(), JsonRequestBehavior.AllowGet);
        }

    }
}
