﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AntonenkoFamilyServerFinances.Controllers.Api
{
    public class MobileController : Controller
    {
        protected bool AuthenticateRequest(string encrTicket)
        {
            return !(User.Identity.Name == null || User.Identity.IsAuthenticated == false);
        }
    }
}
