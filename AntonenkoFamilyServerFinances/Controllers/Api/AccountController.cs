using System.Web.Mvc;
using System.Web.Security;
using BusinessLayer.Entities;
using AFinances.PresentationLayer.Infrastructure;

namespace AntonenkoFamilyServerFinances.Controllers.Api
{
    public class AccountController : MobileController
    {

        /// <summary>
        /// Аутентифицирует пользователя в программе
        /// </summary>
        /// <param name="model">Модель аутентификации пользователя</param>
        /// <param name="returnUrl">Предыдущая страница, на которую необходимо вернуться</param>
        /// <returns>Переадресовывает на предыдущую страницу</returns>
        [HttpPost]
        public string LogOn(LogOnEntity model)
        {
            if (ModelState.IsValid && Membership.ValidateUser(model.UserName, model.Password))
            {
                return MobileAuthenticator.GenerateEncryptedTicket(model.UserName);
            }

            return string.Empty;
        }

        /// <summary>
        /// Проверяет соединение с сервером
        /// </summary>
        /// <returns>Возвращает строку "Pong", если соединение с сервером установлено</returns>
        public JsonResult Ping()
        {
            return Json("Pong", JsonRequestBehavior.AllowGet);
        }
    }
}